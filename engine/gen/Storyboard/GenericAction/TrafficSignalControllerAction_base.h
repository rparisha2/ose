/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include <string>
#include <utility>

#include "Conversion/OscToMantle/ConvertScenarioPhase.h"
#include "Conversion/OscToMantle/ConvertScenarioTrafficSignalController.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_1
{
class TrafficSignalControllerActionBase
{
public:
  struct Values
  {
    std::string phase;
    OPENSCENARIO::TrafficSignalController trafficSignalControllerRef;
    OPENSCENARIO::Phase phaseRef;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  TrafficSignalControllerActionBase(Values values, Interfaces interfaces)
      : values{std::move(values)},
        mantle{std::move(interfaces)} {};

  virtual ~TrafficSignalControllerActionBase() = default;
  virtual bool Step() = 0;

protected:
  Values values;
  Interfaces mantle;
};

}  // namespace OpenScenarioEngine::v1_1