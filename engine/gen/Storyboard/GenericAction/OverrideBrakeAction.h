/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <cassert>
#include <utility>

#include "Storyboard/GenericAction/OverrideBrakeAction_impl.h"
#include "Utils/EntityBroker.h"

class OverrideBrakeAction : public yase::ActionNode
{
public:
  explicit OverrideBrakeAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOverrideBrakeAction> overrideBrakeAction)
      : yase::ActionNode{"OverrideBrakeAction"},
        overrideBrakeAction_{std::move(overrideBrakeAction)}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const auto entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_1::OverrideBrakeAction>(
        OpenScenarioEngine::v1_1::OverrideBrakeAction::Values{
            entityBroker->GetEntites(),
            overrideBrakeAction_->GetActive(),
            overrideBrakeAction_->GetValue()},
        OpenScenarioEngine::v1_1::OverrideBrakeAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_1::OverrideBrakeAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOverrideBrakeAction> overrideBrakeAction_;
};