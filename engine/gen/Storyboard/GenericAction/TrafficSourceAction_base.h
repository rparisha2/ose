/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include <utility>

#include "Conversion/OscToMantle/ConvertScenarioPosition.h"
#include "Conversion/OscToMantle/ConvertScenarioTrafficDefinition.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_1
{
class TrafficSourceActionBase
{
public:
  struct Values
  {
    double radius;
    double rate;
    double velocity;
    std::function<std::optional<mantle_api::Pose>()> GetPosition;
    OPENSCENARIO::TrafficDefinition trafficDefinition;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  TrafficSourceActionBase(Values values, Interfaces interfaces)
      : values{values},
        mantle{std::move(interfaces)} {};

  virtual ~TrafficSourceActionBase() = default;

  virtual bool Step() = 0;

protected:
  Values values;
  Interfaces mantle;
};

}  // namespace OpenScenarioEngine::v1_1