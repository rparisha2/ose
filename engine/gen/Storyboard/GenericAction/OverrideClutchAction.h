/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <cassert>
#include <utility>

#include "Storyboard/GenericAction/OverrideClutchAction_impl.h"
#include "Utils/EntityBroker.h"

class OverrideClutchAction : public yase::ActionNode
{
public:
  explicit OverrideClutchAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOverrideClutchAction> overrideClutchAction)
      : yase::ActionNode{"OverrideClutchAction"},
        overrideClutchAction_{std::move(overrideClutchAction)}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const auto entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_1::OverrideClutchAction>(
        OpenScenarioEngine::v1_1::OverrideClutchAction::Values{
            entityBroker->GetEntites(),
            overrideClutchAction_->GetActive(),
            overrideClutchAction_->GetValue()},
        OpenScenarioEngine::v1_1::OverrideClutchAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_1::OverrideClutchAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOverrideClutchAction> overrideClutchAction_;
};