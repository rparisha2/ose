/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include <utility>

#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_1
{
class ActivateControllerActionBase
{
public:
  struct Values
  {
    Entities entities;
    bool lateral;
    bool longitudinal;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  ActivateControllerActionBase(Values values, Interfaces interfaces)
      : values{std::move(values)},
        mantle{std::move(interfaces)} {};

  virtual ~ActivateControllerActionBase() = default;

  virtual bool Step() = 0;

protected:
  Values values;
  Interfaces mantle;
};

}  // namespace OpenScenarioEngine::v1_1