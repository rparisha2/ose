/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <cassert>
#include <utility>

#include "Storyboard/GenericAction/AssignRouteAction_impl.h"
#include "Utils/EntityBroker.h"

class AssignRouteAction : public yase::ActionNode
{
public:
  explicit AssignRouteAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAssignRouteAction> assignRouteAction)
      : yase::ActionNode{"AssignRouteAction"},
        assignRouteAction_{std::move(assignRouteAction)}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const auto entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_1::AssignRouteAction>(
        OpenScenarioEngine::v1_1::AssignRouteAction::Values{
            entityBroker->GetEntites(),
            OPENSCENARIO::ConvertScenarioRoute(environment, assignRouteAction_->GetRoute(), assignRouteAction_->GetCatalogReference())},
        OpenScenarioEngine::v1_1::AssignRouteAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_1::AssignRouteAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAssignRouteAction> assignRouteAction_;
};