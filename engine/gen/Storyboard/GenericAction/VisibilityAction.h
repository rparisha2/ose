/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <cassert>
#include <utility>

#include "Storyboard/GenericAction/VisibilityAction_impl.h"
#include "Utils/EntityBroker.h"

class VisibilityAction : public yase::ActionNode
{
public:
  explicit VisibilityAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IVisibilityAction> visibilityAction)
      : yase::ActionNode{"VisibilityAction"},
        visibilityAction_{std::move(visibilityAction)}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const auto entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_1::VisibilityAction>(
        OpenScenarioEngine::v1_1::VisibilityAction::Values{
            entityBroker->GetEntites(),
            visibilityAction_->GetGraphics(),
            visibilityAction_->GetSensors(),
            visibilityAction_->GetTraffic()},
        OpenScenarioEngine::v1_1::VisibilityAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_1::VisibilityAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IVisibilityAction> visibilityAction_;
};