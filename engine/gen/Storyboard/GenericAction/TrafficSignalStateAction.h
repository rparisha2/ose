/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <cassert>
#include <utility>

#include "Storyboard/GenericAction/TrafficSignalStateAction_impl.h"
#include "Utils/EntityBroker.h"

class TrafficSignalStateAction : public yase::ActionNode
{
public:
  explicit TrafficSignalStateAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalStateAction> trafficSignalStateAction)
      : yase::ActionNode{"TrafficSignalStateAction"},
        trafficSignalStateAction_{std::move(trafficSignalStateAction)}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");

    impl_ = std::make_unique<OpenScenarioEngine::v1_1::TrafficSignalStateAction>(
        OpenScenarioEngine::v1_1::TrafficSignalStateAction::Values{
            trafficSignalStateAction_->GetName(),
            trafficSignalStateAction_->GetState()},
        OpenScenarioEngine::v1_1::TrafficSignalStateAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_1::TrafficSignalStateAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalStateAction> trafficSignalStateAction_;
};