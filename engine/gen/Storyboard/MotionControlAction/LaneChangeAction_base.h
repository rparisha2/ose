/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include <utility>

#include "Conversion/OscToMantle/ConvertScenarioLaneChangeTarget.h"
#include "Conversion/OscToMantle/ConvertScenarioTransitionDynamics.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_1
{
class LaneChangeActionBase
{
public:
  struct Values
  {
    Entities entities;
    double targetLaneOffset;
    mantle_api::TransitionDynamics laneChangeActionDynamics;
    std::function<mantle_api::UniqueId()> GetLaneChangeTarget;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  LaneChangeActionBase(Values values, Interfaces interfaces)
      : values{std::move(values)},
        mantle{std::move(interfaces)} {};

  virtual ~LaneChangeActionBase() = default;

  virtual void SetControlStrategy() = 0;
  virtual bool HasControlStrategyGoalBeenReached(const std::string& actor) = 0;
  [[nodiscard]] virtual mantle_api::MovementDomain GetMovementDomain() const = 0;

protected:
  Values values;
  Interfaces mantle;
};

}  // namespace OpenScenarioEngine::v1_1