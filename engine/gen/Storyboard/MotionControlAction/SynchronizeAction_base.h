/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include <utility>

#include "Conversion/OscToMantle/ConvertScenarioEntity.h"
#include "Conversion/OscToMantle/ConvertScenarioFinalSpeed.h"
#include "Conversion/OscToMantle/ConvertScenarioPosition.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_1
{
class SynchronizeActionBase
{
public:
  struct Values
  {
    Entities entities;
    double targetTolerance;
    double targetToleranceMaster;
    std::function<std::optional<mantle_api::Pose>()> GetTargetPositionMaster;
    std::function<std::optional<mantle_api::Pose>()> GetTargetPosition;
    mantle_api::Vec3<units::velocity::meters_per_second_t> finalSpeed;
    OPENSCENARIO::Entity masterEntityRef;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  SynchronizeActionBase(Values values, Interfaces interfaces)
      : values{std::move(values)},
        mantle{std::move(interfaces)} {};

  virtual ~SynchronizeActionBase() = default;

  virtual void SetControlStrategy() = 0;
  virtual bool HasControlStrategyGoalBeenReached(const std::string& actor) = 0;
  [[nodiscard]] virtual mantle_api::MovementDomain GetMovementDomain() const = 0;

protected:
  Values values;
  Interfaces mantle;
};

}  // namespace OpenScenarioEngine::v1_1