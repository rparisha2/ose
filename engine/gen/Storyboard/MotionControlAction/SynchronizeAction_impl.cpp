/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/MotionControlAction/SynchronizeAction_impl.h"

#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_1
{
void SynchronizeAction::SetControlStrategy()
{
  // Note:
  // - Access to values parse to mantle/ose datatypes: this->values.xxx
  // - Access to mantle interfaces: this->mantle.xxx
  OPENSCENARIO::Logger::Warning("Method SynchronizeAction::SetControlStrategy() not implemented yet");
}

bool SynchronizeAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
  // Note:
  // - Access to values parse to mantle/ose datatypes: this->values.xxx
  // - Access to mantle interfaces: this->mantle.xxx
  OPENSCENARIO::Logger::Warning("Method SynchronizeAction::HasControlStrategyGoalBeenReached() not implemented yet (returning \"true\" by default)");
  return true;
}

mantle_api::MovementDomain SynchronizeAction::GetMovementDomain() const
{
  // Note: return _control_strategy.movement_domain;
  return mantle_api::MovementDomain::kUndefined;
}

}  // namespace OpenScenarioEngine::v1_1