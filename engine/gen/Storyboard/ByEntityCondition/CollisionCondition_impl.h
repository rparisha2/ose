/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include <utility>

#include "Conversion/OscToMantle/ConvertScenarioByObjectType.h"
#include "Conversion/OscToMantle/ConvertScenarioEntity.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_1
{
class CollisionCondition
{
public:
  struct Values
  {
    std::string triggeringEntity;
    OPENSCENARIO::Entity entityRef;
    OPENSCENARIO::ByObjectType byType;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  CollisionCondition(Values values, Interfaces interfaces)
      : values{std::move(values)},
        mantle{std::move(interfaces)} {};

  [[nodiscard]] bool IsSatisfied() const;

private:
  Values values;
  Interfaces mantle;
};

}  // namespace OpenScenarioEngine::v1_1