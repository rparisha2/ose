/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseLateralAction.h"

#include <memory>

#include "Conversion/OscToNode/ParseLaneChangeAction.h"
#include "Conversion/OscToNode/ParseLaneOffsetAction.h"
#include "Conversion/OscToNode/ParseLateralDistanceAction.h"

yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILateralAction> lateralAction)
{
  if (auto element = lateralAction->GetLaneChangeAction(); element)
  {
    return parse(element);
  }
  if (auto element = lateralAction->GetLaneOffsetAction(); element)
  {
    return parse(element);
  }
  if (auto element = lateralAction->GetLateralDistanceAction(); element)
  {
    return parse(element);
  }
  throw std::runtime_error("Corrupted openSCENARIO file: No choice made within std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILateralAction>");
}
