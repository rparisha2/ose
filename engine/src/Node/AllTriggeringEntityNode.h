/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <agnostic_behavior_tree/composite_node.h>

namespace yase
{
class AllTriggeringEntitiyNode : public CompositeNode
{
public:
  explicit AllTriggeringEntitiyNode(const std::string &name = "Unnamed")
      : AllTriggeringEntitiyNode(name, nullptr){};

  explicit AllTriggeringEntitiyNode(Extension::UPtr extension_ptr)
      : AllTriggeringEntitiyNode("Unnamed", std::move(extension_ptr)){};

  AllTriggeringEntitiyNode(const std::string &name, Extension::UPtr extension_ptr)
      : CompositeNode(std::string("Sequence::").append(name), std::move(extension_ptr)){};

  ~AllTriggeringEntitiyNode() override = default;

  void onInit() override
  {
    m_last_running_child = 0;
    this->child(m_last_running_child).onInit();
  }

private:
  NodeStatus tick() final
  {
    // Loop over all children
    for (size_t index = m_last_running_child; index < childrenCount(); index++)
    {
      auto &child = this->child(index);
      const NodeStatus child_status = child.executeTick();

      switch (child_status)
      {
        case NodeStatus::kIdle:
        {
          throw std::runtime_error("Child was not initilized.");
        }
        case NodeStatus::kRunning:
        {
          return NodeStatus::kRunning;
        }
        case NodeStatus::kFailure:
        {
          throw std::runtime_error("Conditions are not allowed to fail.");
        }
        case NodeStatus::kSuccess:
        {
          // Terminate current child and init subsequent one
          child.onTerminate();
          m_last_running_child++;
          if (m_last_running_child < childrenCount())
          {
            this->child(m_last_running_child).onInit();
          }
          continue;
        }
      }

      throw std::invalid_argument("Undefined child_status.");
    }

    return NodeStatus::kSuccess;
  }

  size_t m_last_running_child{0};
};

}  // namespace yase
