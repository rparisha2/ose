/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2021 Max Paul Bauer - Robert Bosch GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <agnostic_behavior_tree/behavior_node.h>
#include <agnostic_behavior_tree/composite/parallel_node.h>
#include <agnostic_behavior_tree/composite/sequence_node.h>
#include <agnostic_behavior_tree/composite_node.h>
#include <agnostic_behavior_tree/decorator_node.h>

#include <memory>
#include <stdexcept>
#include <utility>

#include "Node/TriggerableCompositeNode.h"
#include "Utils/EngineAbortFlags.h"

namespace yase
{
class ExecuteOnce : public DecoratorNode
{
public:
  ExecuteOnce()
      : DecoratorNode(std::string("ExecuteOnce")) {}
  ~ExecuteOnce() override = default;

private:
  // Executes child behavior until condition is evaluated to true
  NodeStatus tick() final
  {
    if (m_execution_pending)
    {
      if (hasChild())
      {
        auto status = child().executeTick();
        if (status != NodeStatus::kSuccess)
        {
          throw std::runtime_error("Node of ExecuteOnce must finish in one step");
        }
      }
      m_execution_pending = false;
      return NodeStatus::kRunning;
    }
    return NodeStatus::kSuccess;
  };

  void onInit() final
  {
    m_execution_pending = true;
    DecoratorNode::onInit();
  };

  bool m_execution_pending{true};
};

class StoryboardNode : public TriggerableCompositeNode
{
public:
  using Init = StrongNodePtr<struct InitTag>;
  using Story = StrongNodePtr<struct StoryTag>;

  StoryboardNode(const std::string& name,
                 yase::BehaviorNode::Ptr stories,
                 yase::BehaviorNode::Ptr init = nullptr,
                 yase::BehaviorNode::Ptr stop_trigger = nullptr)
      : TriggerableCompositeNode{name}
  {
    initTree(StopTriggerPtr{std::move(stop_trigger)}, Init{std::move(init)});
    addStory(Story{std::move(stories)});
  }

  StoryboardNode(const std::string& name, Init init, const StopTriggerPtr& stop_trigger = StopTriggerPtr::Empty())
      : TriggerableCompositeNode{name}
  {
    initTree(stop_trigger, std::move(init));
  }

  explicit StoryboardNode(const std::string& name, const StopTriggerPtr& stop_trigger = StopTriggerPtr::Empty(), Init init = Init::Empty())
      : TriggerableCompositeNode{name}
  {
    initTree(stop_trigger, std::move(init));
  }

  void addStory(Story story)
  {
    story_mediator->addChild(story);
  }

  void lookupAndRegisterData(yase::Blackboard &blackboard) override
  {
    engine_abort_flags_ = blackboard.get<std::shared_ptr<EngineAbortFlags> >("EngineAbortFlags");
  }

  NodeStatus tick() override
  {
    auto node_status = TriggerableCompositeNode::tick();
    if (node_status == NodeStatus::kSuccess)
    {
      *engine_abort_flags_ = EngineAbortFlags::kStopTrigger;
    }
    if (node_status == NodeStatus::kFailure)
    {
      *engine_abort_flags_ = EngineAbortFlags::kFailure;
    }
    return node_status;
  }

private:
  void initTree(const StopTriggerPtr& stop_trigger = StopTriggerPtr::Empty(), Init init = Init::Empty())
  {
    storyboard_mediator->addChild(init_mediator);

    if (init != Init::Empty())
    {
      init_mediator->addChild(init);
    }

    storyboard_mediator->addChild(story_mediator);

    if (stop_trigger != StopTriggerPtr::Empty())
    {
      set(storyboard_mediator, stop_trigger);
    }
    else
    {
      set(storyboard_mediator);
    }
  }

  ParallelNode::Ptr storyboard_mediator{std::make_shared<ParallelNode>("StoryboardMediator")};
  ParallelNode::Ptr init_mediator{std::make_shared<ParallelNode>("StoryboardInitMediator")};
  ParallelNode::Ptr story_mediator{std::make_shared<ParallelNode>("StoryboardStoryMediator")};
  std::shared_ptr<EngineAbortFlags> engine_abort_flags_;
};
}  // namespace yase