/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Node/ManeuverGroupNode.h"

#include "Conversion/OscToMantle/ConvertScenarioEntityRef.h"
#include "Utils/EntityBroker.h"
#include "Utils/Logger.h"

ManeuverGroupNode::ManeuverGroupNode(std::string name,
                                     std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IActors> actors)
    : yase::ParallelNode{std::move(name)}
{
  if (actors != nullptr)
  {
    entityBroker_ = std::make_shared<EntityBroker>(actors->GetSelectTriggeringEntities());
    for (std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEntityRef> entityRef : actors->GetEntityRefs())
    {
      entityBroker_->add(parse(entityRef));
    }
  }
  else
  {
    OPENSCENARIO::Logger::Warning("ManeuverGroupNode: No actor defined in ManeuverGroup");
  }
}

void ManeuverGroupNode::lookupAndRegisterData(yase::Blackboard &blackboard)
{
  blackboard.set("EntityBroker", entityBroker_);
}
