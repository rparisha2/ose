
/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>
#include <optional>
#include <string>

namespace OPENSCENARIO
{
using TrafficDefinition = std::optional<bool>;  // not implemented yet

inline TrafficDefinition ConvertScenarioTrafficDefinition(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficDefinition>& /*trafficDefinition*/)
{
  return std::nullopt;
}

}  // namespace OPENSCENARIO