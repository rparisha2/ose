
/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Traffic/i_controller_config.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>

namespace OPENSCENARIO
{
using Controller = mantle_api::ExternalControllerConfig;

Controller ConvertScenarioController(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IController>& controller,
                                     const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference>& catalogReference);

}  // namespace OPENSCENARIO