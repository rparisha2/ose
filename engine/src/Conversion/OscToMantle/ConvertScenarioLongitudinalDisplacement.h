/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Map/i_lane_location_query_service.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>
#include <string>

namespace OPENSCENARIO
{
using LongitudinalDisplacement = mantle_api::Direction;

LongitudinalDisplacement ConvertScenarioLongitudinalDisplacement(const NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement& longitudinalDisplacement);
}  // namespace OPENSCENARIO