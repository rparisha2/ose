
/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioTransitionDynamics.h"

#include <MantleAPI/Traffic/entity_helper.h>

namespace OPENSCENARIO
{
mantle_api::Shape ConvertToMantleApi(const NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape& from)
{
  if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::STEP)
  {
    return mantle_api::Shape::kStep;
  }
  if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::CUBIC)
  {
    return mantle_api::Shape::kCubic;
  }
  if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::LINEAR)
  {
    return mantle_api::Shape::kLinear;
  }
  if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::SINUSOIDAL)
  {
    return mantle_api::Shape::kSinusoidal;
  }

  return mantle_api::Shape::kUndefined;
}

mantle_api::Dimension ConvertToMantleApi(const NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension& from)
{
  if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::TIME)
  {
    return mantle_api::Dimension::kTime;
  }
  if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::DISTANCE)
  {
    return mantle_api::Dimension::kDistance;
  }
  if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::RATE)
  {
    return mantle_api::Dimension::kRate;
  }
  return mantle_api::Dimension::kUndefined;
}

mantle_api::TransitionDynamics ConvertScenarioTransitionDynamics(
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITransitionDynamics>& transitionDynamics)
{
  mantle_api::TransitionDynamics transition_dynamics{};
  transition_dynamics.value = transitionDynamics->GetValue();
  transition_dynamics.dimension = ConvertToMantleApi(transitionDynamics->GetDynamicsDimension());
  transition_dynamics.shape = ConvertToMantleApi(transitionDynamics->GetDynamicsShape());
  return transition_dynamics;
}

}  // namespace OPENSCENARIO