
/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>
#include <string>
#include <variant>

#include "Utils/EntityBroker.h"

namespace OPENSCENARIO
{
using TimeToCollisionConditionTarget = std::variant<Entity, std::optional<mantle_api::Pose>>;

TimeToCollisionConditionTarget ConvertScenarioTimeToCollisionConditionTarget(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITimeToCollisionConditionTarget>& timeToCollisionConditionTarget);

}  // namespace OPENSCENARIO