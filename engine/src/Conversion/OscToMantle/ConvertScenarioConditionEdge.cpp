/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioConditionEdge.h"

#include "Utils/ConditionEdgeEvaluator.h"

namespace OPENSCENARIO
{
ConditionEdge ConvertScenarioConditionEdge(const NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge& conditionEdge)
{
  if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge::RISING)
  {
    return ConditionEdge::kRising;
  }
  if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge::FALLING)
  {
    return ConditionEdge::kFalling;
  }
  if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge::RISING_OR_FALLING)
  {
    return ConditionEdge::kRisingOrFalling;
  }
  if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge::NONE)
  {
    return ConditionEdge::kNone;
  }

  throw std::runtime_error("ConvertScenarioConditionEdge: Unsupported ConditionEdge");
}

}  // namespace OPENSCENARIO
