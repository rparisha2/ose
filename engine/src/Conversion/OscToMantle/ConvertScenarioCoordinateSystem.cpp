/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioCoordinateSystem.h"

#include <stdexcept>

namespace OPENSCENARIO
{
CoordinateSystem ConvertScenarioCoordinateSystem(const NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem& coordinateSystem)
{
  switch (NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::GetFromLiteral(coordinateSystem.GetLiteral()))
  {
    case NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::UNKNOWN:
      return CoordinateSystem::kUnknown;
    case NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::ENTITY:
      return CoordinateSystem::kEntity;
    case NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE:
      return CoordinateSystem::kLane;
    case NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::ROAD:
      return CoordinateSystem::kRoad;
    case NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::TRAJECTORY:
      return CoordinateSystem::kTrajectory;
  }
  throw std::runtime_error("ConvertScenarioCoordinateSystem: Unknown CoordinateSystem");
}

}  // namespace OPENSCENARIO