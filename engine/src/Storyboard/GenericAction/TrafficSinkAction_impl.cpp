/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/TrafficSinkAction_impl.h"

#include <memory>
#include <optional>

#include "Conversion/OscToMantle/ConvertScenarioTrafficDefinition.h"
#include "MantleAPI/Common/pose.h"
#include "MantleAPI/Traffic/i_entity.h"
#include "Utils/Logger.h"

using namespace units::literals;

namespace OpenScenarioEngine::v1_1
{
namespace detail
{
/// everything within 1mm shall be considered as "on spot"
static constexpr double EPSILON_RADIUS{1E-3};

[[nodiscard]] bool WithinRadius(
    const mantle_api::Vec3<units::length::meter_t>& current_position,
    const mantle_api::Vec3<units::length::meter_t>& target_position,
    double radius)
{
  return (target_position - current_position).Length().value() <= radius + EPSILON_RADIUS;
}

[[nodiscard]] bool IsVehicleOrPedestrian(const std::unique_ptr<mantle_api::IEntity>& entity)
{
  return dynamic_cast<mantle_api::IVehicle*>(entity.get()) ||
         dynamic_cast<mantle_api::IPedestrian*>(entity.get());
}

/// Helper class to print warnings only once
///
/// Example Usage:
/// IssueOnce<double>("Test") will print "Test" only once within the
/// translation unit for the type "double". The template parameter is
/// used to distinguish "who" is calling. You can use anonymous structs
/// like `struct MyMethodTag` to easily tag each callee.
///
/// @tparam Tag used to distinguish the callee
template <typename Tag>
class IssueOnce
{
public:
  constexpr IssueOnce(std::string_view message) noexcept
  {
    if (!warning_issued)
    {
      OPENSCENARIO::Logger::Warning(message);
      warning_issued = true;
    }
  }

private:
  static inline bool warning_issued{false};
};

/// Checks (and unwraps) a pose to a position
///
/// @param pose optional pose
/// @returns optional position with no value, if pose was not defined,
///          which will also issue a warning (once)
[[nodiscard]] std::optional<decltype(mantle_api::Pose::position)> CheckPosition(const std::optional<mantle_api::Pose>& pose)
{
  if (pose)
  {
    return pose->position;
  }
  else
  {
    IssueOnce<decltype(CheckPosition)>("TrafficSinkAction: TrafficSinkAction cannot be satisfied (pose undefined).");
    return {};
  }
}

/// Checks if the given rate is infinite (only supported value)
///
/// @param rate
/// @returns inf and raises a warning (once) if any other input value is given
[[nodiscard]] double CheckRate(double rate)
{
  if (rate == std::numeric_limits<double>::infinity())
  {
    return rate;
  }
  else
  {
    IssueOnce<decltype(CheckRate)>("TrafficSinkAction: Rate not implemented yet. Treating rate as \"inf\"\n");
    return std::numeric_limits<double>::infinity();
  }
}

/// Checks if the trafficDefinition is set (not supported yet)
///
/// @param trafficDefinition
/// @returns empty optional and raises a warning (once) if any other input value is given
[[nodiscard]] OPENSCENARIO::TrafficDefinition CheckTrafficDefinition(const OPENSCENARIO::TrafficDefinition& trafficDefinition)
{
  if (!trafficDefinition)
  {
    return trafficDefinition;
  }
  else
  {
    IssueOnce<decltype(CheckTrafficDefinition)>("TrafficSinkAction: TrafficDefinition not implemented yet. Ignoring parameter.");
    return {};
  }
}

}  // namespace detail

bool TrafficSinkAction::Step()
{
  auto position = detail::CheckPosition(values.GetPosition());
  [[maybe_unused]] auto rate = detail::CheckRate(values.rate);
  [[maybe_unused]] auto traffic_definition = detail::CheckTrafficDefinition(values.trafficDefinition);

  if (!position)
  {
    return false;
  }

  std::vector<mantle_api::UniqueId> affected_entity_ids;

  auto& entity_repository = mantle.environment->GetEntityRepository();
  for (const auto& entity : entity_repository.GetEntities())
  {
    if (detail::IsVehicleOrPedestrian(entity) &&
        detail::WithinRadius(entity->GetPosition(), position.value(), values.radius))
    {
      affected_entity_ids.emplace_back(entity->GetUniqueId());
    }
  }

  // Delete would invalidate iterator of GetEntities()
  for (const auto& id : affected_entity_ids)
  {
    entity_repository.Delete(id);
  }

  // A TrafficSinkAction is an ongoing action and never succeeds
  return false;
}

}  // namespace OpenScenarioEngine::v1_1