/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/TeleportAction_impl.h"

#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_1
{
bool TeleportAction::Step()
{
  const auto global_position = values.GetPosition();
  if (!global_position)
  {
    OPENSCENARIO::Logger::Warning("TeleportAction: Unable to execute (undefined position).");
    return true;
  }

  for (auto actor : values.entities)
  {
    auto entity_tmp = mantle.environment->GetEntityRepository().Get(actor);

    if (!entity_tmp.has_value())
    {
      throw std::runtime_error("Actor '" + actor + "' in TeleportAction does not exist. Please adjust scenario.");
    }

    mantle_api::IEntity &entity = mantle.environment->GetEntityRepository().Get(actor).value().get();
    auto position = mantle.environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
        global_position->position,
        global_position->orientation,
        entity.GetProperties()->bounding_box.geometric_center);
    if (const auto properties = dynamic_cast<mantle_api::StaticObjectProperties*>(entity.GetProperties()))
    {
        position =
        mantle.environment->GetQueryService().GetUpwardsShiftedLanePosition(position, properties->vertical_offset(), true);
    }
    entity.SetPosition(position);
    entity.SetOrientation(global_position->orientation);

    auto lane_ids = mantle.environment->GetQueryService().GetLaneIdsAtPosition(global_position->position);
    entity.SetAssignedLaneIds(lane_ids);
  }
  return true;
}

}  // namespace OpenScenarioEngine::v1_1