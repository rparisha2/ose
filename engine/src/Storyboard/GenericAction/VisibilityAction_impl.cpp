/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/VisibilityAction_impl.h"

#include "Utils/EntityUtils.h"

namespace OpenScenarioEngine::v1_1
{
bool VisibilityAction::Step()
{
  for (const auto& entity : values.entities)
  {
    auto& entityRef = OPENSCENARIO::EntityUtils::GetEntityByName(mantle.environment, entity);
    mantle_api::EntityVisibilityConfig vis_config{values.graphics, values.traffic, values.sensors};
    entityRef.SetVisibility(vis_config);
  }
  return true;
}

}  // namespace OpenScenarioEngine::v1_1