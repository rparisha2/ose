/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/ByEntityCondition/TimeHeadwayCondition_impl.h"

#include "Utils/Constants.h"
#include "Utils/EntityUtils.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_1
{
bool TimeHeadwayCondition::IsSatisfied() const
{
  if (values.relativeDistanceType != OPENSCENARIO::RelativeDistanceType::kLongitudinal)
  {
    OPENSCENARIO::Logger::Warning("TimeHeadwayCondition: The given RelativeDistanceType is not supported. Only \"longitudinal\" distances are supported for now. Returning false.");
    return false;
  }

  if (!(values.coordinateSystem == OPENSCENARIO::CoordinateSystem::kEntity || values.coordinateSystem == OPENSCENARIO::CoordinateSystem::kLane))
  {
    OPENSCENARIO::Logger::Warning(R"(TimeHeadwayCondition: The given CoordinateSystem is not supported. Only "entity" and "lane" coordinate systems are supported for now. Returning false.)");
    return false;
  }

  const auto& ref_entity = OPENSCENARIO::EntityUtils::GetEntityByName(mantle.environment, values.entityRef);
  const auto& trigger_entity = OPENSCENARIO::EntityUtils::GetEntityByName(mantle.environment, values.triggeringEntity);

  units::length::meter_t distance{};
  if (values.coordinateSystem == OPENSCENARIO::CoordinateSystem::kEntity)
  {
    distance = values.freespace
                   ? OPENSCENARIO::EntityUtils::CalculateLongitudinalFreeSpaceDistance(mantle.environment, trigger_entity, ref_entity)
                   : OPENSCENARIO::EntityUtils::CalculateRelativeLongitudinalDistance(mantle.environment, trigger_entity, ref_entity);
  }
  else if (values.coordinateSystem == OPENSCENARIO::CoordinateSystem::kLane)
  {
    const auto longitudinal_lane_distance =
        mantle.environment->GetQueryService().GetLongitudinalLaneDistanceBetweenPositions(trigger_entity.GetPosition(),
                                                                                          ref_entity.GetPosition());
    if (!longitudinal_lane_distance.has_value())
    {
      throw std::runtime_error(
          "TimeHeadwayCondition: CoordinateSystem is set to \"LANE\", but can not get the longitudinal distance "
          "of the reference and the triggering entities along the lane center line. Please adjust scenario.");
    }
    distance = longitudinal_lane_distance.value();

    if (values.freespace)
    {
      const auto trigger_entity_lane_orientation =
          mantle.environment->GetQueryService().GetLaneOrientation(trigger_entity.GetPosition());
      const auto trigger_entity_corners_along_lane =
          OPENSCENARIO::EntityUtils::GetCornerPositionsInLocalSortedByLongitudinalDistance(
              mantle.environment, trigger_entity, trigger_entity.GetPosition(), trigger_entity_lane_orientation);

      const auto ref_entity_lane_pose = mantle.environment->GetQueryService().FindLanePoseAtDistanceFrom(
          {trigger_entity.GetPosition(), trigger_entity.GetOrientation()},
          distance,
          mantle_api::Direction::kForward);
      const auto ref_entity_corners_along_lane =
          OPENSCENARIO::EntityUtils::GetCornerPositionsInLocalSortedByLongitudinalDistance(
              mantle.environment, ref_entity, ref_entity.GetPosition(), ref_entity_lane_pose.value().orientation);

      distance = distance - (units::math::abs(trigger_entity_corners_along_lane.front().x) +
                             units::math::abs(ref_entity_corners_along_lane.back().x));
    }
  }
  else
  {
    // should never happen, as pre-conditions are already checked in the beginning
    throw std::runtime_error("TimeHeadwayCondition: The given CoordinateSystem is not supported. Only \"entity\" and \"lane\" coordinate systems are supported for now.");
  }

  const auto trigger_entity_velocity = trigger_entity.GetVelocity().Length();
  const auto time_headway = trigger_entity_velocity > OPENSCENARIO::limits::kAcceptableVelocityMin
                                ? distance / trigger_entity_velocity
                                : OPENSCENARIO::limits::kTimeHeadwayMax;

  return values.rule.IsSatisfied(time_headway.value());
}

}  // namespace OpenScenarioEngine::v1_1
