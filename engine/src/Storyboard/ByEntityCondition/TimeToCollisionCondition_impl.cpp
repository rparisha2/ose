/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/ByEntityCondition/TimeToCollisionCondition_impl.h"

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <MantleAPI/Traffic/i_entity_repository.h>
#include <units.h>

#include <stdexcept>
#include <variant>

#include "Utils/EntityUtils.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_1
{
using namespace OPENSCENARIO;

units::time::second_t TTCCalculation::operator()(const Entity& entity)
{
  if (!entity.empty())
  {
    OPENSCENARIO::Logger::Warning("TimeToCollisionCondition: TimeToCollision with respect to target entity not implemented yet. Returning 0.");
    return units::make_unit<units::time::second_t>(0);
  }

  throw std::runtime_error("TTCCalculation: Unable to resolve entity.");
}

units::time::second_t TTCCalculation::operator()(const std::optional<mantle_api::Pose>& pose)
{
  if (pose)
  {
    OPENSCENARIO::Logger::Warning("TimeToCollision with respect to pose not implemented yet (returning 0).");
    return units::make_unit<units::time::second_t>(0);
  }
  throw std::runtime_error("TTCCalculation: Unable to resolve position.");
}

bool TimeToCollisionCondition::IsSatisfied() const
{
  const auto& triggeringEntity = EntityUtils::GetEntityByName(mantle.environment, values.triggeringEntity);
  const auto ttc = std::visit(
      TTCCalculation(mantle.environment, triggeringEntity),
      values.timeToCollisionConditionTarget);
  return values.rule.IsSatisfied(ttc.value());
}

}  // namespace OpenScenarioEngine::v1_1
