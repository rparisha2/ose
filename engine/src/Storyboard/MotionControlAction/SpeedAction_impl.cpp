/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/MotionControlAction/SpeedAction_impl.h"

#include <MantleAPI/Traffic/control_strategy.h>
#include <MantleAPI/Traffic/entity_helper.h>

#include "Utils/EntityUtils.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_1
{
void SpeedAction::SetControlStrategy()
{
  switch (values.speedActionDynamics.shape)
  {
    case mantle_api::Shape::kUndefined:
      throw std::runtime_error("SpeedAction: Shape undefined\n");
      break;
    case mantle_api::Shape::kStep:
      for (const auto &entityRef : values.entities)
      {
        auto &entity = OPENSCENARIO::EntityUtils::GetEntityByName(mantle.environment, entityRef);
        mantle_api::SetSpeed(&entity, values.GetSpeedActionTarget());
      }
      break;
    case mantle_api::Shape::kCubic:
      // TODO: implement
      OPENSCENARIO::Logger::Warning(
          "SpeedAction: Dynamics shape CUBIC not yet implemented for SpeedAction."
          " Replacing with linear shape.");
      for (const auto &entityRef : values.entities)
      {
        SetLinearVelocitySplineControlStrategy(entityRef);
      }
      break;
    case mantle_api::Shape::kLinear:
      for (const auto &entityRef : values.entities)
      {
        SetLinearVelocitySplineControlStrategy(entityRef);
      }
      break;
    case mantle_api::Shape::kSinusoidal:
      OPENSCENARIO::Logger::Warning("SpeedAction: Sinusoidal shape not implemented yet");
      break;
  }
}

bool SpeedAction::HasControlStrategyGoalBeenReached(const std::string &actor)
{
  auto entity = mantle.environment->GetEntityRepository().Get(actor);
  if (!entity.has_value())
  {
    return false;
  }

  const auto &shape = values.speedActionDynamics.shape;
  return (shape == mantle_api::Shape::kStep) ||
         (shape == mantle_api::Shape::kLinear && mantle.environment->HasControlStrategyGoalBeenReached(
                                                     entity.value().get().GetUniqueId(),
                                                     mantle_api::ControlStrategyType::kFollowVelocitySpline));
}

mantle_api::MovementDomain SpeedAction::GetMovementDomain() const
{
  return mantle_api::MovementDomain::kLongitudinal;
}

void SpeedAction::SetSpline(const mantle_api::IEntity &entity,
                            mantle_api::SplineSection<units::velocity::meters_per_second> spline_section,
                            units::velocity::meters_per_second_t target_speed)
{
  std::get<3>(spline_section.polynomial) = entity.GetVelocity().Length();
  auto control_strategy = std::make_shared<mantle_api::FollowVelocitySplineControlStrategy>();
  control_strategy->default_value = target_speed;
  control_strategy->velocity_splines.push_back(std::move(spline_section));
  mantle.environment->UpdateControlStrategies(entity.GetUniqueId(), {control_strategy});
}

void SpeedAction::SetLinearVelocitySplineControlStrategy(const std::string &actor)
{
  auto &entity = OPENSCENARIO::EntityUtils::GetEntityByName(mantle.environment, actor);
  const auto target_speed = values.GetSpeedActionTarget();

  mantle_api::SplineSection<units::velocity::meters_per_second> spline_section;
  spline_section.start_time = mantle_api::Time(0);
  if (values.speedActionDynamics.dimension == mantle_api::Dimension::kTime)
  {
    const auto duration = units::time::second_t(values.speedActionDynamics.value);
    spline_section.end_time = duration;
    std::get<2>(spline_section.polynomial) = (target_speed - entity.GetVelocity().Length()) / duration;
    SetSpline(entity, spline_section, target_speed);
    return;
  }
  if (values.speedActionDynamics.dimension == mantle_api::Dimension::kRate)
  {
    const auto acceleration = units::acceleration::meters_per_second_squared_t(values.speedActionDynamics.value);
    spline_section.end_time = units::math::abs(target_speed - entity.GetVelocity().Length()) / acceleration;
    std::get<2>(spline_section.polynomial) = units::math::copysign(acceleration, target_speed - entity.GetVelocity().Length());
    SetSpline(entity, spline_section, target_speed);
    return;
  }
  if (values.speedActionDynamics.dimension == mantle_api::Dimension::kDistance)
  {
    const auto start_speed = entity.GetVelocity().Length().value();

    // Distance has to be converted to time, since spline is expected over time, not distance
    //  distance = time(v_init + v_delta/2) = time(v_init + v_end)/2 => time = 2*distance/(v_init + v_end)
    spline_section.end_time
      = units::time::second_t((2 * values.speedActionDynamics.value)/(target_speed.value() + start_speed));

    // v_end = v_init + acceleration*time, where time = 2*distance/(v_init + v_end) then acceleration = (v_end -
    // v_init) * (v_end + v_init) / 2* distance
    auto rate = units::acceleration::meters_per_second_squared_t((target_speed.value() - start_speed) *
                                                                 (target_speed.value() + start_speed) /
                                                                 (2 * values.speedActionDynamics.value));
    std::get<2>(spline_section.polynomial) = units::math::copysign(
      (units::acceleration::meters_per_second_squared_t(rate)), target_speed - entity.GetVelocity().Length());
    SetSpline(entity, spline_section, target_speed);
    return;
  }
  throw std::runtime_error("Dimension must be either kTime, kRate, or kDistance");
}

}  // namespace OpenScenarioEngine::v1_1