/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/MotionControlAction/LaneOffsetAction_impl.h"

#include <MantleAPI/Traffic/control_strategy.h>

#include "Utils/EntityUtils.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_1
{
void LaneOffsetAction::SetControlStrategy()
{
  control_strategy_->offset = values.GetLaneOffsetTarget();
  // control_strategy_->transition_dynamics = values.laneOffsetActionDynamics;
  for (const auto& actor : values.entities)
  {
    auto& entity = OPENSCENARIO::EntityUtils::GetEntityByName(mantle.environment, actor);
    mantle.environment->UpdateControlStrategies(
        entity.GetUniqueId(),
        {control_strategy_});
  }
}

bool LaneOffsetAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
  auto& entity = OPENSCENARIO::EntityUtils::GetEntityByName(mantle.environment, actor);
  return mantle.environment->HasControlStrategyGoalBeenReached(
      entity.GetUniqueId(),
      control_strategy_->type);
}

mantle_api::MovementDomain LaneOffsetAction::GetMovementDomain() const
{
  return mantle_api::MovementDomain::kLateral;
}

}  // namespace OpenScenarioEngine::v1_1
