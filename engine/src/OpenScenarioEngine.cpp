/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "OpenScenarioEngine/OpenScenarioEngine.h"

#include <agnostic_behavior_tree/behavior_node.h>
#include <agnostic_behavior_tree/utils/tree_print.h>
#include <openScenarioLib/src/common/ErrorLevel.h>
#include <openScenarioLib/src/loader/FileResourceLocator.h>
#include <openScenarioLib/src/v1_1/loader/XmlScenarioImportLoaderFactoryV1_1.h>

#include <sstream>
#include <utility>

#include "Node/RootNode.h"
#include "Utils/Constants.h"
#include "Utils/ControllerCreator.h"
#include "Utils/EngineAbortFlags.h"
#include "Utils/EntityCreator.h"
#include "Utils/Logger.h"

namespace OPENSCENARIO
{
namespace detail
{
std::pair<size_t, size_t> CountWarningsAndErrors(const OpenScenarioEngine::SimpleMessageLoggerPtr& logger)
{
  const auto warnings = logger->GetMessagesFilteredByErrorLevel(NET_ASAM_OPENSCENARIO::ErrorLevel::WARNING);
  const auto errors = logger->GetMessagesFilteredByWorseOrEqualToErrorLevel(NET_ASAM_OPENSCENARIO::ErrorLevel::ERROR);
  return std::make_pair(errors.size(), warnings.size());
}

int SumAndPrintWarningsAndErrors(const OpenScenarioEngine::SimpleMessageLoggerPtr& message_logger,
                                 const OpenScenarioEngine::SimpleMessageLoggerPtr& catalog_logger)
{
  const auto message = detail::CountWarningsAndErrors(message_logger);
  const auto catalog = detail::CountWarningsAndErrors(catalog_logger);

  const auto total_errors = message.first + catalog.first;
  if (total_errors == 0)
  {
    Logger::Info("Validation succeeded with 0 errors, " +
                 std::to_string(message.second) + " warnings, and " +
                 std::to_string(catalog.second) + " catalog warnings");
  }
  else
  {
    Logger::Error("Validation failed with " +
                  std::to_string(message.first) + " errors, " +
                  std::to_string(catalog.first) + " catalog_errors with " +
                  std::to_string(message.second) + " warnings, and " +
                  std::to_string(catalog.second) + " catalog_warnings");
  }
  return static_cast<int>(total_errors);
}

void TraceTree(const yase::BehaviorNode& root_node)
{
  std::stringstream ss;
  yase::printTreeWithStates(root_node, ss);
  Logger::Trace(ss.str());
}

}  // namespace detail

OpenScenarioEngine::OpenScenarioEngine(const std::string& scenario_file_path,
                                       std::shared_ptr<mantle_api::IEnvironment> environment,
                                       std::shared_ptr<mantle_api::ILogger> logger)
    : scenario_file_path_(scenario_file_path),
      environment_(environment),
      entity_creator_{std::make_shared<EntityCreator>(environment)},
      controller_creator_{std::make_shared<ControllerCreator>(environment)}
{
  Logger::SetLogger(std::move(logger));
}

void OpenScenarioEngine::Init()
{
  if (!environment_)
  {
    throw std::runtime_error("Unable to initialize OpenScenarioEngine: No valid environment (nullptr)");
  }
  environment_->SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute);

  ParseScenarioFile();

  LoadRoadNetwork();

  CreateEntities();

  CreateControllers();

  CreateAndInitStoryboard();

  SetEnvironmentDefaults();
}

mantle_api::ScenarioInfo OpenScenarioEngine::GetScenarioInfo() const
{
  mantle_api::ScenarioInfo info;

  info.description = scenario_data_ptr_->GetFileHeader()->GetDescription();
  info.scenario_timeout_duration = GetDuration();
  info.additional_information.emplace("full_scenario_path", ResolveScenarioPath(scenario_file_path_));

  if (scenario_definition_ptr_->GetCatalogLocations()->GetVehicleCatalog() != nullptr)
  {
    const auto& vehicle_catalog_path =
        scenario_definition_ptr_->GetCatalogLocations()->GetVehicleCatalog()->GetDirectory()->GetPath();
    info.additional_information.emplace("vehicle_catalog_path", vehicle_catalog_path);
  }

  if (scenario_definition_ptr_->GetRoadNetwork()->GetLogicFile() != nullptr)
  {
    info.additional_information.emplace(
        "full_map_path", ResolveMapPath(scenario_definition_ptr_->GetRoadNetwork()->GetLogicFile()->GetFilepath()));
  }
  return info;
}

void OpenScenarioEngine::Step()
{
  using namespace std::chrono_literals;
  const auto nodeStatus = root_node_->executeTick();

  finished_ =
      nodeStatus == yase::NodeStatus::kFailure ||
      nodeStatus == yase::NodeStatus::kSuccess;

  if (Logger::GetCurrentLogLevel() == mantle_api::LogLevel::kTrace)
  {
    detail::TraceTree(*root_node_);
  }
}

bool OpenScenarioEngine::IsFinished() const
{
  return finished_;
}

void OpenScenarioEngine::LoadScenarioData()
{
  const std::string resolved_scenario_file_path = ResolveScenarioPath(scenario_file_path_);
  message_logger_ =
      std::make_shared<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO);
  catalog_message_logger_ =
      std::make_shared<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO);
  auto loader_factory = NET_ASAM_OPENSCENARIO::v1_1::XmlScenarioImportLoaderFactory(catalog_message_logger_,
                                                                                    resolved_scenario_file_path);
  const auto loader = loader_factory.CreateLoader(std::make_shared<NET_ASAM_OPENSCENARIO::FileResourceLocator>());

  scenario_data_ptr_ = std::static_pointer_cast<NET_ASAM_OPENSCENARIO::v1_1::IOpenScenario>(
      loader->Load(message_logger_)->GetAdapter(typeid(NET_ASAM_OPENSCENARIO::v1_1::IOpenScenario).name()));
}

void OpenScenarioEngine::ParseScenarioFile()
{
  if (ValidateScenario() > 0)
  {
    throw std::runtime_error("Scenario file contains warnings or errors.");
  }
  SetScenarioDefinitionPtr(scenario_data_ptr_);
}

int OpenScenarioEngine::ValidateScenario()
{
  LoadScenarioData();
  LogParsingMessages(message_logger_);
  LogParsingMessages(catalog_message_logger_);
  return detail::SumAndPrintWarningsAndErrors(message_logger_, catalog_message_logger_);
}

void OpenScenarioEngine::LogParsingMessages(SimpleMessageLoggerPtr message_logger)
{
  using namespace std::string_literals;

  for (auto&& log_message : message_logger->GetMessagesFilteredByWorseOrEqualToErrorLevel(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO))
  {
    const auto text_marker = log_message.GetTextmarker();
    const auto message = "(File:"s + text_marker.GetFilename() + ") "s +
                         log_message.GetMsg() + " ("s +
                         std::to_string(text_marker.GetLine()) + ","s +
                         std::to_string(text_marker.GetColumn()) + ")"s;

    switch (log_message.GetErrorLevel())
    {
      case NET_ASAM_OPENSCENARIO::ErrorLevel::DEBUG:
        Logger::Debug(message);
        break;
      case NET_ASAM_OPENSCENARIO::ErrorLevel::INFO:
        Logger::Info(message);
        break;
      case NET_ASAM_OPENSCENARIO::ErrorLevel::WARNING:
        Logger::Warning(message);
        break;
      case NET_ASAM_OPENSCENARIO::ErrorLevel::ERROR:
        Logger::Error(message);
        break;
      case NET_ASAM_OPENSCENARIO::ErrorLevel::FATAL:
        Logger::Critical(message);
        break;
      default:
        std::runtime_error("Unknown ErrorLevel of SimpleMessageLogger message");
    }
  }
}

void OpenScenarioEngine::LoadRoadNetwork()
{
  if (!scenario_definition_ptr_->GetRoadNetwork())
  {
    throw std::runtime_error("Scenario does not contain road network.");
  }

  auto road_network_ptr = scenario_definition_ptr_->GetRoadNetwork();

  // Check if, logic file is provided and load it via environment
  if (road_network_ptr->GetLogicFile() != nullptr)
  {
    mantle_api::MapDetails map_details;

    auto used_area = road_network_ptr->GetUsedArea();
    if (used_area != nullptr)
    {
      LoadUsedArea(used_area, map_details);
    }

    std::string resolved_map_path = ResolveMapPath(road_network_ptr->GetLogicFile()->GetFilepath());
    environment_->CreateMap(resolved_map_path, map_details);
  }

  // Check if, scenery file is provided and load it via environment
  if (road_network_ptr->GetSceneGraphFile() != nullptr)
  {
  }
}

void OpenScenarioEngine::LoadUsedArea(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IUsedArea> used_area,
                                      mantle_api::MapDetails& map_details)
{
  for (const auto& position : used_area->GetPositions())
  {
    const auto& geo_position = position->GetGeoPosition();
    mantle_api::LatLonPosition lat_lon_position{units::angle::radian_t(geo_position->GetLatitude()),
                                                units::angle::radian_t(geo_position->GetLongitude())};
    map_details.map_region.push_back(lat_lon_position);
  }
}

void OpenScenarioEngine::CreateEntities()
{
  if (!scenario_definition_ptr_->GetEntities())
  {
    throw std::runtime_error("Scenario does not contain entities.");
  }

  // Add scenario objects to environment
  for (auto&& scenario_object : scenario_definition_ptr_->GetEntities()->GetScenarioObjects())
  {
    if (!scenario_object)
    {
      continue;
    }

    entity_creator_->CreateEntity(scenario_object);
  }

  // TODO: What to do with the entity selections?
}

void OpenScenarioEngine::CreateControllers()
{
  for (auto&& scenario_object : scenario_definition_ptr_->GetEntities()->GetScenarioObjects())
  {
    if (!scenario_object)
    {
      continue;
    }

    controller_creator_->CreateControllers(scenario_object);
  }
}

void OpenScenarioEngine::CreateAndInitStoryboard()
{
  if (!scenario_definition_ptr_->GetStoryboard())
  {
    throw std::runtime_error("Scenario does not contain storyboard.");
  }

  auto engine_abort_flags = std::make_shared<EngineAbortFlags>(EngineAbortFlags::kNoAbort);
  root_node_ = std::make_shared<RootNode>(scenario_definition_ptr_, environment_, engine_abort_flags);
  Step();  // First step executes InitActions only
}

void OpenScenarioEngine::SetEnvironmentDefaults()
{
  environment_->SetDateTime(mantle_api::Time{43200000});
  environment_->SetWeather(GetDefaultWeather());
}

void OpenScenarioEngine::SetScenarioDefinitionPtr(OpenScenarioPtr open_scenario_ptr)
{
  if (!(open_scenario_ptr && open_scenario_ptr->GetOpenScenarioCategory() &&
        open_scenario_ptr->GetOpenScenarioCategory()->GetScenarioDefinition()))
  {
    throw std::runtime_error("Scenario file not found or file does not contain scenario definition.");
  }

  scenario_definition_ptr_ = open_scenario_ptr->GetOpenScenarioCategory()->GetScenarioDefinition();
}

mantle_api::Time OpenScenarioEngine::GetDuration() const
{
  auto storyboard = scenario_definition_ptr_->GetStoryboard();
  auto condition_groups = storyboard->GetStopTrigger()->GetConditionGroups();
  std::vector<double> durations{};
  for (const auto& condition_group : condition_groups)
  {
    auto conditions = condition_group->GetConditions();
    std::for_each(conditions.begin(), conditions.end(), [&durations](const auto& condition) {
            if (const auto& by_val_condition = condition->GetByValueCondition())
            {
                if (const auto& sim_time_condition = by_val_condition->GetSimulationTimeCondition())
                {
                    durations.push_back(sim_time_condition->GetValue());
                }
            } });
  }
  std::sort(durations.begin(), durations.end(), std::greater<double>());
  return durations.empty() ? mantle_api::Time{std::numeric_limits<double>::max()}
                           : mantle_api::Time{units::time::second_t{durations.at(0)}};
}

void OpenScenarioEngine::ActivateExternalHostControl()
{
  controller_creator_->SetExternalControllerOverride();
}

}  // namespace OPENSCENARIO
