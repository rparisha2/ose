/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/EnvironmentalConditions/weather.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <set>

namespace OPENSCENARIO
{
namespace limits
{
static constexpr const auto kAcceptableVelocityMin = units::velocity::meters_per_second_t(1e-6);
static constexpr const auto kTimeHeadwayMax = units::time::second_t(std::numeric_limits<double>::max());

}  // namespace limits

static inline bool IsDrivingFunctionControlledEntity(const std::string& entity_name)
{
  static std::set<std::string> viable_names = {"Ego", "Host"};

  // "M1:" is for compatibility with converted dSPACE Model Desk scenarios
  return viable_names.count(entity_name) != 0 || entity_name.find("M1:") != std::string::npos;
}

static inline mantle_api::Weather GetDefaultWeather()
{
  return mantle_api::Weather{mantle_api::Fog::kExcellentVisibility,
                             mantle_api::Precipitation::kNone,
                             mantle_api::Illumination::kOther,
                             // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)
                             units::concentration::percent_t{60.0},       
                             units::temperature::kelvin_t{273.15 + 15},   
                             units::pressure::pascal_t{101325.0}};        
                             // NOLINTEND(cppcoreguidelines-avoid-magic-numbers)

}

}  // namespace OPENSCENARIO
