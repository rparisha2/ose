/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <memory>
#include <string>
#include <utility>
#include <vector>

using Entity = std::string;
using Entities = std::vector<std::string>;

class EntityBroker
{
private:
  Entities entities_;
  const bool selectTriggeringEntities_;

public:
  using Ptr = std::shared_ptr<EntityBroker>;

  explicit EntityBroker(bool selectTriggeringEntities = false)
      : selectTriggeringEntities_{selectTriggeringEntities}
  {
  }

  void add(const Entity& entity)
  {
    entities_.emplace_back(entity);
  }

  void add(Entities entities)
  {
    entities_.insert(entities_.end(), entities.begin(), entities.end());
  }

  void add_triggering(const Entity &entity)
  {
    if (selectTriggeringEntities_)
    {
      add(entity);
    }
  }
  void add_triggering(Entities entities)
  {
    if (selectTriggeringEntities_)
    {
      add(std::move(entities));
    }
  }

  [[nodiscard]] Entities GetEntites() const noexcept
  {
    return entities_;
  }
};
