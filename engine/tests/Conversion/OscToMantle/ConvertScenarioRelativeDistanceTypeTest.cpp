/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRelativeDistanceType.h"

TEST(RelativeDistanceTypeTest, GivenUnkownCoorodinates)
{
  auto relativeDistanceType = OPENSCENARIO::ConvertScenarioRelativeDistanceType(NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::UNKNOWN);

  ASSERT_EQ(OPENSCENARIO::RelativeDistanceType::kUnknown, relativeDistanceType);
}

TEST(RelativeDistanceTypeTest, GivenEntityCoorodinates)
{
  auto relativeDistanceType = OPENSCENARIO::ConvertScenarioRelativeDistanceType(NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::CARTESIAN_DISTANCE);

  ASSERT_EQ(OPENSCENARIO::RelativeDistanceType::kCartesian_distance, relativeDistanceType);
}

TEST(RelativeDistanceTypeTest, GivenLaneCoorodinates)
{
  auto relativeDistanceType = OPENSCENARIO::ConvertScenarioRelativeDistanceType(NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::EUCLIDIAN_DISTANCE);

  ASSERT_EQ(OPENSCENARIO::RelativeDistanceType::kEuclidian_distance, relativeDistanceType);
}

TEST(RelativeDistanceTypeTest, GivenRoadCoorodinates)
{
  auto relativeDistanceType = OPENSCENARIO::ConvertScenarioRelativeDistanceType(NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::LATERAL);

  ASSERT_EQ(OPENSCENARIO::RelativeDistanceType::kLateral, relativeDistanceType);
}

TEST(RelativeDistanceTypeTest, GivenTrajectoryCoorodinates)
{
  auto relativeDistanceType = OPENSCENARIO::ConvertScenarioRelativeDistanceType(NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::LONGITUDINAL);

  ASSERT_EQ(OPENSCENARIO::RelativeDistanceType::kLongitudinal, relativeDistanceType);
}