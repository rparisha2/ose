/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Utils/Constants.h"
#include "TestUtils.h"
#include "OpenScenarioEngine/OpenScenarioEngine.h"

#include <MantleAPI/Test/test_utils.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_1/catalog/CatalogHelperV1_1.h>

#include <filesystem>

using OPENSCENARIO::OpenScenarioEngineTestBase;
using OPENSCENARIO::GetDefaultWeather;
using OPENSCENARIO::GetScenariosPath;
using namespace units::literals;

class OpenScenarioEngineTest : public OpenScenarioEngineTestBase
{
  protected:
    void SetUp() override { OpenScenarioEngineTestBase::SetUp(); }
};

TEST_F(OpenScenarioEngineTest, GivenValidScenarioFile_WhenInitialized_ThenDefaultRoutingBehaviourIsSetToRandom)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};

    EXPECT_CALL(*env_, SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute)).Times(1);

    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_NO_THROW(engine.Init());
}

TEST_F(OpenScenarioEngineTest, GivenValidScenarioFile_WhenInitializedAndSteppedOnce_ThenNoThrow)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};

    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());
    EXPECT_NO_THROW(engine.Step());
}

MATCHER_P(EqualWeather, expected_weather, "The Weather does not match the default Weather")
{
    return arg.fog == expected_weather.fog && arg.precipitation == expected_weather.precipitation &&
           arg.illumination == expected_weather.illumination && arg.humidity == expected_weather.humidity &&
           arg.temperature == expected_weather.temperature &&
           arg.atmospheric_pressure == expected_weather.atmospheric_pressure;
}

TEST_F(OpenScenarioEngineTest, GivenValidScenarioFile_WhenEnvironmentDefaultInitialized_ThenVerifyDefaultWeatherSet)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_CALL(*env_, SetWeather(EqualWeather(GetDefaultWeather()))).Times(1);
    EXPECT_NO_THROW(engine.Init());
}

TEST_F(OpenScenarioEngineTest, GivenOscVersion1_1ScenarioFile_WhenParsingScenario_ThenScenarioContainsNewFeature)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test.xosc"};

    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());

    OPENSCENARIO::OpenScenarioEngine::OpenScenarioPtr scenario_ptr = engine.GetScenario();

    ASSERT_TRUE(scenario_ptr != nullptr);
    auto file_header = scenario_ptr->GetFileHeader();
    ASSERT_TRUE(file_header != nullptr);
    auto license = file_header->GetLicense();
    ASSERT_TRUE(license != nullptr);

    EXPECT_EQ("GLWTPL", license->GetSpdxId());
}

TEST_F(OpenScenarioEngineTest, GivenOscVersion1_1ScenarioFile_WhenParsingScenario_ThenExpressionsAreResolved)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test.xosc"};

    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());

    OPENSCENARIO::OpenScenarioEngine::OpenScenarioPtr scenario_ptr = engine.GetScenario();

    ASSERT_TRUE(scenario_ptr != nullptr);
    auto open_scenario_category = scenario_ptr->GetOpenScenarioCategory();
    ASSERT_TRUE(open_scenario_category != nullptr);
    auto scenario_definition = open_scenario_category->GetScenarioDefinition();
    ASSERT_TRUE(scenario_definition != nullptr);

    // Expression in parameter declaration
    double value_parametrized = scenario_definition->GetStoryboard()
                                    ->GetInit()
                                    ->GetActions()
                                    ->GetPrivates()[0]
                                    ->GetPrivateActions()[1]
                                    ->GetLongitudinalAction()
                                    ->GetSpeedAction()
                                    ->GetSpeedActionTarget()
                                    ->GetAbsoluteTargetSpeed()
                                    ->GetValue();
    EXPECT_EQ(20.0, value_parametrized);

    // Expression in storyboard element
    auto value = scenario_definition->GetStoryboard()
                     ->GetStopTrigger()
                     ->GetConditionGroupsAtIndex(0)
                     ->GetConditionsAtIndex(0)
                     ->GetByValueCondition()
                     ->GetSimulationTimeCondition()
                     ->GetValue();
    EXPECT_EQ(300.0, value);
}

TEST_F(OpenScenarioEngineTest, GivenValidScenarioFile_WhenParsingScenario_ThenScenarioContainsHost)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};

    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_NO_THROW(engine.Init());

    OPENSCENARIO::OpenScenarioEngine::OpenScenarioPtr scenario_ptr = engine.GetScenario();

    ASSERT_TRUE(scenario_ptr != nullptr);
    const auto& scenario_objects =
        scenario_ptr->GetOpenScenarioCategory()->GetScenarioDefinition()->GetEntities()->GetScenarioObjects();

    ASSERT_EQ(1, scenario_objects.size());
    EXPECT_EQ("Ego", scenario_objects.front()->GetName());

    auto catalogue_ref = scenario_objects.front()->GetEntityObject()->GetCatalogReference();
    ASSERT_TRUE(catalogue_ref != nullptr);

    EXPECT_EQ("car_ego", catalogue_ref->GetEntryName());

    auto the_ref = catalogue_ref->GetRef();

    ASSERT_TRUE(the_ref != nullptr);
    ASSERT_TRUE(NET_ASAM_OPENSCENARIO::v1_1::CatalogHelper::IsVehicle(the_ref));
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithLogicFileReference_WhenInitScenario_ThenCreateMapIsCalled)
{
    const std::string relative_odr_file_path_{"../../Maps/xodr/ALKS_Road_Different_Curvatures.xodr"};
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_CALL(*env_, CreateMap(relative_odr_file_path_, testing::_)).Times(1);

    EXPECT_NO_THROW(engine.Init());
}

MATCHER(IsMapDetailsWithLatLon, "")
{
    const mantle_api::MapDetails& actual_map_details = arg;

    const auto& map_region = actual_map_details.map_region;
    if (map_region.size() != 2)
    {
        return false;
    }

    if (const auto* lat_lon_position = std::get_if<mantle_api::LatLonPosition>(&map_region.front()))
    {
        if (*lat_lon_position != mantle_api::LatLonPosition{48.298065_deg, 11.583095_deg})
        {
            return false;
        }
    }

    if (const auto* lat_lon_position = std::get_if<mantle_api::LatLonPosition>(&map_region.back()))
    {
        if (*lat_lon_position != mantle_api::LatLonPosition{48.346524_deg, 11.720233_deg})
        {
            return false;
        }
    }

    return true;
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithNdsMap_WhenInitScenario_ThenCreateMapIsCalled)
{
    const std::string expected_nds_path{"/var/lib/hdmaps/hdmap-highway-westerneurope/ROOT.NDS"};
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) +
                               "AutomatedLaneKeepingSystemScenarios/ALKS_Scenario_4.1_1_FreeDriving_NDS_TEMPLATE.xosc"};
    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_CALL(*env_, CreateMap(expected_nds_path, IsMapDetailsWithLatLon())).Times(1);

    EXPECT_NO_THROW(engine.Init());
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithEntities_WhenInitScenario_ThenCreateIsCalledInEnvironmentForEveryEntity)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    mantle_api::MockVehicle mock_vehicle{};

    mantle_api::VehicleProperties ego_properties{};
    ego_properties.type = mantle_api::EntityType::kVehicle;
    ego_properties.classification = mantle_api::VehicleClass::kMedium_car;
    ego_properties.model = "medium_car";
    ego_properties.bounding_box.dimension.length = 5.0_m;
    ego_properties.bounding_box.dimension.width = 2.0_m;
    ego_properties.bounding_box.dimension.height = 1.8_m;
    ego_properties.bounding_box.geometric_center.x = 1.4_m;
    ego_properties.bounding_box.geometric_center.y = 0.0_m;
    ego_properties.bounding_box.geometric_center.z = 0.9_m;
    ego_properties.performance.max_acceleration = 10_mps_sq;
    ego_properties.performance.max_deceleration = 10_mps_sq;
    ego_properties.performance.max_speed = 70_mps;
    ego_properties.front_axle.bb_center_to_axle_center = {1.58_m, 0.0_m, -0.5_m};
    ego_properties.front_axle.max_steering = 0.5_rad;
    ego_properties.front_axle.track_width = 1.68_m;
    ego_properties.front_axle.wheel_diameter = 0.8_m;
    ego_properties.rear_axle.bb_center_to_axle_center = {-1.4_m, 0.0_m, -0.5_m};
    ego_properties.rear_axle.max_steering = 0_rad;
    ego_properties.rear_axle.track_width = 1.68_m;
    ego_properties.rear_axle.wheel_diameter = 0.8_m;
    ego_properties.is_host = true;

    EXPECT_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository()),
                Create(std::string("Ego"), ego_properties))
        .Times(1)
        .WillRepeatedly(testing::ReturnRef(mock_vehicle));

    EXPECT_NO_THROW(engine.Init());
}

TEST_F(OpenScenarioEngineTest,
       GivenScenarioWithExternallyControlledEntity_WhenInit_ThenControllerWithExternalConfigIsCreatedAndAssigned)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    mantle_api::ExternalControllerConfig external_controller_config;
    external_controller_config.name = "ALKSController";
    external_controller_config.parameters.emplace("controller_property", "3");

    EXPECT_CALL(env_->GetControllerRepository(), Create(testing::Pointee(external_controller_config))).Times(1);
    EXPECT_CALL(*env_, AddEntityToController(testing::_, 0)).Times(1);

    EXPECT_NO_THROW(engine.Init());
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithInitTeleportAction_WhenInitAndStepEngine_ThenSetPositionIsCalled)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(1);

    EXPECT_NO_THROW(engine.Init());
    EXPECT_NO_THROW(engine.Step());
}

TEST_F(OpenScenarioEngineTest, GivenExistingScenarioFileWithWrongCatalog_WhenValidateScenario_ThenReturnsNumberofErrors)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "scenario_with_wrong_catalog_path.xosc"};
    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_EQ(3, engine.ValidateScenario());
}

TEST_F(OpenScenarioEngineTest, GivenExistingScenarioFileWithWrongCatalog_WhenValidateScenario_ThenReturnsZero)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);
    EXPECT_EQ(0, engine.ValidateScenario());
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithOneSimulationTimeStopTrigger_WhenGetDuration_ThenReturnsDuration)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test.xosc"};

    EXPECT_CALL(*env_, SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute)).Times(1);

    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_NO_THROW(engine.Init());

    EXPECT_EQ(engine.GetScenarioInfo().scenario_timeout_duration, units::time::second_t(300.0));
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithTwoSimulationTimeStopTrigger_WhenGetDuration_ThenReturnsMaxDuration)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test_TwoSimulationTimeConditions.xosc"};

    EXPECT_CALL(*env_, SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute)).Times(1);

    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_NO_THROW(engine.Init());

    EXPECT_EQ(engine.GetScenarioInfo().scenario_timeout_duration, units::time::second_t(300.0));
}

TEST_F(OpenScenarioEngineTest, GivenScenarioWithNoSimulationTimeStopTrigger_WhenGetDuration_ThenReturnsMaxDouble)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "OSC_1_1_test_NoSimulationTimeCondition.xosc"};

    EXPECT_CALL(*env_, SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute)).Times(1);

    OPENSCENARIO::OpenScenarioEngine engine(xosc_file_path, env_);

    EXPECT_NO_THROW(engine.Init());

    EXPECT_EQ(engine.GetScenarioInfo().scenario_timeout_duration, mantle_api::Time{std::numeric_limits<double>::max()});
}
