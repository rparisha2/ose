/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once
#include <openScenarioLib/generated/v1_1/impl/ApiClassImplV1_1.h>

#include <memory>

namespace OPENSCENARIO::TESTING
{

class FakePositionBuilder
{
  public:
    FakePositionBuilder();
    FakePositionBuilder& WithLanePosition(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILanePositionWriter> lane_position);
    FakePositionBuilder& WithWorldPosition(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IWorldPositionWriter> world_position);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPositionWriter> Build();

    FakePositionBuilder& WithGeoPosition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IGeoPositionWriter> geo_position);
    FakePositionBuilder& WithRelativeLanePosition(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRelativeLanePositionWriter> relative_lane_position);

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::PositionImpl> position_{};
};

class FakeLanePositionBuilder
{
  public:
    FakeLanePositionBuilder(std::string road_id, std::string lane_id, double offset, double s);
    FakeLanePositionBuilder& WithOrientation(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOrientationWriter> orientation);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILanePositionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::LanePositionImpl> lane_position_{};
};

class FakeWorldPositionBuilder
{
  public:
    FakeWorldPositionBuilder(double x, double y);
    FakeWorldPositionBuilder(double x, double y, double z, double h, double p, double r);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IWorldPositionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::WorldPositionImpl> world_position_{};
};

class FakeGeoPositionBuilder
{
  public:
    FakeGeoPositionBuilder(double latitude, double longitude);
    FakeGeoPositionBuilder& WithOrientation(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOrientationWriter> orientation);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IGeoPositionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::GeoPositionImpl> geo_position_{};
};

class FakeRelativeLanePositionBuilder
{
  public:
    FakeRelativeLanePositionBuilder(int relative_lane, double offset);
    FakeRelativeLanePositionBuilder& WithOrientation(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOrientationWriter> orientation);
    
    FakeRelativeLanePositionBuilder& WithDs(double  ds);
    FakeRelativeLanePositionBuilder& WithDsLane(double ds_lane);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRelativeLanePositionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::RelativeLanePositionImpl> relative_lane_position_{};
};

}  // namespace OPENSCENARIO::TESTING
