/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "builders/ActionBuilder.h"

#include <openScenarioLib/src/impl/NamedReferenceProxy.h>

namespace OPENSCENARIO::TESTING
{

FakeActionBuilder::FakeActionBuilder() : action_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::ActionImpl>()) {}

FakeActionBuilder& FakeActionBuilder::WithPrivateAction(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPrivateActionWriter> private_action)
{
    action_->SetPrivateAction(private_action);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IActionWriter> FakeActionBuilder::Build()
{
    return action_;
}

FakePrivateActionBuilder::FakePrivateActionBuilder()
    : private_action_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::PrivateActionImpl>())
{
}

FakePrivateActionBuilder& FakePrivateActionBuilder::WithTeleportAction(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITeleportActionWriter> teleport_action)
{
    private_action_->SetTeleportAction(teleport_action);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPrivateActionWriter> FakePrivateActionBuilder::Build()
{
    return private_action_;
}

FakeTeleportActionBuilder::FakeTeleportActionBuilder()
    : teleport_action_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TeleportActionImpl>())
{
}

FakeTeleportActionBuilder& FakeTeleportActionBuilder::WithPosition(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPositionWriter> position)
{
    teleport_action_->SetPosition(position);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITeleportActionWriter> FakeTeleportActionBuilder::Build()
{
    return teleport_action_;
}

FakeLaneChangeActionBuilder::FakeLaneChangeActionBuilder()
        : lane_change_action_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::LaneChangeActionImpl>())
{
}

FakeLaneChangeActionBuilder& FakeLaneChangeActionBuilder::WithDynamics(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITransitionDynamicsWriter> transition_dynamics)
{
    lane_change_action_->SetLaneChangeActionDynamics(transition_dynamics);
    return *this;
}

FakeLaneChangeActionBuilder& FakeLaneChangeActionBuilder::WithLaneChangeTarget(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILaneChangeTargetWriter> lane_change_action_dynamics)
{
    lane_change_action_->SetLaneChangeTarget(lane_change_action_dynamics);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILaneChangeActionWriter> FakeLaneChangeActionBuilder::Build()
{
    return lane_change_action_;
}

FakeLaneChangeTargetBuilder::FakeLaneChangeTargetBuilder()
    : lane_change_target_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::LaneChangeTargetImpl>())
{
}

FakeLaneChangeTargetBuilder& FakeLaneChangeTargetBuilder::WithRelativeTargetLane(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRelativeTargetLaneWriter> relative_target_lane)
{
    lane_change_target_->SetRelativeTargetLane(relative_target_lane);
    return *this;
}

FakeLaneChangeTargetBuilder& FakeLaneChangeTargetBuilder::WithAbsoluteTargetLane(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAbsoluteTargetLaneWriter> absolute_target_lane)
{
    lane_change_target_->SetAbsoluteTargetLane(absolute_target_lane);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILaneChangeTargetWriter> FakeLaneChangeTargetBuilder::Build()
{
    return lane_change_target_;
}

FakeRelativeTargetLaneBuilder::FakeRelativeTargetLaneBuilder()
    : relative_target_lane_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::RelativeTargetLaneImpl>())
{
}

FakeRelativeTargetLaneBuilder& FakeRelativeTargetLaneBuilder::WithEntityRef(
        const std::string entity_name)
{
    auto relative_entity =
            std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_1::IEntity>>(
                    entity_name);
    relative_target_lane_->SetEntityRef(relative_entity);
    return *this;
}

FakeRelativeTargetLaneBuilder& FakeRelativeTargetLaneBuilder::WithValue(const int value)
{
    relative_target_lane_->SetValue(value);
    return *this;
}


std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRelativeTargetLaneWriter> FakeRelativeTargetLaneBuilder::Build()
{
    return relative_target_lane_;
}

FakeAbsoluteTargetLaneBuilder::FakeAbsoluteTargetLaneBuilder()
        : absolute_target_lane_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::AbsoluteTargetLaneImpl>())
{
}

FakeAbsoluteTargetLaneBuilder& FakeAbsoluteTargetLaneBuilder::WithValue(const std::string value)
{
    absolute_target_lane_->SetValue(value);
    return *this;
}


std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAbsoluteTargetLaneWriter> FakeAbsoluteTargetLaneBuilder::Build()
{
    return absolute_target_lane_;
}

FakeSpeedActionBuilder::FakeSpeedActionBuilder()
    : speed_action_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::SpeedActionImpl>())
{
}

FakeSpeedActionBuilder& FakeSpeedActionBuilder::WithSpeedActionTarget(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ISpeedActionTargetWriter> speed_action_target)
{
    speed_action_->SetSpeedActionTarget(speed_action_target);
    return *this;
}

FakeSpeedActionBuilder& FakeSpeedActionBuilder::WithTransitionDynamics(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITransitionDynamicsWriter> transition_dynamics)
{
    speed_action_->SetSpeedActionDynamics(transition_dynamics);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ISpeedActionWriter> FakeSpeedActionBuilder::Build()
{
    return speed_action_;
}

FakeSpeedActionTargetBuilder::FakeSpeedActionTargetBuilder()
    : speed_action_target_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::SpeedActionTargetImpl>())
{
}

FakeSpeedActionTargetBuilder& FakeSpeedActionTargetBuilder::WithAbsoluteTargetSpeed(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAbsoluteTargetSpeedWriter> absolute_target_speed)
{
    speed_action_target_->SetAbsoluteTargetSpeed(absolute_target_speed);
    return *this;
}

FakeSpeedActionTargetBuilder& FakeSpeedActionTargetBuilder::WithRelativeTargetSpeed(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRelativeTargetSpeedWriter> relative_target_speed)
{
    speed_action_target_->SetRelativeTargetSpeed(relative_target_speed);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ISpeedActionTargetWriter> FakeSpeedActionTargetBuilder::Build()
{
    return speed_action_target_;
}

FakeAbsoluteTargetSpeedBuilder::FakeAbsoluteTargetSpeedBuilder(double value)
    : absolute_target_speed_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::AbsoluteTargetSpeedImpl>())
{
    absolute_target_speed_->SetValue(value);
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAbsoluteTargetSpeedWriter> FakeAbsoluteTargetSpeedBuilder::Build()
{
    return absolute_target_speed_;
}

FakeRelativeTargetSpeedBuilder::FakeRelativeTargetSpeedBuilder(
    double value,
    std::string entity_name,
    const NET_ASAM_OPENSCENARIO::v1_1::SpeedTargetValueType::SpeedTargetValueTypeEnum speed_type)
    : relative_target_speed_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::RelativeTargetSpeedImpl>())
{
    relative_target_speed_->SetValue(value);
    relative_target_speed_->SetSpeedTargetValueType(speed_type);
    auto relative_entity =
        std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_1::IEntity>>(entity_name);
    relative_target_speed_->SetEntityRef(relative_entity);
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRelativeTargetSpeedWriter> FakeRelativeTargetSpeedBuilder::Build()
{
    return relative_target_speed_;
}

FakeTransitionDynamicsBuilder::FakeTransitionDynamicsBuilder()
    : transition_dynamics_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TransitionDynamicsImpl>())
{
}

FakeTransitionDynamicsBuilder::FakeTransitionDynamicsBuilder(
    const NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::DynamicsShapeEnum dynamics_shape_enum,
    const NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::DynamicsDimensionEnum dynamics_dimension_enum,
    double value)
    : transition_dynamics_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TransitionDynamicsImpl>())
{
    transition_dynamics_->SetDynamicsShape(dynamics_shape_enum);
    transition_dynamics_->SetDynamicsDimension(dynamics_dimension_enum);
    transition_dynamics_->SetValue(value);
}

FakeTransitionDynamicsBuilder& FakeTransitionDynamicsBuilder::WithDynamicsShape(
    NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape dynamics_shape)
{
    transition_dynamics_->SetDynamicsShape(dynamics_shape);
    return *this;
}

FakeTransitionDynamicsBuilder& FakeTransitionDynamicsBuilder::WithDynamicsDimension(
    NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension dynamics_dimension)
{
    transition_dynamics_->SetDynamicsDimension(dynamics_dimension);
    return *this;
}

FakeTransitionDynamicsBuilder& FakeTransitionDynamicsBuilder::WithValue(double value)
{
    transition_dynamics_->SetValue(value);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITransitionDynamicsWriter> FakeTransitionDynamicsBuilder::Build()
{
    return transition_dynamics_;
}

FakeTrafficSignalStateActionBuilder::FakeTrafficSignalStateActionBuilder()
    : traffic_signal_state_action_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TrafficSignalStateActionImpl>())
{
}

FakeTrafficSignalStateActionBuilder& FakeTrafficSignalStateActionBuilder::WithState(const std::string& state)
{
    traffic_signal_state_action_->SetState(state);

    return *this;
}

FakeTrafficSignalStateActionBuilder& FakeTrafficSignalStateActionBuilder::WithName(const std::string& name)
{
    traffic_signal_state_action_->SetName(name);

    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalStateActionWriter>
FakeTrafficSignalStateActionBuilder::Build()
{
    return traffic_signal_state_action_;
}

FakeFollowTrajectoryActionBuilder::FakeFollowTrajectoryActionBuilder()
    : follow_trajectory_action_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::FollowTrajectoryActionImpl>())
{
}

FakeFollowTrajectoryActionBuilder& FakeFollowTrajectoryActionBuilder::WithTimeReference(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITimeReferenceWriter> time_reference)
{
    follow_trajectory_action_->SetTimeReference(time_reference);
    return *this;
}

FakeFollowTrajectoryActionBuilder& FakeFollowTrajectoryActionBuilder::WithTrajectory(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrajectoryWriter> trajectory)
{
    auto trajectory_ref = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TrajectoryRefImpl>();
    trajectory_ref->SetTrajectory(trajectory);
    follow_trajectory_action_->SetTrajectoryRef(trajectory_ref);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IFollowTrajectoryActionWriter> FakeFollowTrajectoryActionBuilder::Build()
{
    return follow_trajectory_action_;
}

FakeTimeReferenceBuilder::FakeTimeReferenceBuilder()
    : time_reference_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TimeReferenceImpl>())
{
}

FakeTimeReferenceBuilder& FakeTimeReferenceBuilder::WithTiming(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITimingWriter> timing)
{
    time_reference_->SetTiming(timing);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITimeReferenceWriter> FakeTimeReferenceBuilder::Build()
{
    return time_reference_;
}

FakeTrajectoryBuilder::FakeTrajectoryBuilder()
    : trajectory_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TrajectoryImpl>())
{
}

FakeTrajectoryBuilder::FakeTrajectoryBuilder(
      const std::string& name,
      bool closed)
    : trajectory_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TrajectoryImpl>())
{
    trajectory_->SetName(name);
    trajectory_->SetClosed(closed);
}

FakeTrajectoryBuilder& FakeTrajectoryBuilder::WithShape(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IShapeWriter> shape)
{
    trajectory_->SetShape(shape);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrajectoryWriter> FakeTrajectoryBuilder::Build()
{
    return trajectory_;
}

FakeShapeBuilder::FakeShapeBuilder()
    : shape_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::ShapeImpl>())
{
}

FakeShapeBuilder& FakeShapeBuilder::WithPolyline(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPolylineWriter> polyline)
{
    shape_->SetPolyline(polyline);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IShapeWriter> FakeShapeBuilder::Build()
{
    return shape_;
}

FakePolylineBuilder::FakePolylineBuilder()
    : polyline_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::PolylineImpl>())
{
}

FakePolylineBuilder& FakePolylineBuilder::WithVertices(
        std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IVertexWriter>> vertices)
{
    polyline_->SetVertices(vertices);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPolylineWriter> FakePolylineBuilder::Build()
{
    return polyline_;
}

FakeVertexBuilder::FakeVertexBuilder()
    : vertex_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::VertexImpl>())
{
}

FakeVertexBuilder::FakeVertexBuilder(
      double time)
    : vertex_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::VertexImpl>())
{
    vertex_->SetTime(time);
}

FakeVertexBuilder& FakeVertexBuilder::WithPosition(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPositionWriter> position)
{
    vertex_->SetPosition(position);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IVertexWriter> FakeVertexBuilder::Build()
{
    return vertex_;
}

FakeAssignRouteActionBuilder::FakeAssignRouteActionBuilder()
    : assign_route_action_impl_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::AssignRouteActionImpl>())
{
}

FakeAssignRouteActionBuilder& FakeAssignRouteActionBuilder::WithRoute(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRouteWriter> route_writer)
{
    assign_route_action_impl_->SetRoute(route_writer);
    return *this;
}

FakeAssignRouteActionBuilder& FakeAssignRouteActionBuilder::WithCatalogReference(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReferenceWriter> catalog_reference_writer)
{
    assign_route_action_impl_->SetCatalogReference(catalog_reference_writer);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAssignRouteActionWriter>
FakeAssignRouteActionBuilder::Build()
{
    return assign_route_action_impl_;
}

FakeLongitudinalDistanceActionBuilder::FakeLongitudinalDistanceActionBuilder()
    : longitudinal_distance_action_impl_(
          std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDistanceActionImpl>())
{
}

FakeLongitudinalDistanceActionBuilder& FakeLongitudinalDistanceActionBuilder::WithEntityRef(
    const std::string entity_name)
{
    auto entity_ref =
        std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_1::IEntity>>(entity_name);
    longitudinal_distance_action_impl_->SetEntityRef(entity_ref);
    return *this;
}

FakeLongitudinalDistanceActionBuilder& FakeLongitudinalDistanceActionBuilder::WithDisplacement(
    const NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LongitudinalDisplacement::LongitudinalDisplacementEnum
        displacement_target)
{
    longitudinal_distance_action_impl_->SetDisplacement(displacement_target);
    return *this;
}

FakeLongitudinalDistanceActionBuilder& FakeLongitudinalDistanceActionBuilder::WithContinuous(bool is_continuous)
{
    longitudinal_distance_action_impl_->SetContinuous(is_continuous);
    return *this;
}

FakeLongitudinalDistanceActionBuilder& FakeLongitudinalDistanceActionBuilder::WithTimeGap(double time_gap_target)
{
    longitudinal_distance_action_impl_->SetTimeGap(time_gap_target);
    return *this;
}

FakeLongitudinalDistanceActionBuilder& FakeLongitudinalDistanceActionBuilder::WithDynamicConstraints(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IDynamicConstraintsWriter> dynamic_constraint_writer)
{
    longitudinal_distance_action_impl_->SetDynamicConstraints(dynamic_constraint_writer);
    return *this;
}

FakeLongitudinalDistanceActionBuilder& FakeLongitudinalDistanceActionBuilder::WithCoordinateSystem(
    const NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::CoordinateSystem::CoordinateSystemEnum
        coordinate_system_target)
{
    longitudinal_distance_action_impl_->SetCoordinateSystem(coordinate_system_target);
    return *this;
}

FakeLongitudinalDistanceActionBuilder& FakeLongitudinalDistanceActionBuilder::WithDistance(double distance_target)
{
    longitudinal_distance_action_impl_->SetDistance(distance_target);
    return *this;
}

FakeLongitudinalDistanceActionBuilder& FakeLongitudinalDistanceActionBuilder::WithFreeSpace(bool is_freespace)
{
    longitudinal_distance_action_impl_->SetFreespace(is_freespace);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILongitudinalDistanceActionWriter>
FakeLongitudinalDistanceActionBuilder::Build()
{
    return longitudinal_distance_action_impl_;
}

FakeUserDefinedActionBuilder::FakeUserDefinedActionBuilder()
    : user_defined_action_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::UserDefinedActionImpl>())
{
}

FakeUserDefinedActionBuilder& FakeUserDefinedActionBuilder::WithCustomCommandAction(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICustomCommandActionWriter> custom_command_action)
{
    user_defined_action_->SetCustomCommandAction(custom_command_action);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IUserDefinedActionWriter> FakeUserDefinedActionBuilder::Build()
{
    return user_defined_action_;
}

FakeCustomCommandActionBuilder::FakeCustomCommandActionBuilder()
    : custom_command_action_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::CustomCommandActionImpl>())
{
}

FakeCustomCommandActionBuilder::FakeCustomCommandActionBuilder(const std::string& type, const std::string& command)
    : custom_command_action_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::CustomCommandActionImpl>())
{
    custom_command_action_->SetType(type);
    custom_command_action_->SetContent(command);
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICustomCommandActionWriter> FakeCustomCommandActionBuilder::Build()
{
    return custom_command_action_;
}
}  // namespace OPENSCENARIO::TESTING
