/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Storyboard/MotionControlAction/LaneOffsetAction_impl.h"
#include "gmock/gmock.h"

using namespace mantle_api;
using namespace units::literals;

using testing::_;
using testing::Return;
using testing::SizeIs;

TEST(LaneOffsetAction, GivenLaneOffsetActionControlStrategyElements_WhenSetUpControlStrategy_ThenCorrectControlStrategy)
{
  bool continuous;
  OPENSCENARIO::LaneOffsetActionDynamics laneOffsetActionDynamics;
  OPENSCENARIO::LaneOffsetTarget laneOffsetTarget = 1_m;
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};
  EXPECT_CALL(*mockEnvironment,
              UpdateControlStrategies(_, _))
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_1::LaneOffsetAction laneOffsetAction({std::vector<std::string>{"Vehicle1"},
                                                               continuous,
                                                               laneOffsetActionDynamics,
                                                               [t = laneOffsetTarget]() -> units::length::meter_t
                                                               { return t; }},
                                                              {mockEnvironment});

  EXPECT_NO_THROW(laneOffsetAction.SetControlStrategy());
  EXPECT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kAcquireLaneOffset);
  auto control_strategy = dynamic_cast<mantle_api::AcquireLaneOffsetControlStrategy*>(control_strategies[0].get());
  EXPECT_EQ(control_strategy->offset, laneOffsetTarget);
}
