/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioSpeedActionTarget.h"
#include "Storyboard/MotionControlAction/SpeedAction_impl.h"
#include "gmock/gmock.h"

using namespace mantle_api;
using namespace units::literals;

using testing::_;
using testing::Return;
using testing::SizeIs;

TEST(SpeedAction, GivenMatchingControlStrategyGoalIsReached_ReturnTrue)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  ON_CALL(*mockEnvironment,
          HasControlStrategyGoalBeenReached(_, mantle_api::ControlStrategyType::kFollowVelocitySpline))
      .WillByDefault(Return(true));
  TransitionDynamics speedActionDynamics;
  speedActionDynamics.shape = mantle_api::Shape::kLinear;

  OpenScenarioEngine::v1_1::SpeedAction speedAction({std::vector<std::string>{"Vehicle1"},
                                                     speedActionDynamics,
                                                     []() -> units::velocity::meters_per_second_t { return 0_mps; }},
                                                    {mockEnvironment});

  ASSERT_TRUE(speedAction.HasControlStrategyGoalBeenReached("Vehicle1"));
}

TEST(SpeedAction, GivenUnmatchingControlStrategyGoalIsReached_ReturnsFalse)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  ON_CALL(*mockEnvironment,
          HasControlStrategyGoalBeenReached(_, mantle_api::ControlStrategyType::kUndefined))
      .WillByDefault(Return(false));

  TransitionDynamics speedActionDynamics;
  speedActionDynamics.shape = mantle_api::Shape::kLinear;

  OpenScenarioEngine::v1_1::SpeedAction speedAction({std::vector<std::string>{"Vehicle1"},
                                                     speedActionDynamics,
                                                     []() -> units::velocity::meters_per_second_t { return 0_mps; }},
                                                    {mockEnvironment});

  ASSERT_FALSE(speedAction.HasControlStrategyGoalBeenReached("Vehicle1"));
}

TEST(SpeedAction, GivenkStepShapeForSpeedActionDynamics_ReturnTrue)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  ON_CALL(*mockEnvironment,
          HasControlStrategyGoalBeenReached(_, mantle_api::ControlStrategyType::kUndefined))
      .WillByDefault(Return(true));
  TransitionDynamics speedActionDynamics;
  speedActionDynamics.shape = mantle_api::Shape::kStep;

  OpenScenarioEngine::v1_1::SpeedAction speedAction({std::vector<std::string>{"Vehicle1"},
                                                     speedActionDynamics,
                                                     []() -> units::velocity::meters_per_second_t { return 0_mps; }},
                                                    {mockEnvironment});

  ASSERT_TRUE(speedAction.HasControlStrategyGoalBeenReached("Vehicle1"));
}

TEST(SpeedAction, GivenkUndefinedShapeForSpeedActionDynamics_ReturnFalse)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  ON_CALL(*mockEnvironment,
          HasControlStrategyGoalBeenReached(_, mantle_api::ControlStrategyType::kUndefined))
      .WillByDefault(Return(false));

  TransitionDynamics speedActionDynamics;
  speedActionDynamics.shape = mantle_api::Shape::kUndefined;

  OpenScenarioEngine::v1_1::SpeedAction speedAction({std::vector<std::string>{"Vehicle1"},
                                                     speedActionDynamics,
                                                     []() -> units::velocity::meters_per_second_t { return 0_mps; }},
                                                    {mockEnvironment});

  ASSERT_FALSE(speedAction.HasControlStrategyGoalBeenReached("Vehicle1"));
}

TEST(SpeedAction, GivenIsStepShapeForSpeedActionDynamics_SetControlStrategy)
{
  TransitionDynamics speedActionDynamics;
  speedActionDynamics.shape = mantle_api::Shape::kStep;

  OPENSCENARIO::SpeedActionTarget speedActionTarget = units::velocity::meters_per_second_t(3.0);
  OpenScenarioEngine::v1_1::SpeedAction speedAction({std::vector<std::string>{"Vehicle1"},
                                                     speedActionDynamics,
                                                     []() -> units::velocity::meters_per_second_t { return 0_mps; }},
                                                    {std::make_shared<mantle_api::MockEnvironment>()});

  EXPECT_NO_THROW(speedAction.SetControlStrategy());
}

TEST(SpeedAction, GivenLinearShapeAndkUndefinedDimensionForSpeedActionDynamics_ThrowError)
{
  TransitionDynamics speedActionDynamics{mantle_api::Dimension::kUndefined, mantle_api::Shape::kLinear, {}};
  OpenScenarioEngine::v1_1::SpeedAction speedAction({std::vector<std::string>{"Vehicle1"},
                                                     speedActionDynamics,
                                                     []() -> units::velocity::meters_per_second_t { return 0_mps; }},
                                                    {std::make_shared<mantle_api::MockEnvironment>()});

  EXPECT_THROW(speedAction.SetControlStrategy(), std::runtime_error);
}

TEST(SpeedAction, GivenLinearShapeAndkTimeDimensionForSpeedActionDynamics_SetControlStrategy)
{
  TransitionDynamics speedActionDynamics{mantle_api::Dimension::kTime, mantle_api::Shape::kLinear, 3.6};
  OPENSCENARIO::SpeedActionTarget speedActionTarget = 1.2_mps;

  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};
  EXPECT_CALL(*mockEnvironment,
              UpdateControlStrategies(_, _))
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_1::SpeedAction speedAction({std::vector<std::string>{"Vehicle1"},
                                                     speedActionDynamics,
                                                     [t = speedActionTarget]() -> units::velocity::meters_per_second_t { return t; }},
                                                    {mockEnvironment});

  EXPECT_NO_THROW(speedAction.SetControlStrategy());
  EXPECT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowVelocitySpline);
  auto control_strategy = dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategies[0].get());
  EXPECT_EQ(control_strategy->default_value, speedActionTarget);
  EXPECT_THAT(control_strategy->velocity_splines, testing::SizeIs(1));
  EXPECT_EQ(control_strategy->velocity_splines[0].start_time, mantle_api::Time(0));
  EXPECT_EQ(control_strategy->velocity_splines[0].end_time, mantle_api::Time(3600));
}

TEST(SpeedAction, GivenLinearShapeAndkRateDimensionForSpeedActionDynamics_SetControlStrategy)
{
  TransitionDynamics speedActionDynamics{mantle_api::Dimension::kRate, mantle_api::Shape::kLinear, 1.2};
  OPENSCENARIO::SpeedActionTarget speedActionTarget = 1.2_mps;

  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};
  EXPECT_CALL(*mockEnvironment,
              UpdateControlStrategies(_, _))
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_1::SpeedAction speedAction({std::vector<std::string>{"Vehicle1"},
                                                     speedActionDynamics,
                                                     [t = speedActionTarget]() -> units::velocity::meters_per_second_t { return t; }},
                                                    {mockEnvironment});

  EXPECT_NO_THROW(speedAction.SetControlStrategy());
  EXPECT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowVelocitySpline);
  auto control_strategy = dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategies[0].get());
  EXPECT_EQ(control_strategy->default_value, speedActionTarget);
  EXPECT_THAT(control_strategy->velocity_splines, testing::SizeIs(1));
  EXPECT_EQ(control_strategy->velocity_splines[0].start_time, mantle_api::Time(0));
  EXPECT_EQ(control_strategy->velocity_splines[0].end_time, mantle_api::Time(1000));
}

TEST(SpeedAction, GivenLinearShapeAndkDistanceDimensionForSpeedActionDynamics_SetControlStrategy)
{
  TransitionDynamics speedActionDynamics{mantle_api::Dimension::kDistance, mantle_api::Shape::kLinear, 0.6};
  OPENSCENARIO::SpeedActionTarget speedActionTarget = 1.2_mps;

  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};
  EXPECT_CALL(*mockEnvironment,
              UpdateControlStrategies(_, _))
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_1::SpeedAction speedAction({std::vector<std::string>{"Vehicle1"},
                                                     speedActionDynamics,
                                                     [t = speedActionTarget]() -> units::velocity::meters_per_second_t { return t; }},
                                                    {mockEnvironment});

  EXPECT_NO_THROW(speedAction.SetControlStrategy());
  EXPECT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowVelocitySpline);
  auto control_strategy = dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategies[0].get());
  EXPECT_EQ(control_strategy->default_value, speedActionTarget);
  EXPECT_THAT(control_strategy->velocity_splines, testing::SizeIs(1));
  EXPECT_EQ(control_strategy->velocity_splines[0].start_time, mantle_api::Time(0));
  EXPECT_EQ(control_strategy->velocity_splines[0].end_time, mantle_api::Time(1000));
}

TEST(SpeedAction, WithCubicShapeAndRate_SetsControlStrategy)
{
  TransitionDynamics speedActionDynamics{mantle_api::Dimension::kRate, mantle_api::Shape::kCubic, 1.2};
  OPENSCENARIO::SpeedActionTarget speedActionTarget = 1.2_mps;

  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};
  EXPECT_CALL(*mockEnvironment, UpdateControlStrategies(_, _))
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_1::SpeedAction speedAction({std::vector<std::string>{"Vehicle1"},
                                                     speedActionDynamics,
                                                     [t = speedActionTarget]() -> units::velocity::meters_per_second_t { return t; }},
                                                    {mockEnvironment});

  EXPECT_NO_THROW(speedAction.SetControlStrategy());
  EXPECT_THAT(control_strategies, SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowVelocitySpline);
  auto control_strategy = dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategies[0].get());
  EXPECT_EQ(control_strategy->default_value, speedActionTarget);
  EXPECT_THAT(control_strategy->velocity_splines, SizeIs(1));
  EXPECT_EQ(control_strategy->velocity_splines[0].start_time, mantle_api::Time(0));
  EXPECT_EQ(control_strategy->velocity_splines[0].end_time, mantle_api::Time(1000));
}

TEST(SpeedAction, WithCubicShapeAndTime_SetsControlStrategy)
{
  TransitionDynamics speedActionDynamics{mantle_api::Dimension::kTime, mantle_api::Shape::kCubic, 1.2};
  OPENSCENARIO::SpeedActionTarget speedActionTarget = 1.2_mps;

  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};
  EXPECT_CALL(*mockEnvironment, UpdateControlStrategies(_, _))
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_1::SpeedAction speedAction({std::vector<std::string>{"Vehicle1"},
                                                     speedActionDynamics,
                                                     [t = speedActionTarget]() -> units::velocity::meters_per_second_t { return t; }},
                                                    {mockEnvironment});

  EXPECT_NO_THROW(speedAction.SetControlStrategy());
  EXPECT_THAT(control_strategies, SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowVelocitySpline);
  auto control_strategy = dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategies[0].get());
  EXPECT_EQ(control_strategy->default_value, speedActionTarget);
  EXPECT_THAT(control_strategy->velocity_splines, SizeIs(1));
  EXPECT_EQ(control_strategy->velocity_splines[0].start_time, mantle_api::Time(0));
  EXPECT_EQ(control_strategy->velocity_splines[0].end_time, mantle_api::Time(1200));
}

TEST(SpeedAction, WithCubicShapeAndDistance_SetsControlStrategy)
{
  TransitionDynamics speedActionDynamics{mantle_api::Dimension::kDistance, mantle_api::Shape::kCubic, 0.6};
  OPENSCENARIO::SpeedActionTarget speedActionTarget = 1.2_mps;

  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};
  EXPECT_CALL(*mockEnvironment,
              UpdateControlStrategies(_, _))
      .WillOnce(testing::SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_1::SpeedAction speedAction({std::vector<std::string>{"Vehicle1"},
                                                     speedActionDynamics,
                                                     [t = speedActionTarget]() -> units::velocity::meters_per_second_t { return t; }},
                                                    {mockEnvironment});

  EXPECT_NO_THROW(speedAction.SetControlStrategy());
  EXPECT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowVelocitySpline);
  auto control_strategy = dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategies[0].get());
  EXPECT_EQ(control_strategy->default_value, speedActionTarget);
  EXPECT_THAT(control_strategy->velocity_splines, testing::SizeIs(1));
  EXPECT_EQ(control_strategy->velocity_splines[0].start_time, mantle_api::Time(0));
  EXPECT_EQ(control_strategy->velocity_splines[0].end_time, mantle_api::Time(1000));
}
