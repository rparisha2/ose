/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/MotionControlAction/LongitudinalDistanceAction.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"

#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_1/impl/OpenScenarioWriterFactoryImplV1_1.h>

using namespace units::literals;
using OPENSCENARIO::OpenScenarioEngineLibraryTestBase;
using OPENSCENARIO::FakeRootNode;

class LongitudinalDistanceActionFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override
    {
        OpenScenarioEngineLibraryTestBase::SetUp();
        auto engine_abort_flags = std::make_shared<EngineAbortFlags>(EngineAbortFlags::kNoAbort);
        auto entity_broker = std::make_shared<EntityBroker>(false);
        entity_broker->add("TrafficVehicle");
        root_node_ = std::make_shared<FakeRootNode>(env_, engine_abort_flags, entity_broker);
    }

    std::shared_ptr<FakeRootNode> root_node_{nullptr};
};

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithNoDisplacementIsSet_WhenExecuteTick_ThenActorPoseIsNotUpdated)
{
    using namespace OPENSCENARIO::TESTING;

    auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDistance(10.0)
            .Build();

    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetOrientation(testing::_))
        .Times(0);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(0);

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithAnyDisplacement_WhenExecuteTick_ThenActorPoseIsNotUpdated)
{
    using namespace OPENSCENARIO::TESTING;
    auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::ANY)
            .WithDistance(10.0)
            .Build();

    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetOrientation(testing::_))
        .Times(0);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(0);

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithUnknownDisplacement_WhenExecuteTick_ThrowExeception)
{
    using namespace OPENSCENARIO::TESTING;

    // Throwing from within distributeData ends early and the expectations on this object are not verified
    testing::Mock::AllowLeak(&(env_->GetControllerRepository()));

    auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::UNKNOWN)
            .WithDistance(10.0)
            .Build();

    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    root_node_->addChild(longitudinal_distance_action);
    EXPECT_ANY_THROW(root_node_->distributeData());
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithTrailingDisplacement_WhenExecuteTick_ThenActorPoseIsUpdated)
{
    using namespace OPENSCENARIO::TESTING;

    auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::TRAILING_REFERENCED_ENTITY)
            .WithContinuous(false)
            .WithCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE)
            .WithFreeSpace(true)
            .WithDistance(10.0)
            .Build();

    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                FindLanePoseAtDistanceFrom(testing::_, testing::_, testing::_))
        .WillOnce(testing::Return(mantle_api::Pose{}));

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetOrientation(testing::_))
        .Times(1);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(1);

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithContinuousSet_WhenExecuteTick_ThenActorPoseIsNotUpdated)
{
    using namespace OPENSCENARIO::TESTING;

    auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LEADING_REFERENCED_ENTITY)
            .WithDistance(10.0)
            .WithContinuous(true)
            .Build();

    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetOrientation(testing::_))
        .Times(0);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(0);

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithDistanceIsNotSet_WhenExecuteTick_ThenThrowException)
{
    using namespace OPENSCENARIO::TESTING;

    auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LEADING_REFERENCED_ENTITY)
            .WithContinuous(false)
            .WithCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE)
            .Build();
    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetOrientation(testing::_))
        .Times(0);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(0);

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_ANY_THROW(root_node_->executeTick());
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithNegativeDistanceSet_WhenExecuteTick_ThenActorPoseIsNotUpdated)
{
    using namespace OPENSCENARIO::TESTING;

    auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LEADING_REFERENCED_ENTITY)
            .WithContinuous(false)
            .WithCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE)
            .WithDistance(-10.0)
            .Build();

    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetOrientation(testing::_))
        .Times(0);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(0);

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithTimeGapSet_WhenExecuteTick_ThenCorrectActorPoseIsUpdated)
{
    using namespace OPENSCENARIO::TESTING;

    const auto time_gap = units::time::second_t{10.0};
    const auto actor_velocity = mantle_api::Vec3<units::velocity::meters_per_second_t>{20.0_mps, 0.0_mps, 0.0_mps};
    const auto ref_velocity = mantle_api::Vec3<units::velocity::meters_per_second_t>{50.0_mps, 0.0_mps, 0.0_mps};
    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{5.0_m, 0.0_m, 0.0_m};
    const auto ref_orientation = mantle_api::Orientation3<units::angle::radian_t>{0.0_deg, 0.0_deg, 0.0_deg};
    const auto ref_pose = mantle_api::Pose{ref_position, ref_orientation};

    const auto expected_position =
        ref_position + mantle_api::Vec3<units::length::meter_t>{ref_velocity.x * time_gap, 0.0_m, 0.0_m};
    const auto expected_orientation = mantle_api::Orientation3<units::angle::radian_t>{0.0_deg, 0.0_deg, 0.0_deg};
    const auto expected_pose = mantle_api::Pose{expected_position, expected_orientation};

    const auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LEADING_REFERENCED_ENTITY)
            .WithContinuous(false)
            .WithCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE)
            .WithTimeGap(time_gap.value())
            .Build();
    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    auto& ref_entity = dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("ego").value().get());
    auto& actor_entity =
        dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("TrafficVehicle").value().get());

    // freespace not set
    EXPECT_CALL(ref_entity, GetPropertiesImpl()).Times(0);
    EXPECT_CALL(actor_entity, GetPropertiesImpl()).Times(1);

    EXPECT_CALL(ref_entity, GetPosition()).WillOnce(testing::Return(ref_position));
    EXPECT_CALL(ref_entity, GetOrientation()).WillOnce(testing::Return(ref_orientation));
    EXPECT_CALL(ref_entity, GetVelocity()).WillRepeatedly(testing::Return(ref_velocity));
    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                FindLanePoseAtDistanceFrom(ref_pose, time_gap * ref_velocity.Length(), mantle_api::Direction::kForward))
        .WillOnce(testing::Return(expected_pose));
    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                GetUpwardsShiftedLanePosition(testing::_, testing::_, true))
        .WillOnce(testing::Return(expected_position));
    EXPECT_CALL(actor_entity, SetPosition(expected_position));
    EXPECT_CALL(actor_entity, SetOrientation(expected_orientation));
    EXPECT_CALL(actor_entity, SetVelocity(ref_velocity));

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
    EXPECT_EQ(longitudinal_distance_action->status(), yase::NodeStatus::kSuccess);
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithTimeGapSetAndFreespace_WhenExecuteTick_ThenEntitiesGetPropertiesAreCalled)
{
    using namespace OPENSCENARIO::TESTING;

    const auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LEADING_REFERENCED_ENTITY)
            .WithContinuous(false)
            .WithCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE)
            .WithTimeGap(10.0)
            .WithFreeSpace(true)
            .Build();
    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                GetPropertiesImpl())
        .Times(2);

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
    EXPECT_EQ(longitudinal_distance_action->status(), yase::NodeStatus::kSuccess);
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithDynamicConstraintsSet_WhenExecuteTick_ThenActorPoseIsNotUpdated)
{
    using namespace OPENSCENARIO::TESTING;

    NET_ASAM_OPENSCENARIO::v1_1::OpenScenarioWriterFactoryImpl open_scenario_writer_factory_impl;

    auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LEADING_REFERENCED_ENTITY)
            .WithDistance(10.0)
            .WithContinuous(false)
            .WithCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE)
            .WithDynamicConstraints(open_scenario_writer_factory_impl.CreateDynamicConstraintsWriter())
            .Build();

    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetOrientation(testing::_))
        .Times(0);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(0);

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithCoordinateSystemIsNotLane_WhenExecuteTick_ThenActorPoseIsNotUpdated)
{
    using namespace OPENSCENARIO::TESTING;

    auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LEADING_REFERENCED_ENTITY)
            .WithDistance(10.0)
            .WithContinuous(false)
            .WithCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::ROAD)
            .Build();

    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetOrientation(testing::_))
        .Times(0);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(0);

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithFreeSpaceSet_WhenExecuteTick_ThenActorPoseIsUpdated)
{
    using namespace OPENSCENARIO::TESTING;

    auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LEADING_REFERENCED_ENTITY)
            .WithContinuous(false)
            .WithCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE)
            .WithFreeSpace(true)
            .WithDistance(10.0)
            .Build();

    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                FindLanePoseAtDistanceFrom(testing::_, testing::_, testing::_))
        .WillOnce(testing::Return(mantle_api::Pose{}));

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetOrientation(testing::_))
        .Times(1);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(1);

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceActionWithAllParametersSet_WhenStepAction_ThenActorPoseIsUpdated)
{
    using namespace OPENSCENARIO::TESTING;

    auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LEADING_REFERENCED_ENTITY)
            .WithContinuous(false)
            .WithCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE)
            .WithDistance(10.0)
            .Build();

    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                FindLanePoseAtDistanceFrom(testing::_, testing::_, testing::_))
        .WillOnce(testing::Return(mantle_api::Pose{}));

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetOrientation(testing::_))
        .Times(1);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(1);

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}

TEST_F(LongitudinalDistanceActionFixture,
       GivenLongitudinalDistanceAction_WhenStepAction_ThenActorSetCorrectVerticalPosition)
{
    using namespace OPENSCENARIO::TESTING;
    using units::literals::operator""_m;

    auto fake_longitudinal_distance_action =
        FakeLongitudinalDistanceActionBuilder{}
            .WithEntityRef("ego")
            .WithDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LEADING_REFERENCED_ENTITY)
            .WithContinuous(false)
            .WithCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE)
            .WithDistance(10.0)
            .Build();
    auto longitudinal_distance_action = std::make_shared<LongitudinalDistanceAction>(fake_longitudinal_distance_action);

    const auto expected_vertical_shift = 2.0_m;

    mantle_api::VehicleProperties actor_entity_properties{};
    actor_entity_properties.bounding_box.dimension.height = expected_vertical_shift * 2;

    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                FindLanePoseAtDistanceFrom(testing::_, testing::_, testing::_))
        .WillOnce(testing::Return(mantle_api::Pose{}));

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("TrafficVehicle").value().get()),
                GetPropertiesImpl())
        .WillOnce(testing::Return(&actor_entity_properties));

    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                GetUpwardsShiftedLanePosition(testing::_, expected_vertical_shift.value(), true))
        .Times(1);

    root_node_->addChild(longitudinal_distance_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}
