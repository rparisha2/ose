/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRule.h"
#include "Storyboard/ByEntityCondition/TimeToCollisionCondition_impl.h"
#include "TestUtils/TestLogger.h"

using namespace mantle_api;
using units::literals::operator""_m;
using units::literals::operator""_rad;

using testing::HasSubstr;

class TimeToCollisionConditionTestFixture : public ::testing::Test
{
protected:
  void SetUp() override
  {
    LOGGER = std::make_unique<testing::OpenScenarioEngine::TestLogger>();
    mock_environment_ = std::make_shared<MockEnvironment>();
  }

  std::shared_ptr<MockEnvironment> mock_environment_;

  OpenScenarioEngine::v1_1::TimeToCollisionCondition::Values condition_values_{
      .triggeringEntity = "vehicle1",
      .alongRoute = false,
      .freespace = false,
      .timeToCollisionConditionTarget = OPENSCENARIO::TimeToCollisionConditionTarget{},
      .coordinateSystem = OPENSCENARIO::CoordinateSystem::kEntity,
      .relativeDistanceType = OPENSCENARIO::RelativeDistanceType::kLongitudinal,
      .rule = OPENSCENARIO::Rule(NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 0.0)};

  std::unique_ptr<testing::OpenScenarioEngine::TestLogger> LOGGER;
};

TEST_F(TimeToCollisionConditionTestFixture, GivenEmptyTtcTarget_WhenCheckingCondition_ThenThrows)
{
  OpenScenarioEngine::v1_1::TimeToCollisionCondition timeToCollisionCondition(condition_values_,
                                                                              {mock_environment_});

  EXPECT_THROW(auto _ = timeToCollisionCondition.IsSatisfied(), std::runtime_error);
}

TEST_F(TimeToCollisionConditionTestFixture, GivenPoseWithTtcTarget_WhenCheckingCondition_ThenReturnsTrue)
{
  ///@todo Once TTC calculation has been properly implemented, this test needs to be updated to produce correct results
  mantle_api::Pose pose{mantle_api::Vec3<units::length::meter_t>{15_m, 0_m, 0_m}, mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad}};
  condition_values_.timeToCollisionConditionTarget = OPENSCENARIO::TimeToCollisionConditionTarget{pose};

  OpenScenarioEngine::v1_1::TimeToCollisionCondition timeToCollisionCondition(condition_values_,
                                                                              {mock_environment_});

  EXPECT_TRUE(timeToCollisionCondition.IsSatisfied());

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kWarning);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("TimeToCollision with respect to pose not implemented yet"));
}

TEST_F(TimeToCollisionConditionTestFixture, GivenInvalidPoseWithTtcTarget_WhenCheckingCondition_ThenThrows)
{
  condition_values_.timeToCollisionConditionTarget = OPENSCENARIO::TimeToCollisionConditionTarget{std::nullopt};

  OpenScenarioEngine::v1_1::TimeToCollisionCondition timeToCollisionCondition(condition_values_,
                                                                              {mock_environment_});

  EXPECT_THROW(auto _ = timeToCollisionCondition.IsSatisfied(), std::runtime_error);
}

TEST_F(TimeToCollisionConditionTestFixture, GivenEntityWithTtcTarget_WhenCheckingCondition_ThenReturnsTrue)
{
  ///@todo Once TTC calculation has been properly implemented, this test needs to be updated to produce correct results
  condition_values_.timeToCollisionConditionTarget = OPENSCENARIO::TimeToCollisionConditionTarget{"ego"};

  OpenScenarioEngine::v1_1::TimeToCollisionCondition timeToCollisionCondition(condition_values_,
                                                                              {mock_environment_});

  EXPECT_TRUE(timeToCollisionCondition.IsSatisfied());

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kWarning);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("TimeToCollision with respect to target entity not implemented yet"));
}