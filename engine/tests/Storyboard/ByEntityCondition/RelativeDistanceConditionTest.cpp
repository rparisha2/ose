/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRule.h"
#include "Storyboard/ByEntityCondition/RelativeDistanceCondition_impl.h"
#include "TestUtils/TestLogger.h"

using namespace mantle_api;
using namespace units::literals;

using testing::HasSubstr;
using testing::Return;

class RelativeDistanceConditionTestFixture : public ::testing::Test
{
protected:
  void SetUp() override
  {
    LOGGER = std::make_unique<testing::OpenScenarioEngine::TestLogger>();
    mock_environment_ = std::make_shared<MockEnvironment>();
  }

  mantle_api::MockVehicle& GET_VEHICLE_MOCK_WITH_NAME(const std::string& name)
  {
    return dynamic_cast<mantle_api::MockVehicle&>(mock_environment_->GetEntityRepository().Get(name).value().get());
  }

  std::shared_ptr<MockEnvironment> mock_environment_;
  OpenScenarioEngine::v1_1::RelativeDistanceCondition::Values condition_values_{.triggeringEntity = "vehicle1",
                                                                                .freespace = true,
                                                                                .entityRef = "Ego",
                                                                                .coordinateSystem = OPENSCENARIO::CoordinateSystem::kEntity,
                                                                                .relativeDistanceType = OPENSCENARIO::RelativeDistanceType::kLongitudinal,
                                                                                .rule = OPENSCENARIO::Rule(NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 10.0)};

  std::unique_ptr<testing::OpenScenarioEngine::TestLogger> LOGGER;
};

TEST_F(RelativeDistanceConditionTestFixture, GivenUnsupportedCoordinateSystem_WhenRelativeDistanceConditionIsCalled_ThenReturnFalse)
{
  condition_values_.coordinateSystem = OPENSCENARIO::CoordinateSystem::kUnknown;

  OpenScenarioEngine::v1_1::RelativeDistanceCondition relative_distance_condition(condition_values_,
                                                                                  {mock_environment_});

  ASSERT_FALSE(relative_distance_condition.IsSatisfied());
}

TEST_F(RelativeDistanceConditionTestFixture, GivenUnsupportedRelativeDistanceType_WhenRelativeDistanceConditionIsCalled_ThenReturnFalse)
{
  condition_values_.relativeDistanceType = OPENSCENARIO::RelativeDistanceType::kLateral;

  OpenScenarioEngine::v1_1::RelativeDistanceCondition relative_distance_condition(condition_values_,
                                                                                  {mock_environment_});

  ASSERT_FALSE(relative_distance_condition.IsSatisfied());
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionWithLongitudinalDistanceWithEntityCoordinateSystemWithFreeSpaceFalse_WhenRelativeDistanceConditionIsCalled_ThenMockVehicleIsCalled)
{
  condition_values_.freespace = false;

  auto& mock_vehicle = GET_VEHICLE_MOCK_WITH_NAME(condition_values_.entityRef);

  EXPECT_CALL(mock_vehicle, GetPosition())
      .WillOnce(Return(mantle_api::Vec3<units::length::meter_t>{5_m, 0_m, 0_m}))
      .WillOnce(Return(mantle_api::Vec3<units::length::meter_t>{15_m, 0_m, 0_m}));

  EXPECT_CALL(mock_vehicle, GetOrientation())
      .WillRepeatedly(Return(mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad}));

  OpenScenarioEngine::v1_1::RelativeDistanceCondition relative_distance_condition(condition_values_,
                                                                                  {mock_environment_});
  // Note: The actual return value of the condition is not being checked,
  // as the MockEnvironment cannot calculate relative distances correctly.
  [[maybe_unused]] auto _ = relative_distance_condition.IsSatisfied();
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionWithLongitudinalDistanceWithEntityCoordinateSystemWithFreeSpaceTrue_WhenRelativeDistanceConditionIsCalled_ThenMockVehicleIsCalled)
{
  auto& mock_vehicle = GET_VEHICLE_MOCK_WITH_NAME(condition_values_.entityRef);

  EXPECT_CALL(mock_vehicle, GetPosition())
      .WillOnce(Return(mantle_api::Vec3<units::length::meter_t>{5_m, 0_m, 0_m}))
      .WillOnce(Return(mantle_api::Vec3<units::length::meter_t>{15_m, 0_m, 0_m}));

  EXPECT_CALL(mock_vehicle, GetOrientation())
      .WillRepeatedly(Return(mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad}));

  OpenScenarioEngine::v1_1::RelativeDistanceCondition relative_distance_condition(condition_values_,
                                                                                  {mock_environment_});
  // Note: The actual return value of the condition is not being checked,
  // as the MockEnvironment cannot calculate relative distances correctly.
  [[maybe_unused]] auto _ = relative_distance_condition.IsSatisfied();
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionWithLongitudinalDistanceWithLaneCoordinateSystemWithFreeSpaceFalse_WhenRelativeDistanceConditionIsCalled_ThenPreconditionIsNotMetAndReturnsFalse)
{
  condition_values_.coordinateSystem = OPENSCENARIO::CoordinateSystem::kLane;
  condition_values_.freespace = false;
  auto rule = OPENSCENARIO::Rule(NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 4.0);
  condition_values_.rule = rule;

  OpenScenarioEngine::v1_1::RelativeDistanceCondition relative_distance_condition(condition_values_,
                                                                                  {mock_environment_});
  EXPECT_FALSE(relative_distance_condition.IsSatisfied());

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kWarning);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("Selected relativeDistanceType or coordinateSystem not implemented yet"));
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionWithLateralDistanceWithEntityCoordinateSystemWithFreeSpaceFalse_WhenRelativeDistanceConditionIsCalled_ThenPreconditionIsNotMetAndReturnsFalse)
{
  condition_values_.relativeDistanceType = OPENSCENARIO::RelativeDistanceType::kLateral;
  condition_values_.freespace = false;
  auto rule = OPENSCENARIO::Rule(NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 4.0);
  condition_values_.rule = rule;

  OpenScenarioEngine::v1_1::RelativeDistanceCondition relative_distance_condition(condition_values_,
                                                                                  {mock_environment_});
  EXPECT_FALSE(relative_distance_condition.IsSatisfied());

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kWarning);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("Selected relativeDistanceType or coordinateSystem not implemented yet"));
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionWithCartesianDistanceWithTrajectoryCoordinateSystemWithFreeSpaceFalse_WhenRelativeDistanceConditionIsCalled_ThenPreconditionIsNotMetAndReturnsFalse)
{
  condition_values_.coordinateSystem = OPENSCENARIO::CoordinateSystem::kTrajectory;
  condition_values_.freespace = false;
  condition_values_.relativeDistanceType = OPENSCENARIO::RelativeDistanceType::kCartesian_distance;
  auto rule = OPENSCENARIO::Rule(NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 5.0);
  condition_values_.rule = rule;

  OpenScenarioEngine::v1_1::RelativeDistanceCondition relative_distance_condition(condition_values_,
                                                                                  {mock_environment_});
  EXPECT_FALSE(relative_distance_condition.IsSatisfied());

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kWarning);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("Selected relativeDistanceType or coordinateSystem not implemented yet"));
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionWithEuclidianDistanceWithLaneCoordinateSystemWithFreeSpaceFalse_WhenRelativeDistanceConditionIsCalled_ThenPreConditionsNotMetAndReturnsFalse)
{
  condition_values_.coordinateSystem = OPENSCENARIO::CoordinateSystem::kLane;
  condition_values_.freespace = false;
  condition_values_.relativeDistanceType = OPENSCENARIO::RelativeDistanceType::kEuclidian_distance;
  auto rule = OPENSCENARIO::Rule(NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 5.0);
  condition_values_.rule = rule;

  OpenScenarioEngine::v1_1::RelativeDistanceCondition relative_distance_condition(condition_values_,
                                                                                  {mock_environment_});
  EXPECT_FALSE(relative_distance_condition.IsSatisfied());

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kWarning);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("Selected relativeDistanceType or coordinateSystem not implemented yet"));
}
