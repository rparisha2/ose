/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <gtest/gtest.h>

#include "Storyboard/GenericAction/CustomCommandAction.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"

using namespace units::literals;
using OPENSCENARIO::OpenScenarioEngineLibraryTestBase;
using OPENSCENARIO::FakeRootNode;

class CustomCommandActionTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override
    {
        OpenScenarioEngineLibraryTestBase::SetUp();
        auto engine_abort_flags = std::make_shared<EngineAbortFlags>(EngineAbortFlags::kNoAbort);
        auto entity_broker = std::make_shared<EntityBroker>(false);
        entity_broker->add("Ego");
        root_node_ = std::make_shared<FakeRootNode>(env_, engine_abort_flags, entity_broker);
    }

    std::shared_ptr<FakeRootNode> root_node_{nullptr};
};

TEST_F(CustomCommandActionTestFixture, GivenCustomCommandAction_WhenStepping_ThenExecutesGivenCommand)
{
    using namespace OPENSCENARIO::TESTING;
    auto fake_custom_command_action = FakeCustomCommandActionBuilder{"MyType", "MyCommand"}.Build();

    auto custom_command_action = std::make_shared<CustomCommandAction>(fake_custom_command_action);

    EXPECT_CALL(*env_, ExecuteCustomCommand({{"Ego"}}, "MyType", "MyCommand")).Times(1);

    root_node_->addChild(custom_command_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
}

TEST_F(CustomCommandActionTestFixture, GivenCustomCommandAction_WhenStepping_ThenCompletes)
{
    using namespace OPENSCENARIO::TESTING;
    auto fake_custom_command_action = FakeCustomCommandActionBuilder{"MyType", "MyCommand"}.Build();

    auto custom_command_action = std::make_shared<CustomCommandAction>(fake_custom_command_action);

    root_node_->addChild(custom_command_action);
    root_node_->distributeData();
    EXPECT_NO_THROW(root_node_->onInit());
    EXPECT_NO_THROW(root_node_->executeTick());
    EXPECT_EQ(custom_command_action->status(), yase::NodeStatus::kSuccess);
}
