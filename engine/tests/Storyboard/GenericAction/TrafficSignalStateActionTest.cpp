/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Storyboard/GenericAction/TrafficSignalStateAction_impl.h"

using namespace mantle_api;
using testing::Return;

TEST(TrafficSignalStateActionTest, GivenTrafficStateAndName_SetTrafficSignal)
{
  std::string expected_state("off");
  std::string expected_name("traffic_light");

  auto mockEnvironment = std::make_shared<MockEnvironment>();

  ON_CALL(*mockEnvironment,
          SetTrafficSignalState(expected_name, expected_state))
      .WillByDefault(Return());

  OpenScenarioEngine::v1_1::TrafficSignalStateAction trafficSignalStateAction({expected_name,
                                                                               expected_state},
                                                                              {mockEnvironment});

  ASSERT_TRUE(trafficSignalStateAction.Step());
}