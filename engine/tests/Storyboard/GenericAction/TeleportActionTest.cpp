/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "builders/ActionBuilder.h"
#include "builders/PositionBuilder.h"
#include <MantleAPI/Test/test_utils.h>
#include <openScenarioLib/generated/v1_1/impl/ApiClassImplV1_1.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioPosition.h"
#include "Storyboard/GenericAction/TeleportAction_impl.h"

using namespace mantle_api;
using namespace units::literals;
using testing::Return;

TEST(TeleportActionTest, GivenTeleportActionWithPosition_WhenStepAction_ThenConvertAndCallSetPosition)
{
  Vec3<units::length::meter_t> position{1.2_m, 2.3_m, 5.0_m};
  Pose pose{};
  pose.position = position;

  auto mockEnvironment = std::make_shared<MockEnvironment>();

  ON_CALL(dynamic_cast<MockVehicle&>(mockEnvironment->GetEntityRepository().Get("Entity1")->get()),
          SetPosition(position))
      .WillByDefault(Return());

  OpenScenarioEngine::v1_1::TeleportAction teleportAction({{"Entity1"},
                                                           [p = pose]() -> std::optional<Pose> { return p; }},
                                                          {mockEnvironment});

  ASSERT_TRUE(teleportAction.Step());
}

TEST(TeleportActionTest, GivenTeleportActionWithOrientation_WhenStepAction_ThenConvertAndCallSetOrientation)
{
  Orientation3<units::angle::radian_t> orientation{1.2_rad, 2.3_rad, 5.0_rad};
  Pose pose{};
  pose.orientation = orientation;

  auto mockEnvironment = std::make_shared<MockEnvironment>();
  ON_CALL(dynamic_cast<MockVehicle&>(mockEnvironment->GetEntityRepository().Get("Entity1")->get()),
          SetOrientation(orientation))
      .WillByDefault(Return());

  OpenScenarioEngine::v1_1::TeleportAction teleportAction({{"Entity1"},
                                                           [p = pose]() -> std::optional<Pose> { return p; }},
                                                          {mockEnvironment});

  ASSERT_TRUE(teleportAction.Step());
}

TEST(TeleportActionTest, GivenTeleportActionWithLanePosition_WhenStepAction_ThenConvertAndCallSetPosition)
{
  mantle_api::OpenDriveLanePosition open_drive_position{"1", 2, 3.0_m, 4.0_m};
  mantle_api::Position expected_position = open_drive_position;

  using namespace OPENSCENARIO::TESTING;
  auto position =
    FakePositionBuilder{}
      .WithLanePosition(FakeLanePositionBuilder(open_drive_position.road,
                                                std::to_string(open_drive_position.lane),
                                                open_drive_position.t_offset(),
                                                open_drive_position.s_offset())
                          .Build())
      .Build();

  auto mock_environment = std::make_shared<MockEnvironment>();
  const auto pose = OPENSCENARIO::ConvertScenarioPosition(mock_environment, position);

  OpenScenarioEngine::v1_1::TeleportAction teleport_action({{"Ego"},
                                                            [p = pose]() -> std::optional<Pose> { return p; }},
                                                           {mock_environment});

  EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(mock_environment->GetEntityRepository().Get("")->get()),
              SetPosition(testing::_))
      .Times(1);

  EXPECT_NO_THROW(teleport_action.Step());
}

TEST(TeleportActionTest, GivenTeleportActionWithLanePositionAndVerticalOffset_WhenStepAction_ThenConvertAndCallSetPosition)
{
  mantle_api::OpenDriveLanePosition open_drive_position{"1", 2, 3.0_m, 4.0_m};

  mantle_api::StaticObjectProperties traffic_sign_properties{};
  traffic_sign_properties.vertical_offset = 1.0_m;
  traffic_sign_properties.type = mantle_api::EntityType::kOther;
  traffic_sign_properties.model = "traffic_sign";
  traffic_sign_properties.bounding_box.dimension.length = 1.0_m;
  traffic_sign_properties.bounding_box.dimension.width = 1.0_m;
  traffic_sign_properties.bounding_box.dimension.height = 1.0_m;

  using namespace OPENSCENARIO::TESTING;
  auto position =
    FakePositionBuilder{}
      .WithLanePosition(FakeLanePositionBuilder(open_drive_position.road,
                                                std::to_string(open_drive_position.lane),
                                                open_drive_position.t_offset(),
                                                open_drive_position.s_offset())
                          .Build())
      .Build();

  auto mock_environment = std::make_shared<MockEnvironment>();

  auto static_object = std::make_unique<mantle_api::MockStaticObject>();
  auto static_object_properties = std::make_unique<mantle_api::StaticObjectProperties>(traffic_sign_properties);

  const auto pose = OPENSCENARIO::ConvertScenarioPosition(mock_environment, position);

  OpenScenarioEngine::v1_1::TeleportAction teleport_action({{"traffic_sign"},
                                                              [p = pose]() -> std::optional<Pose> { return p; }},
                                                           {mock_environment});

  EXPECT_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(mock_environment->GetEntityRepository()), GetImpl(testing::_))
    .WillRepeatedly(testing::Return(static_object.get()));

  EXPECT_CALL(*static_object, GetPropertiesImpl()).WillRepeatedly(testing::Return(static_object_properties.get()));

  EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(mock_environment->GetQueryService()),
              GetUpwardsShiftedLanePosition(testing::_, traffic_sign_properties.vertical_offset.value(), testing::_))
    .Times(1);

  EXPECT_CALL(*static_object, SetPosition(testing::_)).Times(1);

  EXPECT_NO_THROW(teleport_action.Step());
}

TEST(TeleportActionTest, GivenTeleportActionWithGeoPosition_WhenStepAction_ThenConvertAndCallSetPosition)
{
  mantle_api::LatLonPosition lat_lon_position{42.123_deg, 11.65874_deg};
  mantle_api::Position expected_position = lat_lon_position;

  using namespace OPENSCENARIO::TESTING;
  auto geo_position =
    FakePositionBuilder{}
      .WithGeoPosition(
        FakeGeoPositionBuilder(lat_lon_position.latitude(), lat_lon_position.longitude()).Build())
      .Build();

  auto mock_environment = std::make_shared<MockEnvironment>();

  const auto pose = OPENSCENARIO::ConvertScenarioPosition(mock_environment, geo_position);

  OpenScenarioEngine::v1_1::TeleportAction teleport_action({{"Ego"},
                                                            [p = pose]() -> std::optional<Pose> { return p; }},
                                                           {mock_environment});

  EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(mock_environment->GetEntityRepository().Get("").value().get()),
              SetPosition(testing::_))
    .Times(1);

  EXPECT_NO_THROW(teleport_action.Step());
}

TEST(TeleportActionTest,
       GivenTeleportActionWithGeoPositionContainingOrientation_WhenStepAction_ThenConvertAndCallSetOrientation)
{
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOrientationWriter> orientation_writer =
    std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::OrientationImpl>();
  orientation_writer->SetH(1.2);
  orientation_writer->SetP(2.3);
  orientation_writer->SetR(5.0);

  mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};

  mantle_api::LatLonPosition lat_lon_position{42.123_deg, 11.65874_deg};

  using namespace OPENSCENARIO::TESTING;
  auto geo_position =
    FakePositionBuilder{}
      .WithGeoPosition(FakeGeoPositionBuilder(lat_lon_position.latitude(), lat_lon_position.longitude())
                          .WithOrientation(orientation_writer)
                          .Build())
      .Build();

  auto mock_environment = std::make_shared<MockEnvironment>();
  const auto pose = OPENSCENARIO::ConvertScenarioPosition(mock_environment, geo_position);

  OpenScenarioEngine::v1_1::TeleportAction teleport_action({{"Ego"},
                                                            [p = pose]() -> std::optional<Pose> { return p; }},
                                                           {mock_environment});

  EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(mock_environment->GetEntityRepository().Get("").value().get()),
              SetOrientation(expected_orientation))
    .Times(1);

  EXPECT_NO_THROW(teleport_action.Step());
}
