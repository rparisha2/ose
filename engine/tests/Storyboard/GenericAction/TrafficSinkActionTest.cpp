/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include <limits>
#include <memory>

#include "MantleAPI/Common/i_identifiable.h"
#include "Storyboard/GenericAction/TrafficSinkAction.h"

using namespace testing;
using namespace mantle_api;
using namespace units::literals;

TEST(TrafficSinkAction, GivenEntityWithinRadius_WhenActionIsStepped_ThenEntityIsDeletedAndActionContinues)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());

  Vec3<units::length::meter_t> vehicle_position{10.1_m, 20.2_m, 30.3_m};
  auto& host_vehicle = static_cast<MockVehicle&>(mockEntityRepository.GetHost());
  ON_CALL(host_vehicle, GetPosition()).WillByDefault(Return(vehicle_position));
  ON_CALL(host_vehicle, GetUniqueId()).WillByDefault(Return(1234));

  Pose target_pose{{10.0_m, 20.0_m, 30.0_m}, {}};  // not at 0, as other mocked entities are per default at 0

  OpenScenarioEngine::v1_1::TrafficSinkAction action_under_test(
      {.radius = 10.0,
       .rate = std::numeric_limits<double>::infinity(),
       .GetPosition = [&]() { return target_pose; },
       .trafficDefinition = {}},
      {.environment = mockEnvironment});

  EXPECT_CALL(mockEntityRepository, Delete(Matcher<UniqueId>(_))).Times(0);
  EXPECT_CALL(mockEntityRepository, Delete(1234)).Times(1);
  ASSERT_FALSE(action_under_test.Step());
}

TEST(TrafficSinkAction, GivenEntityLessOrEqual1mmToPosition_WhenActionIsStepped_ThenEntityIsDeletedAndActionContinues)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());

  Vec3<units::length::meter_t> vehicle_position{1_mm,  // = EPSILON FOR DETECTION
                                                10_m,
                                                10_m};
  auto& host_vehicle = static_cast<MockVehicle&>(mockEntityRepository.GetHost());
  ON_CALL(host_vehicle, GetPosition()).WillByDefault(Return(vehicle_position));
  ON_CALL(host_vehicle, GetUniqueId()).WillByDefault(Return(1234));

  Pose target_pose{{0_m, 10_m, 10_m}, {}};  // not at 0, as other mocked entities are per default at 0
  OpenScenarioEngine::v1_1::TrafficSinkAction action_under_test(
      {.radius = 0,
       .rate = std::numeric_limits<double>::infinity(),
       .GetPosition = [&] { return target_pose; },
       .trafficDefinition = {}},
      {.environment = mockEnvironment});

  EXPECT_CALL(mockEntityRepository, Delete(Matcher<UniqueId>(_))).Times(0);
  EXPECT_CALL(mockEntityRepository, Delete(1234)).Times(1);
  ASSERT_FALSE(action_under_test.Step());
}

TEST(TrafficSinkAction, GivenEntityMoreThan1mmApartToPosition_WhenActionIsStepped_ThenEntityIsNotDeletedAndActionContinues)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());

  Vec3<units::length::meter_t> vehicle_position{1.001_mm,  // BEYOND EPSILON FOR DETECTIN
                                                10_m,
                                                10_m};
  auto& host_vehicle = static_cast<MockVehicle&>(mockEntityRepository.GetHost());
  ON_CALL(host_vehicle, GetPosition()).WillByDefault(Return(vehicle_position));
  ON_CALL(host_vehicle, GetUniqueId()).WillByDefault(Return(1234));

  Pose target_pose{{0_m, 10_m, 10_m}, {}};  // not at 0, as other mocked entities are per default at 0
  OpenScenarioEngine::v1_1::TrafficSinkAction action_under_test(
      {.radius = 0,
       .rate = std::numeric_limits<double>::infinity(),
       .GetPosition = [&] { return target_pose; },
       .trafficDefinition = {}},
      {.environment = mockEnvironment});

  EXPECT_CALL(mockEntityRepository, Delete(Matcher<UniqueId>(_))).Times(0);
  ASSERT_FALSE(action_under_test.Step());
}

TEST(TrafficSinkAction, GivenThreeEntitiesWithinRadius_WhenOneEntityIsAStaticObject_ThenStaticObjectIsNotDeleted)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();

  // the entity repository returns 1 vehicle, 1 pedestrian and 1 stationary object
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());

  Pose target_pose{{0.0_m, 0.0_m, 0.0_m}, {}};  // all mocked entities are at 0 per default!
  OpenScenarioEngine::v1_1::TrafficSinkAction action_under_test(
      {.radius = std::numeric_limits<double>::infinity(),
       .rate = std::numeric_limits<double>::infinity(),
       .GetPosition = [&] { return target_pose; },
       .trafficDefinition = {}},
      {.environment = mockEnvironment});

  EXPECT_CALL(mockEntityRepository, Delete(Matcher<UniqueId>(_))).Times(2);
  ASSERT_FALSE(action_under_test.Step());
}

TEST(TrafficSinkAction, GivenEntityOutsideOfRadius_WhenActionIsStepped_EntityIsNotDeletedAndActionContinues)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());

  Vec3<units::length::meter_t> vehicle_position{10.0_m, 20.0_m, 30.0_m};
  auto& host_vehicle = static_cast<MockVehicle&>(mockEntityRepository.GetHost());
  ON_CALL(host_vehicle, GetPosition()).WillByDefault(Return(vehicle_position));
  ON_CALL(host_vehicle, GetUniqueId()).WillByDefault(Return(1234));

  Pose target_pose{{1.0_m, 2.0_m, 3.0_m}, {}};
  OpenScenarioEngine::v1_1::TrafficSinkAction action_under_test(
      {.radius = 1.0,
       .rate = std::numeric_limits<double>::infinity(),
       .GetPosition = [&] { return target_pose; },
       .trafficDefinition = {}},
      {.environment = mockEnvironment});

  EXPECT_CALL(mockEntityRepository, Delete(Matcher<UniqueId>(_))).Times(0);
  ASSERT_FALSE(action_under_test.Step());
}

TEST(TrafficSinkAction, GivenEntityAtPositionOfAction_WhenRadiusOfActionZero_ThenEntityIsDeleted)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());

  Vec3<units::length::meter_t> vehicle_position{1.0_m, 2.0_m, 3.0_m};
  auto& host_vehicle = static_cast<MockVehicle&>(mockEntityRepository.GetHost());
  ON_CALL(host_vehicle, GetPosition()).WillByDefault(Return(vehicle_position));
  ON_CALL(host_vehicle, GetUniqueId()).WillByDefault(Return(1234));

  Pose target_pose{vehicle_position, {}};
  OpenScenarioEngine::v1_1::TrafficSinkAction action_under_test(
      {.radius = 0.0,
       .rate = std::numeric_limits<double>::infinity(),
       .GetPosition = [&] { return target_pose; },
       .trafficDefinition = {}},
      {.environment = mockEnvironment});

  EXPECT_CALL(mockEntityRepository, Delete(Matcher<UniqueId>(_))).Times(0);
  EXPECT_CALL(mockEntityRepository, Delete(1234)).Times(1);
  ASSERT_FALSE(action_under_test.Step());
}