/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Test/test_utils.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <optional>
#include <stdexcept>

#include "Storyboard/GenericAction/AcquirePositionAction_impl.h"

using testing::_;
using testing::Return;
using testing::ReturnRef;
using testing::SaveArg;
using testing::SizeIs;

using namespace mantle_api;
using namespace units::literals;
using namespace OpenScenarioEngine::v1_1;

TEST(AcquirePositionAction, Stepping_FinishesImmediately)
{
  AcquirePositionAction acquirePositionAction(
      {std::vector<std::string>{}, []() -> mantle_api::Pose { return {}; }},
      {std::make_shared<MockEnvironment>()});
  ASSERT_TRUE(acquirePositionAction.Step());
}

TEST(AcquirePositionAction, GivenValidTargetPosition_SetsRouteDefinitionFromCurrentToTargetPosition)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());
  auto& mockHostVehicle = static_cast<MockVehicle&>(mockEntityRepository.GetHost());

  RouteDefinition route_definition{};
  EXPECT_CALL(*mockEnvironment, AssignRoute(_, _)).WillOnce(SaveArg<1>(&route_definition));

  Vec3<units::length::meter_t> current_position{1.2_m, 2.3_m, 4.5_m};
  ON_CALL(mockHostVehicle, GetPosition()).WillByDefault(Return(current_position));

  Pose target_pose{{5.6_m, 6.7_m, 7.8_m}, {}};
  AcquirePositionAction acquirePositionAction(
      {std::vector<std::string>{"Vehicle1"}, [&] { return target_pose; }},
      {mockEnvironment});

  acquirePositionAction.Step();
  ASSERT_THAT(route_definition.waypoints, SizeIs(2));
  EXPECT_THAT(route_definition.waypoints[0].waypoint, current_position);
  EXPECT_THAT(route_definition.waypoints[1].waypoint, target_pose.position);
}

TEST(AcquirePositionAction, GivenInvalidTargetPosition_Throws)
{
  AcquirePositionAction acquirePositionAction(
      {{{}, [] { return std::nullopt; }},
       {std::make_shared<MockEnvironment>()}});

  ASSERT_THROW(acquirePositionAction.Step(), std::runtime_error);
}
