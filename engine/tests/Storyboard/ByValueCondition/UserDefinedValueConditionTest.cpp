/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Storyboard/ByValueCondition/UserDefinedValueCondition_impl.h"

using namespace mantle_api;
using namespace units::literals;
using testing::Return;
using testing::_;

class UserDefinedValueConditionTestFixture : public ::testing::Test
{
protected:
  void SetUp() override
  {
    mock_environment_ = std::make_shared<MockEnvironment>();
  }

  std::shared_ptr<MockEnvironment> mock_environment_;
  OpenScenarioEngine::v1_1::UserDefinedValueCondition::Values condition_values_{.name = "value_1",
                                                                                .rule = OPENSCENARIO::Rule(NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, std::string("true"))};
};

TEST_F(UserDefinedValueConditionTestFixture,
       GivenUserDefinedValueCondition_WhenValueIsNotSet_ThenIsSatisfiedReturnsFalse)
{
  OpenScenarioEngine::v1_1::UserDefinedValueCondition user_defined_values_condition(condition_values_,
                                                                                    {mock_environment_});

  EXPECT_FALSE(user_defined_values_condition.IsSatisfied());
}

TEST_F(UserDefinedValueConditionTestFixture,
       GivenUserDefinedValueCondition_WhenValueIsDifferent_ThenIsSatisfiedReturnsFalse)
{
  OpenScenarioEngine::v1_1::UserDefinedValueCondition user_defined_values_condition(condition_values_,
                                                                                    {mock_environment_});

  ON_CALL(*mock_environment_, GetUserDefinedValue(_)).WillByDefault(Return("false"));

  EXPECT_FALSE(user_defined_values_condition.IsSatisfied());
}

TEST_F(UserDefinedValueConditionTestFixture, GivenUserDefinedValueCondition_WhenValueIsSame_ThenIsSatisfiedReturnsTrue)
{
  OpenScenarioEngine::v1_1::UserDefinedValueCondition user_defined_values_condition(condition_values_,
                                                                                    {mock_environment_});

  ON_CALL(*mock_environment_, GetUserDefinedValue(_)).WillByDefault(Return("true"));

  EXPECT_TRUE(user_defined_values_condition.IsSatisfied());
}
