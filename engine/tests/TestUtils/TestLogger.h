/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <gmock/gmock.h>

#include <tuple>

#include "TestUtils/MockLogger.h"
#include "Utils/Logger.h"

using ::testing::Invoke;
using ::testing::OpenScenarioEngine::MockLogger;

namespace testing::OpenScenarioEngine
{
class TestLogger
{
public:
  /// Registers the TestLogger to the OpenScenarioEngine
  ///
  /// @param init_log_level Sets the log level, which will be received when calling GetLastLogLevel()
  ///                       As long as no calls to the logger has been made
  TestLogger(mantle_api::LogLevel init_log_level = mantle_api::LogLevel::kTrace)
      : logger_args_{init_log_level, "NO_LOGMSG_RECEIVED"}
  {
    auto mock_logger = std::make_shared<MockLogger>();
    ON_CALL(*mock_logger, Log).WillByDefault(Invoke([&](mantle_api::LogLevel l, std::string_view m) {
      logger_args_ = {l, m};
      ++logger_calls_;
    }));
    OPENSCENARIO::Logger::SetLogger(mock_logger);
  }

  ~TestLogger()
  {
    OPENSCENARIO::Logger::SetLogger(nullptr);
  }

  mantle_api::LogLevel LastLogLevel() const
  {
    return std::get<0>(logger_args_);
  }

  std::string LastLogMessage() const
  {
    return std::string(std::get<1>(logger_args_));
  }

  std::size_t Calls() const
  {
    return logger_calls_;
  }

private:
  std::size_t logger_calls_{0};
  std::tuple<mantle_api::LogLevel, std::string_view> logger_args_;
};
}  // namespace testing::OpenScenarioEngine