load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

def osc_engine_deps():
    """Load dependencies"""
    native.local_repository(
        name = "mantle_api",
        path = "deps/scenario_api/",
    )

    native.new_local_repository(
        name = "units_nhh",
        path = "deps/units/",
        build_file = "//bazel:units.BUILD",
    )

    native.local_repository(
        name = "yase",
        path = "deps/yase",
    )

    http_archive(
        name = "open_scenario_parser",
        build_file = "//bazel:openscenario_api.BUILD",
        url = "https://github.com/RA-Consulting-GmbH/openscenario.api.test/releases/download/v1.3.0/OpenSCENARIO_API_LinuxSharedRelease_2022.10.19.tgz",
        sha256 = "a15ee746f75d49291bc7068e4193d03b5c3e8bb81c7676885d207428f7536aa6",
        strip_prefix = "OpenSCENARIO_API_LinuxSharedRelease",
    )
