# OpenScenarioEngine

- [User Guide](../README.md)
- [Architecture Documentation](#Overview) (this document)
- [Generator Documentation](generator.md)

# Architecture Documentation

## Overview

Figure 1 shows, how the engine is orchestrated:

![Dependency Graph](figures/dependencies.png)

<small>_Figure 1: Dependency Graph_</small>

For **reading** and **validation** of an OpenSCENARIO description, the engine uses the [**OpenScenarioLib**](https://github.com/RA-Consulting-GmbH/openscenario.api.test/), publicly hosted by [RA-Consulting GmbH](https://www.rac.de/).
The library interfaces over the **OpenScenario API** and is available for all current OpenSCENARIO 1.X standards.

For interfacing against an environment simulator (e.g. [openPASS::opSimulation](https://gitlab.eclipse.org/eclipse/openpass/opSimulation)), the engine uses the scenario agnostic interface definition [openPASS::MantleAPI](https://gitlab.eclipse.org/eclipse/openpass/mantle-api).
In one direction, the engine implements `mantle_api::IScenarioEngine`, which allows to control the execution (e.g. `Init()` or `Step()`).
In the other direction it needs access to the `mantle_api::IEnvironment` (e.g. to setup and control entities), which needs to be implemented by the environment simulator.

As the **MantleAPI** relies on the [nholthaus/units](https://github.com/nholthaus/units) library for physically correct representation of its properties, this dependency is also injected and used by the OpenScenarioEngine.

The heart of the engine is a behavior tree, based on the openPASS project [openPASS::yase](https://gitlab.eclipse.org/eclipse/openpass/yase).
  Its responsibility is to handle **the interaction between** conditions and actions interact according to the standard, i.e. what should be triggered when.

The actual actions and conditions reside in the leaves of the tree.

The following shows an (beautified) excerpt of the such an actual behavior tree, taken from the demo example:
```
[RUNNING] - ParallelUntil[AllChildrenSucceed][OpenScenarioEngine]
|  [RUNNING] - Storyboard
|    [RUNNING] - StopTrigger
|    |  [RUNNING] - ConditionGroups
|    |    [RUNNING] - Conditions
|    |      [RUNNING] - SimulationTimeCondition
|    [RUNNING] - Child
|      [RUNNING] - ExecuteOnce
|      |  [RUNNING] - ParallelUntil[AllChildrenSucceed][InitActions]
|      |    [RUNNING] - ParallelUntil[AllChildrenSucceed][PrivateActions]
|      |      [SUCCESS] - TeleportAction
|      |      [RUNNING] - SpeedAction
...
```

# Design Principles

The "ground truth" of [ASAM **OpenSCENARIO**](https://www.asam.net/standards/detail/openscenario/) is an UML model, used to generate the XML specification, documentation, the OpenScenarioLib, and OpenSCENARIO API.
When reading an OpenSCENARIO description with the parser component of the OpenScenarioLib, it generates objects implementing the OpenSCENARIO API.
As such the (validated) information of the scenario file is available, but (a) not their logical relation and (b) no business logic.

> Major parts of the engine are generated using a python code generator, which uses the same UML model to generate a full set of concrete business logic objects that inherently know about their relationship.  
_<small>See also [Generator Documentation](generator.md)</small>_

For example, a `Condition` object holds either a `ByEntityCondition` or a `ByValueCondition` (logical relation).
Based on the current openSCENARIO description, the engine translates from pure **OpenSCENARIO API** objects into actual nodes of the internal behavior tree.
Tree generation is described in the following.

## Generation of the Behavior Tree

The heart of the engine is a behavior tree, based on the openPASS project [Yase](https://gitlab.eclipse.org/eclipse/openpass/yase).
Its responsibility is to handle the **interaction between conditions and actions** according to the standard.

**Example**

> A condition group triggers when all its conditions are satisfied, which means that the corresponding action needs to be executed after a given delay and potentially receive all triggering entities.

Thereby the behavior of the tree is parameterized by the scenario description.
In the example the user can decide whether **any** or **all** triggering entities lead to satisfaction of the corresponding condition.

**✍️ Principle**

> The engine strives to separate the interaction logic from the final conditions and actions.

The **OpenScenarioLib** is used to interpret each individual XML element of the scenario definition.
This scenario description is transformed into elements of the behavior tree.
The following example shows, how the hierarchical and parameterized structure is transformed into the final tree.

**Example**

Suppose that somewhere in the scenario definition, the user specifies a `DistanceCondition`:

```xml
<ConditionGroup>
  <Condition name="distance_reached" conditionEdge="rising" delay="0">
    <ByEntityCondition>
      <TriggeringEntities triggeringEntitiesRule="all">
        <EntityRef name="Ego">
      </TriggeringEntities>
      <DistanceCondition freespace="True" rule="lessThan" value="5">
        <Position>
          <WorldPosition x="100" y="200">
        </Position>
      </DistanceCondition>
    </ByEntityCondition>
  </Condition>
<ConditionGroup>
```

When the conversion reaches the `ConditionGroup`, the function `parse(condition)` is called, which shall act as entry point of this example (see `src/Conversion/OscToNode/ParseCondition.cpp`):

```cpp
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICondition> condition)
{
  return std::make_shared<yase::ConditionNode>(
      condition->GetName(),
      condition->GetDelay(),
      OPENSCENARIO::ConvertScenarioConditionEdge(condition->GetConditionEdge()),
      detail::resolveChildCondition(condition));
}
```

According to the standard, the condition needs to interpret the condition edge and a delay.
As such, its functionality is bundled in a specialized `yase::ConditionNode`.

Two design principles can be derived from this example:

1. **✍️ Principle**
   > Separation of Interaction

    The implementation is almost completely independent of the underlying condition.
    All it needs to know is the state of the condition (in terms of yase `success` or `running`).
    Hence, the underlying condition is again **parsed** (`resolveChildCondition` will be discussed below), which is also the term used within the engine for any translation from the context of an *OpenScenario API type** to a *YASE Node*. Corresponding functions can be found under `src/Conversion/OscToNode`.

2. **✍️ Principle**
   > Separation of Values

     The implementation should only know only what it needs to do its job.
     Passing in values of type `NET_ASAM_OPENSCENARIO::*` would violate this principle, as the *OpenScenario API type* generally offers access to more functionality than necessary and would introduce an unnecessary dependency.

     Internal values are either *MantelAPI types* or *C++17 types*, if no other option is available.
     This also makes testing lot easier.

     The functions for conversion from *OpenScenario API type** to *MantleAPI or C++17 types* are always named `ConvertScenarioTypeToBeConverted()`, here `ConvertScenarioConditionEdge()` and can be found under `src/Conversion/OscToMantle`.

The function `resolveChildCondition` (same source file) shows how the engine makes use of semantic information of the description:

```cpp
namespace detail
{
yase::BehaviorNode::Ptr resolveChildCondition(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICondition>& condition)
{
  if (auto element = condition->GetByEntityCondition(); element)
  {
    return parse(element);
  }
  if (auto element = condition->GetByValueCondition(); element)
  {
    return parse(element);
  }
  throw std::runtime_error("Corrupted openSCENARIO file: No choice made within std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICondition>");
}
}  // namespace detail
```

Here, the standard states that a condition is either a `ByValueCondition` or a `ByEntityCondition`, which is why the function simply checks one after the other.
Depending on the outcome of the checks, **parsing** simply continues with the next layer.
Note that *specifying no condition* is not allowed and that the engine uses the **OpenScenarioLib** to validate the scenario *before building the tree*.
So reaching the end of the function is literally an exception:

**✍️ Principle**
> The engine assumes that the description is complete.
> If not, exceptions are allowed.

## Handling of Union and List Types

Very often, the standard allow _xor_ (union) or _sequence_ (list) relations within elements the scenario description.
In the following, some special rules for these types are described:

### Union Types

Suppose that the user specified a `ByValueCondition`, namely a `SimulationTimeCondition`, as `Condition`.
As `ByValueCondition`s are just a **union type** without additional information, no intermediate node is generated.
Instead the result of the next `parse` is directly returned (see `ParseByValueCondition.cpp`).
In other words `SimulationTimeCondition` will become the direct child `ConditionNode`.

**✍️ Principle**
> **Skip** tree nodes for **union type**s, if they do not contain additional logic.

### List Types

For *list type*s, such as `Actions` in the `Event` element, parsing unwraps the individual elements into children of an ``yase::ParallelNode`` (see `gen/Conversion/OscToNode/ParseActions.cpp`):

```cpp
yase::BehaviorNode::Ptr parse(std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAction>> actions)
{
auto node = std::make_shared<yase::ParallelNode>("Actions");
for (const auto& action : actions)
{
    node->addChild(parse(action));
}
return node;
}
```

**✍️ Principle**
> Use `ParallelNode`s for **list types**, if they do not contain additional logic.

---

Figure 2 shows both principles at work:

![Tree Structure](figures/init_path.png)

<small>_Figure 2: Init Path_</small>

The list of `InitAction`s is transformed into individual children of the `ParallelNode` _InitActions_.
Yet, no `Action` node exist as intermediate layer.
Instead each child is directly specialized to the lowest possible occurrence (_union type_).
This could be a concrete action, such as an `EnvironmentAction`, or another intermediate node, such as an `EntityAction`.

## Conditions and Actions

Concrete conditions and actions are the leafs of the tree.
As conditions precede actions, they will be described first.

### Conditions

When the parsing process reaches a concrete condition, a leaf is instantiated.

In general, `ByEntityCondition`s and `ByValueCondition`s are supported, with `ByEntityCondition`s being an extension of `ByValueCondition`s, also including the management of triggering entities.

Note that interaction (_"who is triggering what"_) is responsibility of the tree.
This means that for `ByEntityCondition`s, the parsing stage generates an intermediate layer (`AllTriggeringEntitiesNode` or `AnyTriggeringEntitiesNode`), separating the individual triggering entities from each other.

In other words:

**✍️ Principle**

> Conditions do not know about each other and therefore a `ByEntityCondition` only refers to a single triggering entity.

The basic structure of a fictitious `ByEntityCondition` is shown in Figure 3 as an example.
For a `ByValueCondition`, entity-specific parts can simply be left out.

![Tree Structure](figures/conditions.png)

<small>_Figure 3: Composition of an Condition leaf_</small>

> **Note that this structure is autogenerated by a code generator and only the implementation files should be customized to implement the actual behavior of a condition.**

Condition leaves have two responsibilities:
1. **Interfacing w.r.t the behavior tree**  

   The entry point of the behavior tree is the `executeTick()` method of the `ActionNode` base.  
   _⚠️YASE context: Do not to confuse with an openSCENARIO Action._

   The condition node (here `FooCondition`) wraps the result in the `tick()` method, required by the base class.

   The actual implementation (`impl_`) of the condition only returns `true` or `false` and is unaware of the behavior tree, which corresponds to the _Separation of Interaction_ principle (see above).
   To achieve this, the implementation of a condition obeys the following principle:

   **✍️ Principle**
   > If an condition is satisfied, its implementation returns `true`.

2. **Instantiation of a concrete implementation**  

   Again, _Separation of Values_ (see according Principle above) is used to make the conditions independent of _OpenScenario API_ datatypes.
   Each parameter, requested by the standard is transformed into its corresponding _Mantle API (or C++)_ type into a structure called `Values`.

   The conditions base class (here `FooConditionBase`) makes all parameters available to its specialization through a struct named `value`.
   Thereby, the name of each field follows the name as defined in the openSCENARIO standard, e.g. `freespace` in a `RelativeDistanceCondition` and is accessed within the implementation as follows:

   ```cpp
   // evaluation of param "freespace" done at initialization
   if (values.freespace) { /*...*/ }
   ```

   Whenever possible, conversion is already executed at the instantiation of the implementation.
   In some cases, as for evaluation of the `Position` _in the very moment_, a parameterless lambda expression is constructed, which can be used as follows:

   ```cpp
   // evaluation of param "position" done during runtime, via GetPosition()
   std::optional<mantle_api::Pose> position = values.GetPosition();

   // or simply
   auto position = values.GetPosition();
   ```

   **✍️ Principle**
   > Within an condition, available parameters are accessed via the member `values`.


   The conditions base class (here `FooConditionBase`) also bundles all necessary interfaces w.r.t. the environment simulator in a struct `Interfaces` made available by the member `mantle`:

   ```cpp
   auto& repository = mantle.environment->GetEntityRepository();
   ```

   **✍️ Principle**
   > Within an condition, available mantle interfaces are accessed via the member `mantle`.

   To make a `ByEntityCondition` aware of the `triggeringEntity` an additional member is added to the `Values` struct, typically used as follows:

   ```cpp
   const auto& triggeringEntity = EntityUtils::GetEntityByName(mantle.environment, values.triggeringEntity);
   ```

   **✍️ Principle**
   > Within an `ByEntityCondition`, the triggering entity is accessed via the field `values.triggeringEntity`.


### Actions

In general actions are very similar to conditions.
To explain the differences, a fictitious action shall be used, that follows the fundamental leaf structure shown in Figure 4:

![Tree Structure](figures/actions.png)

<small>_Figure 4: Composition of an Action leaf_</small>

> **Note that this structure is autogenerated by a code generator and only the implementation files should be customized to implement the actual behavior of a condition.**

Action leaves have two responsibilities:
1. **Interfacing w.r.t the behavior tree**  
   As conditions, actions are and unaware of the behavior tree.
   The main difference is that actions use the method `Step()` to report their state and it has to be distinguished whether they are executed immediately or continuously:

   **✍️ Principle**
   > If an action is done, the implementation returns `true`.  
     Continuous actions **always** return `false`.

2. **Instantiation of a concrete implementation**  
    _Separation of Values_ (see according Principle above) also applies to actions and the following principles can be adopted:

   **✍️ Principles**
   > Within an action, available parameters are accessed via the member `values`.

   > Within an action, available mantle interfaces are accessed via the member `mantle`.

   The main difference is, that if an action has to be applied to a triggering entity, an additional vector `entities` is added to the `Values` struct, typically used as follows:

   ```cpp
   for (const auto& entity_name : values.entities)
   {
     const auto& entity = EntityUtils::GetEntityByName(mantle.environment, entity_name);
   }
   ```

   **✍️ Principle**
   > Within actions interacting with triggering entities, these entities are accessed via the field `values.entities`.

