################################################################################
# Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

metainfo = {
    'interfaces':
    {
        'Storyboard/MotionControlAction': [
            'FollowTrajectoryAction',
            'LaneChangeAction',
            'LaneOffsetAction',
            'LateralDistanceAction',
            'LongitudinalDistanceAction',
            'SpeedAction',
            'SynchronizeAction']
    },
    'selector':
    [
        # root, pattern, ose_type, dependencies
        ('InitActions', '\w+Action$', 'Storyboard/GenericAction', ['Environment']),
        ('Action', '\w+Action$', 'Storyboard/GenericAction', ['Environment']),
        ('ByEntityCondition', '\w+Condition$', 'Storyboard/ByEntityCondition', ['Environment']),
        ('ByValueCondition', '\w+Condition$', 'Storyboard/ByValueCondition', ['Environment'])
    ],
    'blackboard': {
        'set':
            [
                ('ManeuverGroup', 'entityBroker', 'EntityBroker::Ptr',
                 'Actors::SelectTriggeringEntities'),
                ('AbsoluteSpeed', 'environment',
                 'std::shared_ptr<mantle_api::IEnvironment>', 'ctor')
            ],
        'get':
            [
                ('RelativeLanePosition', 'environment',
                 'std::shared_ptr<mantle_api::IEnvironment>'),
                ('RelativeLanePosition', 'entityBroker', 'EntityBroker::Ptr')
            ]
    },
    'dependencies':
    {
        'Environment': {
            'name': 'environment',
            'type': 'std::shared_ptr<mantle_api::IEnvironment>',
            'include': '<MantleAPI/Execution/i_environment.h>'
        }
    },
    'converter':
    {
        'Rule':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioRule',
            'return_type': 'OPENSCENARIO::Rule<double>',
            'include': '"Conversion/OscToMantle/ConvertScenarioRule.h"',
            'type': 'static',
            'dependencies': [],
            'consumes': ['value', 'dateTime']
        },
        'CoordinateSystem':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioCoordinateSystem',
            'return_type': 'OPENSCENARIO::CoordinateSystem',
            'include': '"Conversion/OscToMantle/ConvertScenarioCoordinateSystem.h"',
            'type': 'static',
            'dependencies': [],
        },
        'RelativeDistanceType':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioRelativeDistanceType',
            'return_type': 'OPENSCENARIO::RelativeDistanceType',
            'include': '"Conversion/OscToMantle/ConvertScenarioRelativeDistanceType.h"',
            'type': 'static',
            'dependencies': [],
        },
        'Actors':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioActors',
            'return_type': 'OPENSCENARIO::Actors',
            'include': '"Conversion/OscToMantle/ConvertScenarioActors.h"',
            'type': 'static',
            'dependencies': []
        },
        'Entity':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioEntity',
            'return_type': 'OPENSCENARIO::Entity',
            'include': '"Conversion/OscToMantle/ConvertScenarioEntity.h"',
            'type': 'static',
            'dependencies': []
        },
        'EntityRef':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioEntity',
            'return_type': 'OPENSCENARIO::Entity',
            'include': '"Conversion/OscToMantle/ConvertScenarioEntity.h"',
            'type': 'static',
            'dependencies': []
        },
        'TriggeringEntities':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioEntity',
            'return_type': 'std::vector<std::string>',
            'include': '"Conversion/OscToMantle/ConvertScenarioEntity.h"',
            'type': 'static',
            'dependencies': []
        },
        'Position':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioPosition',
            'return_type': 'std::optional<mantle_api::Pose>',
            'include': '"Conversion/OscToMantle/ConvertScenarioPosition.h"',
            'type': 'dynamic',
            'dependencies':
             [
                "Environment"
             ]
        },
        'FinalSpeed':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioFinalSpeed',
            'return_type': 'mantle_api::Vec3<units::velocity::meters_per_second_t>',
            'include': '"Conversion/OscToMantle/ConvertScenarioFinalSpeed.h"',
            'type': 'static',
            'dependencies': [
                "Environment"
             ]
        },
        'Trajectory':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioTrajectory',
            'return_type': 'mantle_api::Trajectory',
            'include': '"Conversion/OscToMantle/ConvertScenarioTrajectory.h"',
            'type': 'static',
            'dependencies': []
        },
        'CatalogReferences':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioCatalogReference',
            'return_type': 'OPENSCENARIO::CatalogReferences',
            'include': '"Conversion/OscToMantle/ConvertScenarioCatalogReference.h"',
            'type': 'static',
            'dependencies': []
        },
        'CatalogReference':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioCatalogReference',
            'return_type': 'OPENSCENARIO::CatalogReference',
            'include': '"Conversion/OscToMantle/ConvertScenarioCatalogReference.h"',
            'type': 'static',
            'dependencies': []
        },
        'Controller':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioController',
            'return_type': 'OPENSCENARIO::Controller',
            'include': '"Conversion/OscToMantle/ConvertScenarioController.h"',
            'type': 'static',
            'dependencies': [],
            'consumes': ['catalogReference']
        },
        'Route':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioRoute',
            'return_type': 'OPENSCENARIO::Route',
            'include': '"Conversion/OscToMantle/ConvertScenarioRoute.h"',
            'type': 'static',
            'dependencies': [
                "Environment"
            ],
            'consumes': ['catalogReference']
        },
        'DynamicConstraints':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioDynamicConstraints',
            'return_type': 'OPENSCENARIO::DynamicConstraints',
            'include': '"Conversion/OscToMantle/ConvertScenarioDynamicConstraints.h"',
            'type': 'static',
            'dependencies': []
        },
        'TransitionDynamics':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioTransitionDynamics',
            'return_type': 'mantle_api::TransitionDynamics',
            'include': '"Conversion/OscToMantle/ConvertScenarioTransitionDynamics.h"',
            'type': 'static',
            'dependencies': []
        },
        'LaneChangeTarget':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioLaneChangeTarget',
            'return_type': 'mantle_api::UniqueId',
            'include': '"Conversion/OscToMantle/ConvertScenarioLaneChangeTarget.h"',
            'type': 'dynamic',
            'dependencies': ['Environment'],
        },
        'LaneOffsetActionDynamics':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioLaneOffsetActionDynamics',
            'return_type': 'OPENSCENARIO::LaneOffsetActionDynamics',
            'include': '"Conversion/OscToMantle/ConvertScenarioLaneOffsetActionDynamics.h"',
            'type': 'static',
            'dependencies': []
        },
        'LaneOffsetTarget':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioLaneOffsetTarget',
            'return_type': 'OPENSCENARIO::LaneOffsetTarget',
            'include': '"Conversion/OscToMantle/ConvertScenarioLaneOffsetTarget.h"',
            'type': 'static',
            'dependencies': []
        },
        'SpeedActionTarget':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioSpeedActionTarget',
            'return_type': 'OPENSCENARIO::SpeedActionTarget',
            'include': '"Conversion/OscToMantle/ConvertScenarioSpeedActionTarget.h"',
            'type': 'dynamic',
            'dependencies': [ "Environment" ]
        },
        'TimeReference':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioTimeReference',
            'return_type': 'OPENSCENARIO::TimeReference',
            'include': '"Conversion/OscToMantle/ConvertScenarioTimeReference.h"',
            'type': 'static',
            'dependencies': []
        },
        'TrajectoryFollowingMode':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioTrajectoryFollowingMode',
            'return_type': 'OPENSCENARIO::TrajectoryFollowingMode',
            'include': '"Conversion/OscToMantle/ConvertScenarioTrajectoryFollowingMode.h"',
            'type': 'static',
            'dependencies': []
        },
        'TrajectoryRef':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioTrajectoryRef',
            'return_type': 'OPENSCENARIO::TrajectoryRef',
            'include': '"Conversion/OscToMantle/ConvertScenarioTrajectoryRef.h"',
            'type': 'static',
            'dependencies': [ "Environment" ],
            'consumes': ['catalogReference']
        },
        'Environment':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioEnvironment',
            'return_type': 'OPENSCENARIO::Environment',
            'include': '"Conversion/OscToMantle/ConvertScenarioEnvironment.h"',
            'type': 'static',
            'dependencies': []
        },
        'ModifyRule':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioModifyRule',
            'return_type': 'OPENSCENARIO::ModifyRule',
            'include': '"Conversion/OscToMantle/ConvertScenarioModifyRule.h"',
            'type': 'static',
            'dependencies': []
        },
        'CentralSwarmObject':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioCentralSwarmObject',
            'return_type': 'OPENSCENARIO::CentralSwarmObject',
            'include': '"Conversion/OscToMantle/ConvertScenarioCentralSwarmObject.h"',
            'type': 'static',
            'dependencies': []
        },
       'Phase':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioPhase',
            'return_type': 'OPENSCENARIO::Phase',
            'include': '"Conversion/OscToMantle/ConvertScenarioPhase.h"',
            'type': 'static',
            'dependencies': []
       },
       'TrafficDefinition':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioTrafficDefinition',
            'return_type': 'OPENSCENARIO::TrafficDefinition',
            'include': '"Conversion/OscToMantle/ConvertScenarioTrafficDefinition.h"',
            'type': 'static',
            'dependencies': []
       },
       'TrafficSignalController':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioTrafficSignalController',
            'return_type': 'OPENSCENARIO::TrafficSignalController',
            'include': '"Conversion/OscToMantle/ConvertScenarioTrafficSignalController.h"',
            'type': 'static',
            'dependencies': []
        },
       'ByObjectType':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioByObjectType',
            'return_type': 'OPENSCENARIO::ByObjectType',
            'include': '"Conversion/OscToMantle/ConvertScenarioByObjectType.h"',
            'type': 'static',
            'dependencies': []
        },
       'TimeToCollisionConditionTarget':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioTimeToCollisionConditionTarget',
            'return_type': 'OPENSCENARIO::TimeToCollisionConditionTarget',
            'include': '"Conversion/OscToMantle/ConvertScenarioTimeToCollisionConditionTarget.h"',
            'type': 'static',
            'dependencies':
             [
                "Environment"
             ]
        },
       'StoryboardElement':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioStoryboardElement',
            'return_type': 'OPENSCENARIO::StoryboardElement',
            'include': '"Conversion/OscToMantle/ConvertScenarioStoryboardElement.h"',
            'type': 'static',
            'dependencies': []
        },
       'ParameterDeclaration':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioParameterDeclaration',
            'return_type': 'OPENSCENARIO::ParameterDeclaration',
            'include': '"Conversion/OscToMantle/ConvertScenarioParameterDeclaration.h"',
            'type': 'static',
            'dependencies': []
        },
        'dateTime':
        {
            'converter': 'OPENSCENARIO::ConvertScenarioDateTime',
            'return_type': 'OPENSCENARIO::DateTime',
            'include': '"Conversion/OscToMantle/ConvertScenarioDateTime.h"',
            'type': 'static',
            'dependencies': []
        }
    },
    'annotations':
    {
        ('Orientation', 'mantle_api::Orientation3<units::angle::radian_t>',
         '"mappings/orientation.h"'),
        ('Trajectory', 'std::string', '<string>'),
        ('File', 'std::string', '<string>')
    }
}
