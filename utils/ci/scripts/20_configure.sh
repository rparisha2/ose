#!/bin/bash

################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script configures the cmake project with its dependencies
################################################################################

# joins arguments using the cmake list separator (;)
function join_paths()
{
  local IFS=\;
  echo "$*"
}

MYDIR="$(dirname "$(readlink -f $0)")"
OPENSCENARIO_API=$MYDIR/../../../engine/deps/openscenario_api/cpp/build

mkdir -p "$MYDIR/../../../../build"
cd "$MYDIR/../../../../build" || exit 1

DEPS=(
    "$OPENSCENARIO_API/output/Linux_shared/Release"
    "$OPENSCENARIO_API/cgReleaseMakeShared/antlr4_runtime/src/antlr4_runtime/runtime/Cpp/dist"
    "$OPENSCENARIO_API/cgReleaseMakeShared/expressionsLib"
    "$OPENSCENARIO_API/cgReleaseMakeShared/openScenarioLib"

)

cmake --version
cmake \
  -D CMAKE_PREFIX_PATH="$(join_paths ${DEPS[@]})" \
  -D CMAKE_INSTALL_PREFIX="$PWD/../dist/openScenarioEngine" \
  -D USE_CCACHE=OFF \
  "$MYDIR/../../../engine"

