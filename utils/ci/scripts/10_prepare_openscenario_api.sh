#!/bin/bash

################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script prepares the openscenario api dependency
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"

#If ci clone is not recursive, uncomment below commands until line 22
cd "$MYDIR/../../.." || exit 1
git submodule init
git submodule update

# building the openscenario_api
cd $MYDIR/../../../engine/deps/openscenario_api/cpp/buildArtifact
chmod +x generateLinux.sh
./generateLinux.sh shared release make parallel
