/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ControllerCreator.h"
#include "Storyboard/Actions/SpeedAction.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"

#include <gtest/gtest.h>

namespace OPENSCENARIO
{
using namespace OPENSCENARIO::TESTING;

class SpeedActionTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override { OpenScenarioEngineLibraryTestBase::SetUp(); }
};

TEST_F(SpeedActionTestFixture,
       GivenSpeedActionWithStepDynamicsAndAbsoluteTargetSpeed_WhenStepAction_ThenCallSetVelocity)
{
    double expected_velocity{20.0};
    std::string shape_name("step");
    NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape dynamics_shape(shape_name);
    auto fake_speed_action =
        FakeSpeedActionBuilder{}
            .WithSpeedActionTarget(
                FakeSpeedActionTargetBuilder{}
                    .WithAbsoluteTargetSpeed(FakeAbsoluteTargetSpeedBuilder(expected_velocity).Build())
                    .Build())
            .WithTransitionDynamics(FakeTransitionDynamicsBuilder{}.WithDynamicsShape(dynamics_shape).Build())
            .Build();

    OPENSCENARIO::SpeedAction absolute_speed_action{fake_speed_action, env_, std::vector<std::string>{"Ego"}};

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("Ego").value().get()),
                GetOrientation())
        .WillOnce(::testing::Return(mantle_api::Orientation3<units::angle::radian_t>{90.0_deg, 0.0_deg, 0.0_deg}));

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetVelocity(testing::_))
        .Times(1)
        .WillOnce([expected_velocity](const mantle_api::Vec3<units::velocity::meters_per_second_t>& velocity) {
            // Due to conversion from deg to rad above the orientation yaw angle is not 100% accurate but the Length()
            // is
            double epsilon = 1e-10;
            EXPECT_NEAR(velocity.x(), 0, epsilon);
            EXPECT_NEAR(velocity.y(), expected_velocity, epsilon);
            EXPECT_NEAR(velocity.z(), 0, epsilon);
            EXPECT_DOUBLE_EQ(velocity.Length()(), expected_velocity);
        });

    EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_)).Times(1);

    absolute_speed_action.Start();
    absolute_speed_action.Step();
    EXPECT_TRUE(absolute_speed_action.isComplete());
}

TEST_F(SpeedActionTestFixture,
       GivenSpeedActionWithStepDynamicsAndRelativeTargetSpeedAsDelta_WhenStepAction_ThenCallSetVelocity)
{
    units::velocity::meters_per_second_t host_velocity{20};
    units::velocity::meters_per_second_t expected_relative_velocity{25};
    double relative_velocity{5};
    std::string shape_name("step");
    NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape dynamics_shape(shape_name);

    auto fake_relative_speed_action =
        FakeSpeedActionBuilder{}
            .WithSpeedActionTarget(
                FakeSpeedActionTargetBuilder{}
                    .WithRelativeTargetSpeed(
                        FakeRelativeTargetSpeedBuilder(
                            relative_velocity, "Host", NET_ASAM_OPENSCENARIO::v1_1::SpeedTargetValueType::DELTA)
                            .Build())
                    .Build())
            .WithTransitionDynamics(FakeTransitionDynamicsBuilder{}.WithDynamicsShape(dynamics_shape).Build())
            .Build();

    OPENSCENARIO::SpeedAction relative_speed_action{
        fake_relative_speed_action, env_, std::vector<std::string>{"Traffic"}};

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("Host").value().get()),
                GetVelocity())
        .WillOnce(
            ::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{host_velocity, 0.0_mps, 0.0_mps}))
        .WillOnce(
            ::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{0.0_mps, 10.0_mps, 0.0_mps}));
    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("Traffic").value().get()),
                SetVelocity(mantle_api::Vec3<units::velocity::meters_per_second_t>{
                    0.0_mps, expected_relative_velocity, 0.0_mps}))
        .Times(1);
    EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_)).Times(1);

    relative_speed_action.Start();
    relative_speed_action.Step();
    EXPECT_TRUE(relative_speed_action.isComplete());
}

TEST_F(SpeedActionTestFixture,
       GivenSpeedActionWithStepDynamicsAndRelativeTargetSpeedAsFactor_WhenStepAction_ThenCallSetVelocity)
{
    units::velocity::meters_per_second_t host_velocity{20};
    units::velocity::meters_per_second_t expected_relative_velocity{25};
    double relative_velocity_factor{1.25};
    std::string shape_name("step");
    NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape dynamics_shape(shape_name);

    auto fake_relative_speed_action =
        FakeSpeedActionBuilder{}
            .WithSpeedActionTarget(
                FakeSpeedActionTargetBuilder{}
                    .WithRelativeTargetSpeed(
                        FakeRelativeTargetSpeedBuilder(
                            relative_velocity_factor, "Host", NET_ASAM_OPENSCENARIO::v1_1::SpeedTargetValueType::FACTOR)
                            .Build())
                    .Build())
            .WithTransitionDynamics(FakeTransitionDynamicsBuilder{}.WithDynamicsShape(dynamics_shape).Build())
            .Build();

    OPENSCENARIO::SpeedAction relative_speed_action{
        fake_relative_speed_action, env_, std::vector<std::string>{"Traffic"}};

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("Host").value().get()),
                GetVelocity())
        .WillOnce(
            ::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{host_velocity, 0.0_mps, 0.0_mps}))
        .WillOnce(::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{0.0_mps, 0.0_mps, 0.0_mps}));
    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("Traffic").value().get()),
                SetVelocity(mantle_api::Vec3<units::velocity::meters_per_second_t>{
                    expected_relative_velocity, 0.0_mps, 0.0_mps}))
        .Times(1);
    EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_)).Times(1);

    relative_speed_action.Start();
    relative_speed_action.Step();
    EXPECT_TRUE(relative_speed_action.isComplete());
}

TEST_F(SpeedActionTestFixture,
       GivenSpeedActionWithNonStepDynamics_WhenStepAction_ThenOnlyCompleteAfterControlStrategyGoalReached)
{
    auto dynamics_shape_enum = NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::DynamicsShapeEnum::LINEAR;
    auto dynamics_dimension_enum = NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::DynamicsDimensionEnum::TIME;
    double expected_target_speed{5.0};
    auto parsed_speed_action =
        FakeSpeedActionBuilder{}
            .WithSpeedActionTarget(
                FakeSpeedActionTargetBuilder{}
                    .WithAbsoluteTargetSpeed(FakeAbsoluteTargetSpeedBuilder(expected_target_speed).Build())
                    .Build())
            .WithTransitionDynamics(
                FakeTransitionDynamicsBuilder(dynamics_shape_enum, dynamics_dimension_enum, 5.0).Build())
            .Build();

    OPENSCENARIO::SpeedAction speed_action{parsed_speed_action, env_, std::vector<std::string>{"Traffic"}};

    EXPECT_CALL(*env_, HasControlStrategyGoalBeenReached(0, mantle_api::ControlStrategyType::kFollowVelocitySpline))
        .Times(2)
        .WillOnce(::testing::Return(false))
        .WillRepeatedly(::testing::Return(true));
    EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_)).Times(2);
    speed_action.Start();
    speed_action.Step();
    EXPECT_FALSE(speed_action.isComplete());
    speed_action.Step();
    EXPECT_TRUE(speed_action.isComplete());
    speed_action.Step();
}

TEST_F(SpeedActionTestFixture,
       GivenSpeedActionWithLinearRateDynamics_WhenStartAction_ThenTransformedIntoCorrespondingControlStrategy)
{
    auto dynamics_shape_enum = NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::DynamicsShapeEnum::LINEAR;
    auto dynamics_dimension_enum = NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::DynamicsDimensionEnum::RATE;
    double expected_target_speed{-5.0};
    auto parsed_speed_action =
        FakeSpeedActionBuilder{}
            .WithSpeedActionTarget(
                FakeSpeedActionTargetBuilder{}
                    .WithAbsoluteTargetSpeed(FakeAbsoluteTargetSpeedBuilder(expected_target_speed).Build())
                    .Build())
            .WithTransitionDynamics(
                FakeTransitionDynamicsBuilder(dynamics_shape_enum, dynamics_dimension_enum, 5.0).Build())
            .Build();

    OPENSCENARIO::SpeedAction speed_action{parsed_speed_action, env_, std::vector<std::string>{"Traffic"}};
    auto expected_control_strategy = mantle_api::FollowVelocitySplineControlStrategy();
    expected_control_strategy.default_value = units::velocity::meters_per_second_t(expected_target_speed);
    expected_control_strategy.velocity_splines.push_back(mantle_api::SplineSection<units::velocity::meters_per_second>{
        mantle_api::Time(0),
        mantle_api::Time(1000),
        {units::unit_t<units::compound_unit<units::velocity::meters_per_second,
                                            units::inverse<units::cubed<units::time::second>>>>(0),
         units::unit_t<units::compound_unit<units::velocity::meters_per_second,
                                            units::inverse<units::squared<units::time::second>>>>(0),
         -5.0_mps_sq,
         0_mps}});

    EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_))
        .Times(1)
        .WillOnce(
            [expected_control_strategy](std::uint64_t entity_id,
                                        std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) {
                std::ignore = entity_id;
                EXPECT_EQ(1, control_strategies.size());
                auto follow_spline_control_strategy =
                    dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategies[0].get());
                EXPECT_NE(nullptr, follow_spline_control_strategy);
                EXPECT_EQ(*follow_spline_control_strategy, expected_control_strategy);
            });
    speed_action.Start();
}

TEST_F(SpeedActionTestFixture,
       GivenSpeedActionWithLinearTimeDynamics_WhenStartAction_ThenTransformedIntoCorrespondingControlStrategy)
{
    auto dynamics_shape_enum = NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::DynamicsShapeEnum::LINEAR;
    auto dynamics_dimension_enum = NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::DynamicsDimensionEnum::TIME;
    double expected_target_speed{-5.0};
    auto parsed_speed_action =
        FakeSpeedActionBuilder{}
            .WithSpeedActionTarget(
                FakeSpeedActionTargetBuilder{}
                    .WithAbsoluteTargetSpeed(FakeAbsoluteTargetSpeedBuilder(expected_target_speed).Build())
                    .Build())
            .WithTransitionDynamics(
                FakeTransitionDynamicsBuilder(dynamics_shape_enum, dynamics_dimension_enum, 5.0).Build())
            .Build();

    OPENSCENARIO::SpeedAction speed_action{parsed_speed_action, env_, std::vector<std::string>{"Traffic"}};
    auto expected_control_strategy = mantle_api::FollowVelocitySplineControlStrategy();
    expected_control_strategy.default_value = units::velocity::meters_per_second_t(expected_target_speed);
    expected_control_strategy.velocity_splines.push_back(mantle_api::SplineSection<units::velocity::meters_per_second>{
        mantle_api::Time(0),
        mantle_api::Time(5000),
        {units::unit_t<units::compound_unit<units::velocity::meters_per_second,
                                            units::inverse<units::cubed<units::time::second>>>>(0),
         units::unit_t<units::compound_unit<units::velocity::meters_per_second,
                                            units::inverse<units::squared<units::time::second>>>>(0),
         -1_mps_sq,
         0_mps}});

    ON_CALL(*env_, UpdateControlStrategies(0, testing::_))
        .WillByDefault(
            [expected_control_strategy](std::uint64_t entity_id,
                                        std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) {
                std::ignore = entity_id;
                EXPECT_EQ(1, control_strategies.size());
                auto follow_spline_control_strategy =
                    dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategies[0].get());
                EXPECT_NE(nullptr, follow_spline_control_strategy);
                EXPECT_EQ(*follow_spline_control_strategy, expected_control_strategy);
            });
    speed_action.Start();
}

TEST_F(SpeedActionTestFixture,
       GivenSpeedActionWithLinearDistanceDynamics_WhenStartAction_ThenTransformedIntoCorrespondingControlStrategy)
{
    auto dynamics_shape_enum = NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::DynamicsShapeEnum::LINEAR;
    auto dynamics_dimension_enum = NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::DynamicsDimensionEnum::DISTANCE;
    double expected_target_speed{10.0};
    double distance = 30.0;
    double expected_rate = (expected_target_speed * expected_target_speed) / (2 * distance);

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("traffic").value().get()),
                GetUniqueId())
        .WillRepeatedly(::testing::Return(mantle_api::UniqueId{13}));

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("traffic").value().get()),
                GetVelocity())
        .WillRepeatedly(
            ::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{0.0_mps, 0.0_mps, 0.0_mps}));

    auto parsed_speed_action =
        FakeSpeedActionBuilder{}
            .WithSpeedActionTarget(
                FakeSpeedActionTargetBuilder{}
                    .WithAbsoluteTargetSpeed(FakeAbsoluteTargetSpeedBuilder(expected_target_speed).Build())
                    .Build())
            .WithTransitionDynamics(
                FakeTransitionDynamicsBuilder(dynamics_shape_enum, dynamics_dimension_enum, distance).Build())
            .Build();

    OPENSCENARIO::SpeedAction speed_action{parsed_speed_action, env_, std::vector<std::string>{"Traffic"}};
    auto expected_control_strategy = mantle_api::FollowVelocitySplineControlStrategy();
    expected_control_strategy.default_value = units::velocity::meters_per_second_t(expected_target_speed);
    expected_control_strategy.velocity_splines.push_back(mantle_api::SplineSection<units::velocity::meters_per_second>{
        mantle_api::Time(0),
        mantle_api::Time(6000),
        {units::unit_t<units::compound_unit<units::velocity::meters_per_second,
                                            units::inverse<units::cubed<units::time::second>>>>(0),
         units::unit_t<units::compound_unit<units::velocity::meters_per_second,
                                            units::inverse<units::squared<units::time::second>>>>(0),
         units::acceleration::meters_per_second_squared_t(expected_rate),
         0_mps}});

    ON_CALL(*env_, UpdateControlStrategies(13, testing::_))
        .WillByDefault(
            [expected_control_strategy](std::uint64_t entity_id,
                                        std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) {
                std::ignore = entity_id;
                EXPECT_EQ(1, control_strategies.size());
                auto follow_spline_control_strategy =
                    dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategies[0].get());
                EXPECT_NE(nullptr, follow_spline_control_strategy);
                EXPECT_EQ(*follow_spline_control_strategy, expected_control_strategy);
            });
    speed_action.Start();
}

TEST_F(SpeedActionTestFixture,
       GivenSpeedActionWithCubicDynamics_WhenStartAction_ThenTransformedIntoCorrespondingControlStrategy)
{
    auto dynamics_shape_enum = NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::DynamicsShapeEnum::CUBIC;
    auto dynamics_dimension_enum = NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::DynamicsDimensionEnum::DISTANCE;
    double expected_target_speed{10.0};
    double distance = 30.0;

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("traffic").value().get()),
                GetUniqueId())
        .WillRepeatedly(::testing::Return(mantle_api::UniqueId{13}));

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("traffic").value().get()),
                GetVelocity())
        .WillRepeatedly(
            ::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{0.0_mps, 0.0_mps, 0.0_mps}));

    auto parsed_speed_action =
        FakeSpeedActionBuilder{}
            .WithSpeedActionTarget(
                FakeSpeedActionTargetBuilder{}
                    .WithAbsoluteTargetSpeed(FakeAbsoluteTargetSpeedBuilder(expected_target_speed).Build())
                    .Build())
            .WithTransitionDynamics(
                FakeTransitionDynamicsBuilder(dynamics_shape_enum, dynamics_dimension_enum, distance).Build())
            .Build();

    OPENSCENARIO::SpeedAction speed_action{parsed_speed_action, env_, std::vector<std::string>{"Traffic"}};

    ON_CALL(*env_, UpdateControlStrategies(13, testing::_))
        .WillByDefault(
            [expected_target_speed](std::uint64_t entity_id,
                                    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) {
                std::ignore = entity_id;
                EXPECT_EQ(1, control_strategies.size());
                auto follow_spline_control_strategy =
                    dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategies[0].get());
                EXPECT_NE(nullptr, follow_spline_control_strategy);
                EXPECT_DOUBLE_EQ(expected_target_speed, follow_spline_control_strategy->default_value());
            });
    speed_action.Start();
}

}  // namespace OPENSCENARIO
