/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Utils/ConvertScenarioPosition.h"
#include "TestUtils.h"
#include "builders/PositionBuilder.h"

#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

namespace OPENSCENARIO
{
using namespace units::literals;

class ConvertScenarioPositionTest : public ::testing::Test
{
  protected:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPosition> GetWorldPosition()
    {
        auto world_pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::WorldPositionImpl>();
        world_pos->SetX(1);
        world_pos->SetY(2);
        world_pos->SetZ(3);
        world_pos->SetH(4);
        world_pos->SetP(5);
        world_pos->SetR(6);
        auto pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::PositionImpl>();
        pos->SetWorldPosition(world_pos);
        return pos;
    }

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPosition> GetGeoPosition(
        std::optional<mantle_api::Orientation3<units::angle::radian_t>> opt_orientation = std::nullopt,
        NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext type =
            NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::ABSOLUTE) const
    {
        EXPECT_CALL(dynamic_cast<const mantle_api::MockConverter&>(*(env_->GetConverter())),
                    Convert(mantle_api::Position{lat_lon_position_}))
            .Times(1)
            .WillRepeatedly(testing::Return(expected_position_));

        OPENSCENARIO::TESTING::FakeGeoPositionBuilder fake_geo_position_builder{lat_lon_position_.latitude(),
                                                                                lat_lon_position_.longitude()};

        if (opt_orientation.has_value())
        {
            auto orientation_writer = GetOrientationWriter(opt_orientation.value(), type);
            fake_geo_position_builder.WithOrientation(orientation_writer);
        }

        return OPENSCENARIO::TESTING::FakePositionBuilder{}.WithGeoPosition(fake_geo_position_builder.Build()).Build();
    }

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPosition> GetLanePosition(
        std::optional<mantle_api::Orientation3<units::angle::radian_t>> opt_orientation = std::nullopt,
        NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext type =
            NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::ABSOLUTE) const
    {
        EXPECT_CALL(dynamic_cast<const mantle_api::MockConverter&>(*(env_->GetConverter())),
                    Convert(mantle_api::Position{open_drive_position_}))
            .Times(1)
            .WillRepeatedly(testing::Return(expected_position_));

        OPENSCENARIO::TESTING::FakeLanePositionBuilder fake_lane_position_builder{
            open_drive_position_.road,
            std::to_string(open_drive_position_.lane),
            open_drive_position_.t_offset(),
            open_drive_position_.s_offset()};

        if (opt_orientation.has_value())
        {
            auto orientation_writer = GetOrientationWriter(opt_orientation.value(), type);
            fake_lane_position_builder.WithOrientation(orientation_writer);
        }

        return OPENSCENARIO::TESTING::FakePositionBuilder{}
            .WithLanePosition(fake_lane_position_builder.Build())
            .Build();
    }

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPosition> GetRelativeLanePosition(
        double ds = 0.0,
        std::optional<mantle_api::Orientation3<units::angle::radian_t>> opt_orientation = std::nullopt,
        NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext type =
            NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::ABSOLUTE) const
    {
        OPENSCENARIO::TESTING::FakeRelativeLanePositionBuilder fake_relative_lane_position_builder{relative_lane_,
                                                                                                   offset_};
        if (ds > 0)
        {
            fake_relative_lane_position_builder.WithDs(ds);
        }
        else
        {
            fake_relative_lane_position_builder.WithDsLane(distance_);
        }
        if (opt_orientation.has_value())
        {
            auto orientation_writer = GetOrientationWriter(opt_orientation.value(), type);
            fake_relative_lane_position_builder.WithOrientation(orientation_writer);
        }
        return OPENSCENARIO::TESTING::FakePositionBuilder{}
            .WithRelativeLanePosition(fake_relative_lane_position_builder.Build())
            .Build();
    }

    mantle_api::LatLonPosition lat_lon_position_{42.123_deg, 11.65874_deg};
    mantle_api::OpenDriveLanePosition open_drive_position_{"1", 2, 3.0_m, 4.0_m};
    mantle_api::Vec3<units::length::meter_t> expected_position_{1_m, 2_m, 3_m};
    int relative_lane_{1};
    double distance_{10.0};
    double offset_{0.0};

    std::shared_ptr<mantle_api::MockEnvironment> env_{std::make_shared<mantle_api::MockEnvironment>()};

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOrientationWriter> GetOrientationWriter(
        mantle_api::Orientation3<units::angle::radian_t> orientation,
        NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext type) const
    {
        auto orientation_writer = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::OrientationImpl>();
        orientation_writer->SetH(orientation.yaw());
        orientation_writer->SetP(orientation.pitch());
        orientation_writer->SetR(orientation.roll());
        orientation_writer->SetType(type);

        return orientation_writer;
    }
};

TEST_F(ConvertScenarioPositionTest,
       GivenWorldPositionWithOrientation_WhenConverting_ThenPositionConvertedAndOrientationSet)
{
    auto pos = GetWorldPosition();
    const auto pose = ConvertScenarioPosition(env_, pos);

    ASSERT_TRUE(pose.has_value());
    EXPECT_EQ(expected_position_, pose.value().position);
    EXPECT_EQ(mantle_api::Orientation3<units::angle::radian_t>(4_rad, 5_rad, 6_rad), pose.value().orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGeoPositionWithNoOrientation_WhenConverting_ThenPositionConvertedAndOrientationZero)
{
    auto pos = GetGeoPosition();
    const auto pose = ConvertScenarioPosition(env_, pos);

    ASSERT_TRUE(pose.has_value());
    EXPECT_EQ(expected_position_, pose.value().position);
    EXPECT_EQ(mantle_api::Orientation3<units::angle::radian_t>(0_rad, 0_rad, 0_rad), pose.value().orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGetLanePositionWithNoOrientation_WhenConverting_ThenPositionConvertedAndOrientationZero)
{
    auto pos = GetLanePosition();
    const auto pose = ConvertScenarioPosition(env_, pos);

    ASSERT_TRUE(pose.has_value());
    EXPECT_EQ(expected_position_, pose.value().position);
    EXPECT_EQ(mantle_api::Orientation3<units::angle::radian_t>(0_rad, 0_rad, 0_rad), pose.value().orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGeoPositionWithAbsoluteOrientation_WhenConverting_ThenOrientationIsReturnedUnchanged)
{
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};

    auto pos = GetGeoPosition(expected_orientation, NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::ABSOLUTE);
    const auto pose = ConvertScenarioPosition(env_, pos);

    ASSERT_TRUE(pose.has_value());
    EXPECT_EQ(expected_orientation, pose.value().orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenRelativeLanePositionWithNoOrientation_WhenConverting_ThenPositionConvertedAndOrientationZero)
{
    auto pos = GetRelativeLanePosition();
    const auto pose = ConvertScenarioPosition(env_, pos);

    ASSERT_TRUE(pose.has_value());
    EXPECT_EQ(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), pose.value().position);
    EXPECT_EQ(mantle_api::Orientation3<units::angle::radian_t>(0_rad, 0_rad, 0_rad), pose.value().orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenRelativeLanePositionWithAbsoluteOrientation_WhenConverting_ThenOrientationIsReturnedUnchanged)
{
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};
    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                GetLaneOrientation(testing::_))
        .Times(0);

    auto pos =
        GetRelativeLanePosition(0, expected_orientation, NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::ABSOLUTE);
    const auto pose = ConvertScenarioPosition(env_, pos);

    ASSERT_TRUE(pose.has_value());
    EXPECT_EQ(expected_orientation, pose.value().orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenRelativeLanePositionWithDSValueAndAbsoluteOrientation_WhenConverting_ThenReturnEmptyPose)
{
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0_rad, 0_rad, 0_rad};
    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                GetLaneOrientation(testing::_))
        .Times(0);

    auto pos =
        GetRelativeLanePosition(10, expected_orientation, NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::ABSOLUTE);
    const auto pose = ConvertScenarioPosition(env_, pos);

    ASSERT_TRUE(pose.has_value());
    EXPECT_EQ(expected_orientation, pose.value().orientation);
    EXPECT_EQ(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), pose.value().position);
}

TEST_F(ConvertScenarioPositionTest,
       GivenRelativeLanePositionWithRelativeOrientation_WhenConverting_ThenOrientationIsIsRelativeToLaneOrientation)
{
    mantle_api::Orientation3<units::angle::radian_t> lane_orientation{10_rad, 11_rad, 12_rad};
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};
    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                GetLaneOrientation(testing::_))
        .Times(1)
        .WillRepeatedly(testing::Return(lane_orientation));

    auto pos =
        GetRelativeLanePosition(0, expected_orientation, NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::RELATIVE);
    const auto pose = ConvertScenarioPosition(env_, pos);

    ASSERT_TRUE(pose.has_value());
    EXPECT_EQ(lane_orientation + expected_orientation, pose.value().orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenRelativeLanePositionWithDSValueAndRelativeOrientation_WhenConverting_ThenReturnEmptyPose)
{
    mantle_api::Orientation3<units::angle::radian_t> lane_orientation{10_rad, 11_rad, 12_rad};
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0_rad, 0_rad, 0_rad};
    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                GetLaneOrientation(testing::_))
        .Times(0);

    auto pos =
        GetRelativeLanePosition(10, expected_orientation, NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::RELATIVE);
    const auto pose = ConvertScenarioPosition(env_, pos);

    ASSERT_TRUE(pose.has_value());
    EXPECT_EQ(expected_orientation, pose.value().orientation);
    EXPECT_EQ(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), pose.value().position);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGetLanePositionWithAbsoluteOrientation_WhenConverting_ThenOrientationIsReturnedUnchanged)
{
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};

    auto pos = GetLanePosition(expected_orientation, NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::ABSOLUTE);
    const auto pose = ConvertScenarioPosition(env_, pos);

    ASSERT_TRUE(pose.has_value());
    EXPECT_EQ(expected_orientation, pose.value().orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGeoPositionWithRelativeOrientation_WhenConverting_ThenOrientationIsRelativeToLaneOrientation)
{
    mantle_api::Orientation3<units::angle::radian_t> lane_orientation{10_rad, 11_rad, 12_rad};
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};
    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                GetLaneOrientation(expected_position_))
        .Times(1)
        .WillRepeatedly(testing::Return(lane_orientation));

    auto pos = GetGeoPosition(expected_orientation, NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::RELATIVE);
    const auto pose = ConvertScenarioPosition(env_, pos);

    ASSERT_TRUE(pose.has_value());
    EXPECT_EQ(expected_orientation + lane_orientation, pose.value().orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGetLanePositionWithRelativeOrientation_WhenConverting_ThenOrientationIsRelativeToLaneOrientation)
{
    mantle_api::Orientation3<units::angle::radian_t> lane_orientation{10_rad, 11_rad, 12_rad};
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};
    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                GetLaneOrientation(expected_position_))
        .Times(1)
        .WillRepeatedly(testing::Return(lane_orientation));

    auto pos = GetLanePosition(expected_orientation, NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::RELATIVE);
    const auto pose = ConvertScenarioPosition(env_, pos);

    ASSERT_TRUE(pose.has_value());
    EXPECT_EQ(expected_orientation + lane_orientation, pose.value().orientation);
}

}  // namespace OPENSCENARIO
