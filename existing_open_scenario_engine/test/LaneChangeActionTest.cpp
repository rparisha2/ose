/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Actions/LaneChangeAction.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"

#include <gtest/gtest.h>

namespace OPENSCENARIO {
using namespace units::literals;

class LaneChangeActionTestFixture : public OpenScenarioEngineLibraryTestBase {
protected:
    void SetUp() override { OpenScenarioEngineLibraryTestBase::SetUp(); }
};

TEST_F(LaneChangeActionTestFixture, GivenLaneChangeActionWithoutDynamics_WhenStart_ExceptionRaised)
{
    using namespace OPENSCENARIO::TESTING;

    auto lane_change_action_data =
            FakeLaneChangeActionBuilder{}
                    .WithLaneChangeTarget(
                            FakeLaneChangeTargetBuilder{}.WithAbsoluteTargetLane(
                                            FakeAbsoluteTargetLaneBuilder{}.WithValue("1").Build())
                                    .Build())
                    .Build();
    OPENSCENARIO::LaneChangeAction lane_change_action(lane_change_action_data, env_, std::vector<std::string>{"Ego"});

    EXPECT_ANY_THROW(lane_change_action.Start());
}

TEST_F(LaneChangeActionTestFixture, GivenLaneChangeActionWithoutLaneChangeTarget_WhenStart_ExceptionRaised)
{
    using namespace OPENSCENARIO::TESTING;

    auto lane_change_action_data =
            FakeLaneChangeActionBuilder{}
                    .WithDynamics(FakeTransitionDynamicsBuilder{}.Build())
                    .WithLaneChangeTarget(FakeLaneChangeTargetBuilder{}.Build())
                    .Build();
    OPENSCENARIO::LaneChangeAction lane_change_action(lane_change_action_data, env_, std::vector<std::string>{"Ego"});

    EXPECT_ANY_THROW(lane_change_action.Start());
}

TEST_F(LaneChangeActionTestFixture,
       GivenLaneChangeActionWithAbsoluteTargetLane_WhenControlStrategyGoalReached_ThenExpectCorrectStrategy)
{
    using namespace OPENSCENARIO::TESTING;
    std::string shape_name("step");
    NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape dynamics_shape(shape_name);
    auto lane_change_action_data =
            FakeLaneChangeActionBuilder{}
            .WithDynamics(FakeTransitionDynamicsBuilder{}.Build())
            .WithLaneChangeTarget(
                    FakeLaneChangeTargetBuilder{}.WithAbsoluteTargetLane(
                            FakeAbsoluteTargetLaneBuilder{}.WithValue("1").Build())
                                .Build())
            .Build();
    OPENSCENARIO::LaneChangeAction lane_change_action(lane_change_action_data, env_, std::vector<std::string>{"Ego"});

    auto expected_control_strategy = mantle_api::PerformLaneChangeControlStrategy();

    EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_))
            .Times(1)
            .WillRepeatedly([expected_control_strategy](
                    std::uint64_t controller_id,
                    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) {
                std::ignore = controller_id;
                EXPECT_EQ(1, control_strategies.size());
                auto control_strategy = dynamic_cast<mantle_api::PerformLaneChangeControlStrategy*>(control_strategies[0].get());
                EXPECT_NE(nullptr, control_strategy);
                EXPECT_EQ(*control_strategy, expected_control_strategy);
            });

    lane_change_action.Start();
    lane_change_action.Step();
}

TEST_F(LaneChangeActionTestFixture,
       GivenLaneChangeActionWithRelativeLaneTarget_WhenControlStrategyGoalReached_ThenExpectCorrectStrategy)
{
    using namespace OPENSCENARIO::TESTING;
    std::string shape_name("step");
    NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape dynamics_shape(shape_name);
    auto lane_change_action_data =
            FakeLaneChangeActionBuilder{}
            .WithDynamics(FakeTransitionDynamicsBuilder{}.Build())
            .WithLaneChangeTarget(
                    FakeLaneChangeTargetBuilder{}.WithRelativeTargetLane(
                            FakeRelativeTargetLaneBuilder{}.WithValue(1).WithEntityRef("Ego").Build())
                                .Build())
            .Build();
    OPENSCENARIO::LaneChangeAction lane_change_action(lane_change_action_data, env_, std::vector<std::string>{"Ego"});

    auto expected_control_strategy = mantle_api::PerformLaneChangeControlStrategy();

    EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_))
            .Times(1)
            .WillRepeatedly([expected_control_strategy](
                    std::uint64_t controller_id,
                    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) {
                std::ignore = controller_id;
                EXPECT_EQ(1, control_strategies.size());
                auto control_strategy = dynamic_cast<mantle_api::PerformLaneChangeControlStrategy*>(control_strategies[0].get());
                EXPECT_NE(nullptr, control_strategy);
                EXPECT_EQ(*control_strategy, expected_control_strategy);
            });

    lane_change_action.Start();
    lane_change_action.Step();
}

}
