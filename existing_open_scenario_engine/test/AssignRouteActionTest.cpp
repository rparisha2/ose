/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Actions/AssignRouteAction.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"
#include "builders/PositionBuilder.h"

#include <MantleAPI/Common/vector.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_1/impl/OpenScenarioWriterFactoryImplV1_1.h>

namespace OPENSCENARIO
{
using namespace units::literals;

class AssignRouteActionTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override { OpenScenarioEngineLibraryTestBase::SetUp(); }
};

MATCHER_P(IsRouteDefinitionSame, route_definition, "Waypoints or route strategy do not match")
{
    bool are_waypoints_equal = false;
    bool are_route_strategies_equal = false;

    for (size_t j = 0; j < route_definition.waypoints.

                           size();

         ++j)
    {
        are_waypoints_equal = (arg.waypoints.at(j).waypoint.x == route_definition.waypoints.at(j).waypoint.x &&
                               arg.waypoints.at(j).waypoint.y == route_definition.waypoints.at(j).waypoint.y &&
                               arg.waypoints.at(j).waypoint.z == route_definition.waypoints.at(j).waypoint.z);
        are_route_strategies_equal =
            (arg.waypoints.at(j).route_strategy == route_definition.waypoints.at(j).route_strategy);
        if (are_waypoints_equal == true && are_route_strategies_equal == true)
        {
            continue;
        }
        else
        {
            return false;
        }
    }
    return true;
}

TEST_F(AssignRouteActionTestFixture, GivenAssignRouteAction_WhenStartAction_ThenRouteAssignedAndNoExceptionInStep)
{
    using namespace OPENSCENARIO::TESTING;

    auto fake_assign_route_action = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::AssignRouteActionImpl>();

    auto route = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::RouteImpl>();

    auto way_point_one = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::WaypointImpl>();
    auto way_point_two = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::WaypointImpl>();

    auto position_one = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::PositionImpl>();
    auto position_two = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::PositionImpl>();

    auto world_position_one = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::WorldPositionImpl>();
    auto world_position_two = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::WorldPositionImpl>();

    world_position_one->SetX(1.0);
    world_position_one->SetY(0.0);
    world_position_one->SetZ(0.0);

    world_position_two->SetX(42.0);
    world_position_two->SetY(0.0);
    world_position_two->SetZ(10.0);

    position_one->SetWorldPosition(world_position_one);
    position_two->SetWorldPosition(world_position_two);

    way_point_one->SetPosition(position_one);
    way_point_one->SetRouteStrategy(NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum::FASTEST);
    way_point_two->SetPosition(position_two);
    way_point_two->SetRouteStrategy(NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum::SHORTEST);

    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IWaypointWriter>> route_way_points = {way_point_one,
                                                                                                   way_point_two};
    route->SetWaypoints(route_way_points);
    fake_assign_route_action->SetRoute(route);

    OPENSCENARIO::AssignRouteAction assign_route_action{
        fake_assign_route_action, env_, std::vector<std::string>{"Ego"}};

    mantle_api::RouteDefinition expected_route_definition{};
    expected_route_definition.waypoints.push_back(mantle_api::RouteWaypoint{
        mantle_api::Vec3<units::length::meter_t>{1_m, 0_m, 0_m}, mantle_api::RouteStrategy::kFastest});
    expected_route_definition.waypoints.push_back(mantle_api::RouteWaypoint{
        mantle_api::Vec3<units::length::meter_t>{42_m, 0_m, 10_m}, mantle_api::RouteStrategy::kShortest});

    EXPECT_CALL(*env_, AssignRoute(testing::_, IsRouteDefinitionSame(expected_route_definition)))
        .WillOnce(testing::SaveArg<1>(&expected_route_definition));

    EXPECT_NO_THROW(assign_route_action.Start());
    EXPECT_NO_THROW(assign_route_action.Step());
}

}  // namespace OPENSCENARIO
