/*******************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once
#include "builders/condition_builder.h"

#include <openScenarioLib/generated/v1_1/impl/ApiClassImplV1_1.h>
#include <openScenarioLib/src/impl/NamedReferenceProxy.h>

#include <memory>

namespace OPENSCENARIO::TESTING
{

class FakeTriggerEntitiesBuilder
{
  public:
    FakeTriggerEntitiesBuilder& WithTriggeringEntitiesRule(
        const NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum rule);

    FakeTriggerEntitiesBuilder& WithEntityRefs(std::vector<std::string> trigger_entities_names);

    NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesImpl Build();

  private:
    NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesImpl trigger_entities_{};
};

class FakeTrigger : public NET_ASAM_OPENSCENARIO::v1_1::TriggerImpl
{
  public:
    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IConditionGroup>> GetConditionGroups() const override;
    void AddConditionGroup(FakeConditionGroup condition_group);

  private:
    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IConditionGroup>> condition_groups_{};
};

class FakeTriggerBuilder
{
  public:
    FakeTriggerBuilder& WithConditionGroup(FakeConditionGroup condition_group);
    std::shared_ptr<FakeTrigger> Build();

  private:
    FakeTrigger trigger_{};
};

class FakeEvent : public NET_ASAM_OPENSCENARIO::v1_1::EventImpl
{
  public:
    void AddAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IActionWriter> action);

  private:
    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IActionWriter>> fake_actions;
};

class FakeEventBuilder
{
  public:
    FakeEventBuilder();
    FakeEventBuilder& WithAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IActionWriter> action);
    FakeEventBuilder& WithStartTrigger(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggerWriter> trigger);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEventWriter> Build();

  private:
    std::shared_ptr<FakeEvent> event_{};
};

class FakeStoryboard : public NET_ASAM_OPENSCENARIO::v1_1::IStoryboard
{
  public:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IInit> GetInit() const override;
    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IStory>> GetStories() const override;
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrigger> GetStopTrigger() const override;
    void SetStopTrigger(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggerWriter> stop_trigger);
    bool IsSetInit() const override { return true; }
    bool IsSetStories() const override { return true; }
    bool IsSetStopTrigger() const override { return true; }

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrigger> stop_trigger_{};
};

class FakeStoryboardBuilder
{
  public:
    FakeStoryboardBuilder& WithStopTrigger(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggerWriter> stop_trigger);
    FakeStoryboard Build();

  private:
    FakeStoryboard storyboard_;
};
}  // namespace OPENSCENARIO::TESTING
