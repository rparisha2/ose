/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Constants.h"

#include <gtest/gtest.h>

namespace OPENSCENARIO
{

TEST(ConstantsTest, GivenInvalidName_WhenIsDrivingFunctionControlledEntityCalled_ThenReturnFalse)
{
    const std::string invalid_name = "arbitrary_invalid_name";
    EXPECT_FALSE(IsDrivingFunctionControlledEntity(invalid_name));
}

class ConstantsValidNamesTestFixture : public ::testing::TestWithParam<std::string>
{
};

INSTANTIATE_TEST_SUITE_P(ValidNames,
                         ConstantsValidNamesTestFixture,
                         ::testing::ValuesIn(std::vector<std::string>{"Ego", "Host", "M1: something"}));

TEST_P(ConstantsValidNamesTestFixture, GivenValidName_WhenIsDrivingFunctionControlledEntityCalled_ThenReturnTrue)
{
    std::string valid_name = GetParam();

    EXPECT_TRUE(IsDrivingFunctionControlledEntity(valid_name));
}

}  // namespace OPENSCENARIO
