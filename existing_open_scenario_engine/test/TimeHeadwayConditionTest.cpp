/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Constants.h"
#include "Storyboard/Conditions/ByEntityConditions/TimeHeadwayCondition.h"
#include "TestUtils.h"
#include "builders/condition_builder.h"
#include "builders/storyboard_builder.h"

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

namespace OPENSCENARIO
{
using namespace NET_ASAM_OPENSCENARIO;
using namespace NET_ASAM_OPENSCENARIO::v1_1;

using RelativeDistance = units::length::meter_t;
using TriggeringEntityVelocity = mantle_api::Vec3<units::velocity::meters_per_second_t>;
using Freespace = bool;
using ConditionValue = double;
using ExpectedSatisfiedStatus = bool;
using TimeHeadwayConditionCoordinateSystemTestInputType =
    std::tuple<RelativeDistance, TriggeringEntityVelocity, ConditionValue, ExpectedSatisfiedStatus>;

class TimeHeadwayConditionTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override
    {
        OpenScenarioEngineLibraryTestBase::SetUp();

        condition_impl_ = std::make_shared<TimeHeadwayConditionImpl>();
        condition_impl_->SetCoordinateSystem({CoordinateSystem::ENTITY});
        condition_impl_->SetFreespace(false);
        condition_impl_->SetEntityRef(nullptr);
        condition_impl_->SetRelativeDistanceType({RelativeDistanceType::LONGITUDINAL});
        // Note: Only consider rule = EQUAL_TO, the rest rules are tested in the OPENSCENARIO::Rule unit test
        condition_impl_->SetRule({NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO});
    }

    auto GenerateFakeTrigger()
    {
        auto fake_trigger_entities =
            OPENSCENARIO::TESTING::FakeTriggerEntitiesBuilder()
                .WithTriggeringEntitiesRule(TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ALL)
                .WithEntityRefs({"entity_ref"})
                .Build();
        return std::make_shared<TriggeringEntitiesImpl>(fake_trigger_entities);
    }

    std::shared_ptr<TimeHeadwayConditionImpl> condition_impl_;
};

TEST_F(TimeHeadwayConditionTestFixture,
       GivenConditionWithUnsupportedRelativeDistanceTypeLateral_WhenEvaluate_ThenAutoSatisfied)
{
    condition_impl_->SetRelativeDistanceType({RelativeDistanceType::LATERAL});
    auto time_headway_condition = TimeHeadwayCondition(condition_impl_, GenerateFakeTrigger(), env_);

    EXPECT_TRUE(time_headway_condition.IsSatisfied());
}

TEST_F(TimeHeadwayConditionTestFixture,
       GivenConditionWithUnsupportedRelativeDistanceTypeEuclidianDistance_WhenEvaluate_ThenAutoSatisfied)
{
    condition_impl_->SetRelativeDistanceType({RelativeDistanceType::EUCLIDIAN_DISTANCE});
    auto time_headway_condition = TimeHeadwayCondition(condition_impl_, GenerateFakeTrigger(), env_);

    EXPECT_TRUE(time_headway_condition.IsSatisfied());
}

TEST_F(TimeHeadwayConditionTestFixture,
       GivenConditionWithUnsupportedCoordinateSystemTrajectory_WhenEvaluate_ThenAutoSatisfied)
{
    condition_impl_->SetCoordinateSystem({CoordinateSystem::TRAJECTORY});
    auto time_headway_condition = TimeHeadwayCondition(condition_impl_, GenerateFakeTrigger(), env_);

    EXPECT_TRUE(time_headway_condition.IsSatisfied());
}

TEST_F(TimeHeadwayConditionTestFixture,
       GivenConditionWithUnsupportedCoordinateSystemRoad_WhenEvaluate_ThenAutoSatisfied)
{
    condition_impl_->SetCoordinateSystem({CoordinateSystem::ROAD});
    auto time_headway_condition = TimeHeadwayCondition(condition_impl_, GenerateFakeTrigger(), env_);

    EXPECT_TRUE(time_headway_condition.IsSatisfied());
}

TEST_F(TimeHeadwayConditionTestFixture,
       GivenConditionWithCoordinateSystemLaneButCanNotCalculateDistance_WhenEvaluate_ThenThrowRuntimeError)
{
    condition_impl_->SetCoordinateSystem({CoordinateSystem::LANE});
    condition_impl_->SetEntityRef(std::make_shared<NamedReferenceProxy<IEntity>>("entity_ref"));

    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                GetLongitudinalLaneDistanceBetweenPositions(testing::_, testing::_))
        .Times(1)
        .WillRepeatedly(testing::Return(std::nullopt));

    auto time_headway_condition = TimeHeadwayCondition(condition_impl_, GenerateFakeTrigger(), env_);

    EXPECT_THROW(time_headway_condition.IsSatisfied(), std::runtime_error);
}

TEST_F(
    TimeHeadwayConditionTestFixture,
    GivenConditionWithTriggeringEntityVelocityZero_WhenEvaluate_ThenNoThrowRuntimeSatisfiedWithDefaultTimeHeadwayValue)
{
    condition_impl_->SetCoordinateSystem({CoordinateSystem::ENTITY});
    condition_impl_->SetEntityRef(std::make_shared<NamedReferenceProxy<IEntity>>("entity_ref"));
    condition_impl_->SetValue(limits::kTimeHeadwayMax.value());

    auto& mocked_geo_helper = dynamic_cast<const mantle_api::MockGeometryHelper&>(*(env_->GetGeometryHelper()));
    auto& mocked_entity = dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get());
    EXPECT_CALL(mocked_geo_helper, TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
        .WillRepeatedly(testing::Return(mantle_api::Vec3<units::length::meter_t>{10.0_m, 0_m, 0_m}));
    EXPECT_CALL(mocked_entity, GetVelocity())
        .WillOnce(::testing::Return(TriggeringEntityVelocity{0.0_mps, 0.0_mps, 0.0_mps}));

    auto time_headway_condition = TimeHeadwayCondition(condition_impl_, GenerateFakeTrigger(), env_);

    EXPECT_EQ(time_headway_condition.IsSatisfied(), true);
}

// clang-format off
const auto kTimeHeadwayConditionCoordinateSystemTestInputs =
    testing::Values(
      std::make_tuple(RelativeDistance{50_m}, TriggeringEntityVelocity{5.0_mps, 0.0_mps, 0.0_mps}, ConditionValue{3.0},  ExpectedSatisfiedStatus(false)),
      std::make_tuple(RelativeDistance{50_m}, TriggeringEntityVelocity{5.0_mps, 0.0_mps, 0.0_mps}, ConditionValue{10.0}, ExpectedSatisfiedStatus(true)),
      std::make_tuple(RelativeDistance{50_m}, TriggeringEntityVelocity{3.0_mps, 4.0_mps, 0.0_mps}, ConditionValue{10.0}, ExpectedSatisfiedStatus(true))
    );
const auto kFreespaceInputs = testing::Values(true, false);
// clang-format on

class TimeHeadwayConditionTestForCoordinateSystem
    : public TimeHeadwayConditionTestFixture,
      public testing::WithParamInterface<std::tuple<TimeHeadwayConditionCoordinateSystemTestInputType, Freespace>>
{
  protected:
    void SetUp() override
    {
        TimeHeadwayConditionTestFixture::SetUp();

        relative_distance_ = std::get<0>(std::get<0>(GetParam()));
        triggering_entity_velocity_ = std::get<1>(std::get<0>(GetParam()));
        condition_value_ = std::get<2>(std::get<0>(GetParam()));
        expected_satisfied_status_ = std::get<3>(std::get<0>(GetParam()));
        freespace_ = std::get<1>(GetParam());

        condition_impl_->SetFreespace(freespace_);
        condition_impl_->SetEntityRef(std::make_shared<NamedReferenceProxy<IEntity>>("entity_ref"));
        condition_impl_->SetValue(condition_value_);
    }
    RelativeDistance relative_distance_{};
    TriggeringEntityVelocity triggering_entity_velocity_{};
    Freespace freespace_{false};
    ConditionValue condition_value_{};
    ExpectedSatisfiedStatus expected_satisfied_status_{false};
};

INSTANTIATE_TEST_CASE_P(TimeHeadwayConditionTestForCoordinateSystemInputs,
                        TimeHeadwayConditionTestForCoordinateSystem,
                        testing::Combine(kTimeHeadwayConditionCoordinateSystemTestInputs, kFreespaceInputs));

TEST_P(TimeHeadwayConditionTestForCoordinateSystem,
       GivenCoordinateSystemIsEntityWithTestInputs_WhenEvaluate_ThenExpectCorrectSatisfiedStatus)
{

    condition_impl_->SetCoordinateSystem({CoordinateSystem::ENTITY});

    auto& mocked_geo_helper = dynamic_cast<const mantle_api::MockGeometryHelper&>(*(env_->GetGeometryHelper()));
    auto& mocked_entity = dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get());

    // Test though stubed function TranslateGlobalPositionLocally that is used by both
    // CalculateLongitudinalFreeSpaceDistance and CalculateRelativeLongitudinalDistance
    EXPECT_CALL(mocked_geo_helper, TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
        .WillRepeatedly(testing::Return(mantle_api::Vec3<units::length::meter_t>{relative_distance_, 0_m, 0_m}));
    EXPECT_CALL(mocked_entity, GetVelocity()).WillOnce(::testing::Return(triggering_entity_velocity_));

    auto time_headway_condition = TimeHeadwayCondition(condition_impl_, GenerateFakeTrigger(), env_);

    EXPECT_EQ(time_headway_condition.IsSatisfied(), expected_satisfied_status_);
}

TEST_P(TimeHeadwayConditionTestForCoordinateSystem,
       GivenCoordinateSystemIsLaneWithTestInputs_WhenEvaluate_ThenExpectCorrectSatisfiedStatus)
{

    condition_impl_->SetCoordinateSystem({CoordinateSystem::LANE});

    auto& mocked_query_service = dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService());
    auto& mocked_entity = dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get());
    auto& mocked_geo_helper = dynamic_cast<const mantle_api::MockGeometryHelper&>(*(env_->GetGeometryHelper()));

    EXPECT_CALL(mocked_entity, GetVelocity()).WillOnce(::testing::Return(triggering_entity_velocity_));

    if (freespace_)
    {
        const auto headmost_corner = mantle_api::Vec3<units::length::meter_t>{3.0_m, 0_m, 0_m};
        const auto rearmost_corner = mantle_api::Vec3<units::length::meter_t>{-2.0_m, 0_m, 0_m};
        const auto other_corner = mantle_api::Vec3<units::length::meter_t>{0.0_m, 0_m, 0_m};
        const auto entity_length = units::math::abs(headmost_corner.x) + units::math::abs(rearmost_corner.x);

        EXPECT_CALL(mocked_query_service, GetLongitudinalLaneDistanceBetweenPositions(testing::_, testing::_))
            .WillOnce(testing::Return(relative_distance_ + entity_length));

        EXPECT_CALL(mocked_query_service, FindLanePoseAtDistanceFrom(testing::_, testing::_, testing::_))
            .WillOnce(testing::Return(mantle_api::Pose{}));

        testing::InSequence s;
        // called two iterations for the trigger entity and reference entity respectively
        EXPECT_CALL(mocked_geo_helper, TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
            .Times(8)
            .WillOnce(::testing::Return(headmost_corner))
            .WillOnce(::testing::Return(rearmost_corner))
            .WillRepeatedly(::testing::Return(other_corner));
        EXPECT_CALL(mocked_geo_helper, TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
            .Times(8)
            .WillOnce(::testing::Return(headmost_corner))
            .WillOnce(::testing::Return(rearmost_corner))
            .WillRepeatedly(::testing::Return(other_corner));
    }
    else
    {
        EXPECT_CALL(mocked_query_service, GetLongitudinalLaneDistanceBetweenPositions(testing::_, testing::_))
            .WillOnce(testing::Return(relative_distance_));
    }

    auto time_headway_condition = TimeHeadwayCondition(condition_impl_, GenerateFakeTrigger(), env_);

    EXPECT_EQ(time_headway_condition.IsSatisfied(), expected_satisfied_status_);
}

}  // namespace OPENSCENARIO
