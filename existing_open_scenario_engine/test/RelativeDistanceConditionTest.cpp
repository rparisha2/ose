/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Conditions/ByEntityConditions/RelativeDistanceCondition.h"
#include "TestUtils.h"
#include "builders/condition_builder.h"
#include "builders/storyboard_builder.h"

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

namespace OPENSCENARIO
{

class RelativeDistanceConditionTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceConditionImpl relative_distance_condition_impl_;
};

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionLaneWithLongitudinalTypeWithEntityCoordinateSystemWithTriggerRuleAllWithFreeSpaceFalse_WhenRelativeDistanceConditionIsCalled_ThenMockVehicleIsCalled)
{

    const auto coordinate_system = NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::ENTITY;
    const auto freespace = false;
    const auto ego_name{"Ego"};
    const auto relative_distance_type = NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::LONGITUDINAL;
    const auto rule = NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO;
    double value = 10;

 
    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get(ego_name).value().get()),
                GetPosition())
      .WillOnce(::testing::Return(mantle_api::Vec3<units::length::meter_t>{5_m, 0_m, 0_m}))
      .WillOnce(::testing::Return(mantle_api::Vec3<units::length::meter_t>{15_m, 0_m, 0_m}));
  
    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get(ego_name).value().get()),
                GetOrientation())
      .WillRepeatedly(::testing::Return(mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad}));
   
    relative_distance_condition_impl_.SetCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem(coordinate_system));
    relative_distance_condition_impl_.SetEntityRef(
        std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_1::IEntity>>(
        ego_name));
    relative_distance_condition_impl_.SetFreespace(freespace);
    relative_distance_condition_impl_.SetRelativeDistanceType(
        NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType(relative_distance_type));
    relative_distance_condition_impl_.SetRule(NET_ASAM_OPENSCENARIO::v1_1::Rule(rule));
    relative_distance_condition_impl_.SetValue(value);

    auto fake_condition =
        std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceConditionImpl>(relative_distance_condition_impl_);

    auto fake_trigger_entities =
        OPENSCENARIO::TESTING::FakeTriggerEntitiesBuilder()
            .WithTriggeringEntitiesRule(
                NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ALL)
            .WithEntityRefs({"traffic_vehicle_1"})
            .Build();

    auto fake_trigger = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesImpl>(fake_trigger_entities);

    auto relative_distance_condition = OPENSCENARIO::RelativeDistanceCondition(fake_condition, fake_trigger, env_);
    relative_distance_condition.IsSatisfied();
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionLaneWithLongitudinalTypeWithEntityCoordinateSystemWithTriggerRuleAllWithFreeSpaceTrue_WhenRelativeDistanceConditionIsCalled_ThenMockVehicleIsCalled)
{

    const auto coordinate_system = NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::ENTITY;
    const auto freespace = true;
    const auto ego_name{"Ego"};
    const auto relative_distance_type = NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::LONGITUDINAL;
    const auto rule = NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO;
    double value = 10;

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get(ego_name).value().get()),
                GetPosition())
      .WillOnce(::testing::Return(mantle_api::Vec3<units::length::meter_t>{5_m, 0_m, 0_m}))
      .WillOnce(::testing::Return(mantle_api::Vec3<units::length::meter_t>{15_m, 0_m, 0_m}));
  
    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get(ego_name).value().get()),
                GetOrientation())
      .WillRepeatedly(::testing::Return(mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad}));

    relative_distance_condition_impl_.SetCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem(coordinate_system));
    relative_distance_condition_impl_.SetEntityRef(
        std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_1::IEntity>>(
        ego_name));
    relative_distance_condition_impl_.SetFreespace(freespace);
    relative_distance_condition_impl_.SetRelativeDistanceType(
        NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType(relative_distance_type));
    relative_distance_condition_impl_.SetRule(NET_ASAM_OPENSCENARIO::v1_1::Rule(rule));
    relative_distance_condition_impl_.SetValue(value);
    auto fake_condition =
        std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceConditionImpl>(relative_distance_condition_impl_);

    auto fake_trigger_entities =
        OPENSCENARIO::TESTING::FakeTriggerEntitiesBuilder()
            .WithTriggeringEntitiesRule(
                NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ALL)
            .WithEntityRefs({"traffic_vehicle_1"})
            .Build();

    auto fake_trigger = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesImpl>(fake_trigger_entities);

    auto relative_distance_condition = OPENSCENARIO::RelativeDistanceCondition(fake_condition, fake_trigger, env_);

    relative_distance_condition.IsSatisfied();
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionLaneWithLongitudinalTypeWithLaneCoordinateSystemWithTriggerRuleAllWithFreeSpaceFalse_WhenRelativeDistanceConditionIsCalled_ThenMockVehicleIsNotCalled)
{

    const auto coordinate_system = NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE;
    const auto freespace = false;
    const auto ego_name{"Ego"};
    const auto relative_distance_type = NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::LONGITUDINAL;
    const auto rule = NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO;
    double value = 4;

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get(ego_name).value().get()),
                GetPosition())
        .Times(0);
    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get(ego_name).value().get()),
                GetOrientation())
        .Times(0);

    relative_distance_condition_impl_.SetCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem(coordinate_system));
    relative_distance_condition_impl_.SetEntityRef(
        std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_1::IEntity>>(
        ego_name));
    relative_distance_condition_impl_.SetFreespace(freespace);
    relative_distance_condition_impl_.SetRelativeDistanceType(
        NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType(relative_distance_type));
    relative_distance_condition_impl_.SetRule(NET_ASAM_OPENSCENARIO::v1_1::Rule(rule));
    relative_distance_condition_impl_.SetValue(value);

    auto fake_condition =
        std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceConditionImpl>(relative_distance_condition_impl_);

    auto fake_trigger_entities =
        OPENSCENARIO::TESTING::FakeTriggerEntitiesBuilder()
            .WithTriggeringEntitiesRule(
                NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ALL)
            .WithEntityRefs({"traffic_vehicle_1"})
            .Build();

    auto fake_trigger = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesImpl>(fake_trigger_entities);

    auto relative_distance_condition = OPENSCENARIO::RelativeDistanceCondition(fake_condition, fake_trigger, env_);

    EXPECT_TRUE(relative_distance_condition.IsSatisfied());
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionLaneWithLateralTypeWithEntityCoordinateSystemWithTriggerRuleAllWithFreeSpaceFalse_WhenRelativeDistanceConditionIsCalled_ThenMockVehicleIsNotCalled)
{

    const auto coordinate_system = NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::ENTITY;
    const auto freespace = false;
    const auto ego_name{"Ego"};
    const auto relative_distance_type = NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::LATERAL;
    const auto rule = NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO;
    double value = 4;

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get(ego_name).value().get()),
                GetPosition())
        .Times(0);
    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get(ego_name).value().get()),
                GetOrientation())
        .Times(0);

    relative_distance_condition_impl_.SetCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem(coordinate_system));
    relative_distance_condition_impl_.SetEntityRef(
        std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_1::IEntity>>(
        ego_name));
    relative_distance_condition_impl_.SetFreespace(freespace);
    relative_distance_condition_impl_.SetRelativeDistanceType(
        NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType(relative_distance_type));
    relative_distance_condition_impl_.SetRule(NET_ASAM_OPENSCENARIO::v1_1::Rule(rule));
    relative_distance_condition_impl_.SetValue(value);

    auto fake_condition =
        std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceConditionImpl>(relative_distance_condition_impl_);

    auto fake_trigger_entities =
        OPENSCENARIO::TESTING::FakeTriggerEntitiesBuilder()
            .WithTriggeringEntitiesRule(
                NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ALL)
            .WithEntityRefs({"traffic_vehicle_1"})
            .Build();

    auto fake_trigger = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesImpl>(fake_trigger_entities);

    auto relative_distance_condition = OPENSCENARIO::RelativeDistanceCondition(fake_condition, fake_trigger, env_);

    EXPECT_TRUE(relative_distance_condition.IsSatisfied());
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionWithTrajectoryWithTrajectoryCoordinateSystemWithTriggerRuleAll_WhenRelativeDistanceConditionIsCalled_ThenMockVehicleIsNotCalled)
{
    const auto coordinate_system = NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::TRAJECTORY;
    const auto ego_name{"Ego"};
    const auto freespace = false;
    const auto relative_distance_type = NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::CARTESIAN_DISTANCE;
    const auto rule = NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO;
    double value = 5;

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get(ego_name).value().get()),
                GetPosition())
        .Times(0);
    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get(ego_name).value().get()),
                GetOrientation())
        .Times(0);

    relative_distance_condition_impl_.SetCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem(coordinate_system));
    relative_distance_condition_impl_.SetEntityRef(
        std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_1::IEntity>>(
        ego_name));
    relative_distance_condition_impl_.SetFreespace(freespace);
    relative_distance_condition_impl_.SetRelativeDistanceType(
        NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType(relative_distance_type));
    relative_distance_condition_impl_.SetRule(NET_ASAM_OPENSCENARIO::v1_1::Rule(rule));
    relative_distance_condition_impl_.SetValue(value);

    auto fake_condition =
        std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceConditionImpl>(relative_distance_condition_impl_);

    auto fake_trigger_entities =
        OPENSCENARIO::TESTING::FakeTriggerEntitiesBuilder()
            .WithTriggeringEntitiesRule(
                NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ALL)
            .WithEntityRefs({"traffic_vehicle_1"})
            .Build();

    auto fake_trigger = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesImpl>(fake_trigger_entities);

    auto relative_distance_condition = OPENSCENARIO::RelativeDistanceCondition(fake_condition, fake_trigger, env_);

    relative_distance_condition.IsSatisfied();
}

TEST_F(
    RelativeDistanceConditionTestFixture,
    GivenConditionWithLaneWithEuclidianTypeWithLaneCoordinateSystemWithTriggerRuleAll_WhenRelativeDistanceConditionIsCalled_ThenMockVehicleIsNotCalled)
{
    const auto coordinate_system = NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE;
    const auto ego_name{"ego"};
    const auto freespace = false;
    const auto relative_distance_type = NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::EUCLIDIAN_DISTANCE;
    const auto rule = NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO;
    double value = 5;

    relative_distance_condition_impl_.SetCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem(coordinate_system));
    relative_distance_condition_impl_.SetEntityRef(
        std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_1::IEntity>>(
        ego_name));
    relative_distance_condition_impl_.SetFreespace(freespace);
    relative_distance_condition_impl_.SetRelativeDistanceType(
        NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType(relative_distance_type));
    relative_distance_condition_impl_.SetRule(NET_ASAM_OPENSCENARIO::v1_1::Rule(rule));
    relative_distance_condition_impl_.SetValue(value);

    auto fake_condition =
        std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceConditionImpl>(relative_distance_condition_impl_);

    auto fake_trigger_entities =
        OPENSCENARIO::TESTING::FakeTriggerEntitiesBuilder()
            .WithTriggeringEntitiesRule(
                NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ALL)
            .WithEntityRefs({"traffic_vehicle_1", "traffic_vehicle_2"})
            .Build();

    auto fake_trigger = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesImpl>(fake_trigger_entities);

    auto relative_distance_condition = OPENSCENARIO::RelativeDistanceCondition(fake_condition, fake_trigger, env_);

    EXPECT_TRUE(relative_distance_condition.IsSatisfied());
}

}  // namespace OPENSCENARIO
