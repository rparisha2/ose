/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Actions/TeleportAction.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"
#include "builders/PositionBuilder.h"

#include <gtest/gtest.h>

namespace OPENSCENARIO
{
using units::literals::operator""_m;

class TeleportActionTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override { OpenScenarioEngineLibraryTestBase::SetUp(); }
};

TEST_F(TeleportActionTestFixture, GivenTeleportActionWithLanePosition_WhenStepAction_ThenConvertAndCallSetPosition)
{
    mantle_api::OpenDriveLanePosition open_drive_position{"1", 2, 3.0_m, 4.0_m};
    mantle_api::Position expected_position = open_drive_position;

    using namespace OPENSCENARIO::TESTING;
    auto fake_teleport_action =
        FakeTeleportActionBuilder{}
            .WithPosition(FakePositionBuilder{}
                              .WithLanePosition(FakeLanePositionBuilder(open_drive_position.road,
                                                                        std::to_string(open_drive_position.lane),
                                                                        open_drive_position.t_offset(),
                                                                        open_drive_position.s_offset())
                                                    .Build())
                              .Build())
            .Build();

    OPENSCENARIO::TeleportAction teleport_action{fake_teleport_action, env_, std::vector<std::string>{"Ego"}};

    EXPECT_CALL(dynamic_cast<const mantle_api::MockConverter&>(*(env_->GetConverter())), Convert(expected_position))
        .Times(1)
        .WillRepeatedly(testing::Return(mantle_api::Vec3<units::length::meter_t>()));

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("")->get()),
                SetPosition(testing::_))
        .Times(1);

    teleport_action.Start();
    EXPECT_NO_THROW(teleport_action.Step());
}

TEST_F(TeleportActionTestFixture,
       GivenTeleportActionWithLanePositionAndVerticalOffset_WhenStepAction_ThenConvertAndCallSetPosition)
{
    mantle_api::OpenDriveLanePosition open_drive_position{"1", 2, 3.0_m, 4.0_m};
    mantle_api::Position expected_position = open_drive_position;
    mantle_api::MockStaticObject mock_static_object{};

    mantle_api::StaticObjectProperties traffic_sign_properties{};
    traffic_sign_properties.vertical_offset = 1.0_m;
    traffic_sign_properties.type = mantle_api::EntityType::kOther;
    traffic_sign_properties.model = "traffic_sign";
    traffic_sign_properties.bounding_box.dimension.length = 1.0_m;
    traffic_sign_properties.bounding_box.dimension.width = 1.0_m;
    traffic_sign_properties.bounding_box.dimension.height = 1.0_m;

    using namespace OPENSCENARIO::TESTING;
    auto fake_teleport_action =
        FakeTeleportActionBuilder{}
            .WithPosition(FakePositionBuilder{}
                              .WithLanePosition(FakeLanePositionBuilder(open_drive_position.road,
                                                                        std::to_string(open_drive_position.lane),
                                                                        open_drive_position.t_offset(),
                                                                        open_drive_position.s_offset())
                                                    .Build())
                              .Build())
            .Build();

    auto static_object = std::make_unique<mantle_api::MockStaticObject>();
    auto static_object_properties = std::make_unique<mantle_api::StaticObjectProperties>(traffic_sign_properties);

    OPENSCENARIO::TeleportAction teleport_action{fake_teleport_action, env_, std::vector<std::string>{"traffic_sign"}};

    EXPECT_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository()), GetImpl(testing::_))
        .WillRepeatedly(testing::Return(static_object.get()));

    EXPECT_CALL(*static_object, GetPropertiesImpl()).WillRepeatedly(testing::Return(static_object_properties.get()));

    EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(env_->GetQueryService()),
                GetUpwardsShiftedLanePosition(testing::_, traffic_sign_properties.vertical_offset.value(), testing::_))
        .Times(1);

    EXPECT_CALL(*static_object, SetPosition(testing::_)).Times(1);

    EXPECT_CALL(dynamic_cast<const mantle_api::MockConverter&>(*(env_->GetConverter())), Convert(expected_position))
        .Times(1)
        .WillRepeatedly(testing::Return(mantle_api::Vec3<units::length::meter_t>()));

    teleport_action.Start();
    EXPECT_NO_THROW(teleport_action.Step());
}

TEST_F(TeleportActionTestFixture, GivenTeleportActionWithGeoPosition_WhenStepAction_ThenConvertAndCallSetPosition)
{
    mantle_api::LatLonPosition lat_lon_position{42.123_deg, 11.65874_deg};
    mantle_api::Position expected_position = lat_lon_position;

    using namespace OPENSCENARIO::TESTING;
    auto fake_teleport_action =
        FakeTeleportActionBuilder{}
            .WithPosition(
                FakePositionBuilder{}
                    .WithGeoPosition(
                        FakeGeoPositionBuilder(lat_lon_position.latitude(), lat_lon_position.longitude()).Build())
                    .Build())
            .Build();

    OPENSCENARIO::TeleportAction teleport_action{fake_teleport_action, env_, std::vector<std::string>{"Ego"}};

    EXPECT_CALL(dynamic_cast<const mantle_api::MockConverter&>(*(env_->GetConverter())), Convert(expected_position))
        .Times(1)
        .WillRepeatedly(testing::Return(mantle_api::Vec3<units::length::meter_t>()));

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(1);

    teleport_action.Start();
    EXPECT_NO_THROW(teleport_action.Step());
}

TEST_F(TeleportActionTestFixture,
       GivenTeleportActionWithGeoPositionContainingOrientation_WhenStepAction_ThenConvertAndCallSetOrientation)
{
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOrientationWriter> orientation_writer =
        std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::OrientationImpl>();
    orientation_writer->SetH(1.2);
    orientation_writer->SetP(2.3);
    orientation_writer->SetR(5.0);

    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};

    mantle_api::LatLonPosition lat_lon_position{42.123_deg, 11.65874_deg};

    using namespace OPENSCENARIO::TESTING;
    auto fake_teleport_action =
        FakeTeleportActionBuilder{}
            .WithPosition(
                FakePositionBuilder{}
                    .WithGeoPosition(FakeGeoPositionBuilder(lat_lon_position.latitude(), lat_lon_position.longitude())
                                         .WithOrientation(orientation_writer)
                                         .Build())
                    .Build())
            .Build();

    OPENSCENARIO::TeleportAction teleport_action{fake_teleport_action, env_, std::vector<std::string>{"Ego"}};

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetOrientation(expected_orientation))
        .Times(1);

    teleport_action.Start();
    EXPECT_NO_THROW(teleport_action.Step());
}

}  // namespace OPENSCENARIO
