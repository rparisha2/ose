/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Actions/FollowTrajectoryAction.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"
#include "builders/PositionBuilder.h"

#include <gtest/gtest.h>

namespace OPENSCENARIO
{
using namespace units::literals;
using namespace OPENSCENARIO::TESTING;

class FollowTrajectoryActionTestFixture : public OpenScenarioEngineLibraryTestBase
{

  protected:
    void SetUp() override { OpenScenarioEngineLibraryTestBase::SetUp(); }
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPolylineWriter> GetPolylineWithRelativeLanePositions()
    {
        int relative_lane{1};
        double offset1{1.0};
        double offset2{1.0};

        OPENSCENARIO::TESTING::FakeRelativeLanePositionBuilder fake_relative_lane_position_builder1{relative_lane,
                                                                                                    offset1};
        OPENSCENARIO::TESTING::FakeRelativeLanePositionBuilder fake_relative_lane_position_builder2{relative_lane,
                                                                                                    offset2};

        fake_relative_lane_position_builder1.WithDs(1.0);
        fake_relative_lane_position_builder1.WithDsLane(2.0);

        fake_relative_lane_position_builder2.WithDs(1.0);
        fake_relative_lane_position_builder2.WithDsLane(2.0);
        return FakePolylineBuilder()
            .WithVertices({FakeVertexBuilder(0)
                               .WithPosition(FakePositionBuilder{}
                                                 .WithRelativeLanePosition(fake_relative_lane_position_builder1.Build())
                                                 .Build())
                               .Build(),
                           FakeVertexBuilder(1)
                               .WithPosition(FakePositionBuilder{}
                                                 .WithRelativeLanePosition(fake_relative_lane_position_builder2.Build())
                                                 .Build())
                               .Build()})
            .Build();
    }
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPolylineWriter> GetPolylineWithGeoPositions()
    {
        mantle_api::LatLonPosition lat_lon_position1{42.123_deg, 11.65874_deg};
        mantle_api::LatLonPosition lat_lon_position2{52.123_deg, 11.65874_deg};

        return FakePolylineBuilder()
            .WithVertices({FakeVertexBuilder(0)
                               .WithPosition(FakePositionBuilder{}
                                                 .WithGeoPosition(FakeGeoPositionBuilder(lat_lon_position1.latitude(),
                                                                                         lat_lon_position1.longitude())
                                                                      .Build())
                                                 .Build())
                               .Build(),
                           FakeVertexBuilder(1)
                               .WithPosition(FakePositionBuilder{}
                                                 .WithGeoPosition(FakeGeoPositionBuilder(lat_lon_position2.latitude(),
                                                                                         lat_lon_position2.longitude())
                                                                      .Build())
                                                 .Build())
                               .Build()})
            .Build();
    }
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPolylineWriter> GetPolylineWithLanePositions()
    {
        mantle_api::OpenDriveLanePosition open_drive_position1{"1", 2, 3.0_m, 0.0_m};
        mantle_api::OpenDriveLanePosition open_drive_position2{"1", 2, 4.0_m, 0.0_m};
        return FakePolylineBuilder()
            .WithVertices({FakeVertexBuilder(0)
                               .WithPosition(FakePositionBuilder{}
                                                 .WithLanePosition(
                                                     FakeLanePositionBuilder(open_drive_position1.road,
                                                                             std::to_string(open_drive_position1.lane),
                                                                             open_drive_position1.t_offset(),
                                                                             open_drive_position1.s_offset())
                                                         .Build())
                                                 .Build())
                               .Build(),
                           FakeVertexBuilder(1)
                               .WithPosition(FakePositionBuilder{}
                                                 .WithLanePosition(
                                                     FakeLanePositionBuilder(open_drive_position2.road,
                                                                             std::to_string(open_drive_position2.lane),
                                                                             open_drive_position2.t_offset(),
                                                                             open_drive_position2.s_offset())
                                                         .Build())
                                                 .Build())
                               .Build()})
            .Build();
    }
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPolylineWriter> GetPolylineWithWorldPositions()
    {
        mantle_api::Vec3<units::length::meter_t> position1{1.0_m, 2.0_m, 0.0_m};
        mantle_api::Orientation3<units::angle::radian_t> orientation1{0.1_rad, 0.0_rad, 0.0_rad};
        mantle_api::Vec3<units::length::meter_t> position2{1.1_m, 2.1_m, 0.0_m};

        return FakePolylineBuilder()
            .WithVertices({FakeVertexBuilder(0)
                               .WithPosition(FakePositionBuilder{}
                                                 .WithWorldPosition(FakeWorldPositionBuilder(position1.x.value(),
                                                                                             position1.y.value(),
                                                                                             position1.z.value(),
                                                                                             orientation1.yaw.value(),
                                                                                             orientation1.pitch.value(),
                                                                                             orientation1.roll.value())
                                                                        .Build())
                                                 .Build())
                               .Build(),
                           FakeVertexBuilder(1)
                               .WithPosition(FakePositionBuilder{}
                                                 .WithWorldPosition(FakeWorldPositionBuilder(position2.x.value(),
                                                                                             position2.y.value(),
                                                                                             position2.z.value(),
                                                                                             orientation1.yaw.value(),
                                                                                             orientation1.pitch.value(),
                                                                                             orientation1.roll.value())
                                                                        .Build())
                                                 .Build())
                               .Build()})
            .Build();
    }
};

TEST_F(FollowTrajectoryActionTestFixture,
       GivenFollowTrajectoryActionWithWorldPosition_WhenStartAction_ThenTransformedIntoCorrespondingControlStrategy)
{

    auto fake_follow_trajectory_action =
        FakeFollowTrajectoryActionBuilder{}
            .WithTimeReference(FakeTimeReferenceBuilder().Build())
            .WithTrajectory(FakeTrajectoryBuilder("TestTrajectory", false)
                                .WithShape(FakeShapeBuilder().WithPolyline(GetPolylineWithWorldPositions()).Build())
                                .Build())
            .Build();

    OPENSCENARIO::FollowTrajectoryAction follow_trajectory_action{
        fake_follow_trajectory_action, env_, std::vector<std::string>{"Ego"}};

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};

    mantle_api::Vec3<units::length::meter_t> expected_position1{1_m, 2_m, 3_m};
    mantle_api::Vec3<units::length::meter_t> expected_position2{2_m, 3_m, 4_m};
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation1{0.1_rad, 0.0_rad, 0.0_rad};

    mantle_api::Pose expected_pose1{expected_position1, expected_orientation1};
    mantle_api::Pose expected_pose2{expected_position2, expected_orientation1};

    EXPECT_CALL(dynamic_cast<const mantle_api::MockGeometryHelper&>(*(env_->GetGeometryHelper())),
                TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
        .WillOnce(testing::Return(expected_position1))
        .WillOnce(testing::Return(expected_position2));

    EXPECT_CALL(*env_, UpdateControlStrategies(testing::_, testing::_))
        .WillOnce(testing::SaveArg<1>(&control_strategies));

    follow_trajectory_action.Start();
    EXPECT_NO_THROW(follow_trajectory_action.Step());

    ASSERT_THAT(control_strategies, testing::SizeIs(1));
    EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowTrajectory);
    auto& trajectory =
        std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(control_strategies.front())->trajectory;
    auto& polyline = std::get<mantle_api::PolyLine>(trajectory.type);
    EXPECT_THAT(polyline, testing::SizeIs(2));
    EXPECT_EQ(polyline.at(0).time, 0.0_s);
    EXPECT_EQ(polyline.at(0).pose, expected_pose1);
    EXPECT_EQ(polyline.at(1).time, 1.0_s);
    EXPECT_EQ(polyline.at(1).pose, expected_pose2);
}

TEST_F(FollowTrajectoryActionTestFixture,
       GivenFollowTrajectoryActionWithLanePosition_WhenStartAction_ThenTransformedIntoCorrespondingControlStrategy)
{

    using namespace OPENSCENARIO::TESTING;
    auto fake_follow_trajectory_action =
        FakeFollowTrajectoryActionBuilder{}
            .WithTimeReference(FakeTimeReferenceBuilder().Build())
            .WithTrajectory(FakeTrajectoryBuilder("TestTrajectory", false)
                                .WithShape(FakeShapeBuilder().WithPolyline(GetPolylineWithLanePositions()).Build())
                                .Build())
            .Build();

    OPENSCENARIO::FollowTrajectoryAction follow_trajectory_action{
        fake_follow_trajectory_action, env_, std::vector<std::string>{"Ego"}};

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};

    mantle_api::Vec3<units::length::meter_t> expected_position1{1_m, 2_m, 3_m};
    mantle_api::Vec3<units::length::meter_t> expected_position2{2_m, 3_m, 4_m};
    mantle_api::Orientation3<units::angle::radian_t> orientation{0.0_rad, 0.0_rad, 0.0_rad};

    mantle_api::Pose expected_pose1{expected_position1, orientation};
    mantle_api::Pose expected_pose2{expected_position2, orientation};

    EXPECT_CALL(dynamic_cast<const mantle_api::MockGeometryHelper&>(*(env_->GetGeometryHelper())),
                TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
        .WillOnce(testing::Return(expected_position1))
        .WillOnce(testing::Return(expected_position2));

    EXPECT_CALL(*env_, UpdateControlStrategies(testing::_, testing::_))
        .WillOnce(testing::SaveArg<1>(&control_strategies));

    EXPECT_CALL(dynamic_cast<const mantle_api::MockConverter&>(*(env_->GetConverter())), Convert(testing::_))
        .WillOnce(testing::Return(expected_position1))
        .WillOnce(testing::Return(expected_position2));

    follow_trajectory_action.Start();
    EXPECT_NO_THROW(follow_trajectory_action.Step());

    ASSERT_THAT(control_strategies, testing::SizeIs(1));
    EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowTrajectory);
    auto& trajectory =
        std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(control_strategies.front())->trajectory;
    auto& polyline = std::get<mantle_api::PolyLine>(trajectory.type);
    EXPECT_THAT(polyline, testing::SizeIs(2));
    EXPECT_EQ(polyline.at(0).time, 0.0_s);
    EXPECT_EQ(polyline.at(0).pose, expected_pose1);
    EXPECT_EQ(polyline.at(1).time, 1.0_s);
    EXPECT_EQ(polyline.at(1).pose, expected_pose2);
}

TEST_F(FollowTrajectoryActionTestFixture,
       GivenFollowTrajectoryActionWithGeoPosition_WhenStartAction_ThenTransformedIntoCorrespondingControlStrategy)
{

    auto fake_follow_trajectory_action =
        FakeFollowTrajectoryActionBuilder{}
            .WithTimeReference(FakeTimeReferenceBuilder().Build())
            .WithTrajectory(FakeTrajectoryBuilder("TestTrajectory", false)
                                .WithShape(FakeShapeBuilder().WithPolyline(GetPolylineWithGeoPositions()).Build())
                                .Build())
            .Build();

    OPENSCENARIO::FollowTrajectoryAction follow_trajectory_action{
        fake_follow_trajectory_action, env_, std::vector<std::string>{"Ego"}};

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};

    mantle_api::Vec3<units::length::meter_t> expected_position1{1_m, 2_m, 3_m};
    mantle_api::Vec3<units::length::meter_t> expected_position2{2_m, 3_m, 4_m};
    mantle_api::Orientation3<units::angle::radian_t> orientation{0.0_rad, 0.0_rad, 0.0_rad};

    mantle_api::Pose expected_pose1{expected_position1, orientation};
    mantle_api::Pose expected_pose2{expected_position2, orientation};

    EXPECT_CALL(*env_, UpdateControlStrategies(testing::_, testing::_))
        .WillOnce(testing::SaveArg<1>(&control_strategies));

    EXPECT_CALL(dynamic_cast<const mantle_api::MockGeometryHelper&>(*(env_->GetGeometryHelper())),
                TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
        .WillOnce(testing::Return(expected_position1))
        .WillOnce(testing::Return(expected_position2));

    EXPECT_CALL(dynamic_cast<const mantle_api::MockConverter&>(*(env_->GetConverter())), Convert(testing::_))
        .WillOnce(testing::Return(expected_position1))
        .WillOnce(testing::Return(expected_position2));

    follow_trajectory_action.Start();
    EXPECT_NO_THROW(follow_trajectory_action.Step());

    ASSERT_THAT(control_strategies, testing::SizeIs(1));
    EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowTrajectory);
    auto& trajectory =
        std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(control_strategies.front())->trajectory;
    auto& polyline = std::get<mantle_api::PolyLine>(trajectory.type);
    EXPECT_THAT(polyline, testing::SizeIs(2));
    EXPECT_EQ(polyline.at(0).time, 0.0_s);
    EXPECT_EQ(polyline.at(0).pose, expected_pose1);
    EXPECT_EQ(polyline.at(1).time, 1.0_s);
    EXPECT_EQ(polyline.at(1).pose, expected_pose2);
}

TEST_F(FollowTrajectoryActionTestFixture,
       GivenFollowTrajectoryActionWithRelativeLane_WhenStartAction_ThenTransformedIntoCorrespondingControlStrategy)
{

    auto fake_follow_trajectory_action =
        FakeFollowTrajectoryActionBuilder{}
            .WithTimeReference(FakeTimeReferenceBuilder().Build())
            .WithTrajectory(
                FakeTrajectoryBuilder("TestTrajectory", false)
                    .WithShape(FakeShapeBuilder().WithPolyline(GetPolylineWithRelativeLanePositions()).Build())
                    .Build())
            .Build();

    OPENSCENARIO::FollowTrajectoryAction follow_trajectory_action{
        fake_follow_trajectory_action, env_, std::vector<std::string>{"Ego"}};

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies{};

    mantle_api::Vec3<units::length::meter_t> expected_position1{1_m, 2_m, 3_m};
    mantle_api::Vec3<units::length::meter_t> expected_position2{2_m, 3_m, 4_m};
    mantle_api::Orientation3<units::angle::radian_t> orientation{0.0_rad, 0.0_rad, 0.0_rad};

    mantle_api::Pose expected_pose1{expected_position1, orientation};
    mantle_api::Pose expected_pose2{expected_position2, orientation};

    EXPECT_CALL(*env_, UpdateControlStrategies(testing::_, testing::_))
        .WillOnce(testing::SaveArg<1>(&control_strategies));

    EXPECT_CALL(dynamic_cast<const mantle_api::MockGeometryHelper&>(*(env_->GetGeometryHelper())),
                TranslateGlobalPositionLocally(testing::_, testing::_, testing::_))
        .WillOnce(testing::Return(expected_position1))
        .WillOnce(testing::Return(expected_position2));

    follow_trajectory_action.Start();
    EXPECT_NO_THROW(follow_trajectory_action.Step());

    ASSERT_THAT(control_strategies, testing::SizeIs(1));
    EXPECT_EQ(control_strategies.front()->type, mantle_api::ControlStrategyType::kFollowTrajectory);
    auto& trajectory =
        std::dynamic_pointer_cast<mantle_api::FollowTrajectoryControlStrategy>(control_strategies.front())->trajectory;
    auto& polyline = std::get<mantle_api::PolyLine>(trajectory.type);
    EXPECT_THAT(polyline, testing::SizeIs(2));
    EXPECT_EQ(polyline.at(0).time, 0.0_s);
    EXPECT_EQ(polyline.at(0).pose, expected_pose1);
    EXPECT_EQ(polyline.at(1).time, 1.0_s);
    EXPECT_EQ(polyline.at(1).pose, expected_pose2);
}

TEST_F(FollowTrajectoryActionTestFixture,
       GivenFollowTrajectoryActionWithRelativeLane_WhenStepAction_ThenOnlyCompleteAfterControlStrategyGoalReached)
{
    auto fake_follow_trajectory_action =
        FakeFollowTrajectoryActionBuilder{}
            .WithTimeReference(FakeTimeReferenceBuilder().Build())
            .WithTrajectory(
                FakeTrajectoryBuilder("TestTrajectory", false)
                    .WithShape(FakeShapeBuilder().WithPolyline(GetPolylineWithRelativeLanePositions()).Build())
                    .Build())
            .Build();

    OPENSCENARIO::FollowTrajectoryAction follow_trajectory_action{
        fake_follow_trajectory_action, env_, std::vector<std::string>{"Ego"}};

    EXPECT_CALL(*env_, HasControlStrategyGoalBeenReached(0, mantle_api::ControlStrategyType::kFollowTrajectory))
        .Times(2)
        .WillOnce(::testing::Return(false))
        .WillRepeatedly(::testing::Return(true));
    EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_)).Times(3);

    follow_trajectory_action.Start();
    follow_trajectory_action.Step();
    EXPECT_FALSE(follow_trajectory_action.isComplete());
    follow_trajectory_action.Step();
    EXPECT_TRUE(follow_trajectory_action.isComplete());
    follow_trajectory_action.Step();
}

}  // namespace OPENSCENARIO
