/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Conditions/ByEntityConditions/ByEntityConditionsBase.h"
#include "TestUtils.h"
#include "builders/condition_builder.h"
#include "builders/storyboard_builder.h"

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

namespace OPENSCENARIO
{

class MockByEntityConditions : public ByEntityConditionsBase
{
  public:
    MockByEntityConditions(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggeringEntities> entities)
        : ByEntityConditionsBase(entities)
    {
    }

    MOCK_METHOD(bool, CheckPreconditions, (), (const, override));
    MOCK_METHOD(bool,
                CheckIsSatisfied,
                (std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEntityRef> entity),
                (const, override));
};

class ByEntityConditionsBaseTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override { OpenScenarioEngineLibraryTestBase::SetUp(); }

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesImpl> GenerateFakeTrigger(
        const NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum rule =
            NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ALL)
    {
        auto fake_trigger_entities = OPENSCENARIO::TESTING::FakeTriggerEntitiesBuilder()
                                         .WithTriggeringEntitiesRule(rule)
                                         .WithEntityRefs({"entity_ref_0", "entity_ref_1", "entity_ref_2"})
                                         .Build();

        return std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesImpl>(fake_trigger_entities);
    }
};

TEST_F(ByEntityConditionsBaseTestFixture, GivenConditionNotSupported_WheEvaluate_ThenConditionIsSatisfied)
{
    MockByEntityConditions mock_condition{GenerateFakeTrigger()};

    EXPECT_CALL(mock_condition, CheckPreconditions()).WillOnce(::testing::Return(false));
    EXPECT_TRUE(mock_condition.IsSatisfied());
}

TEST_F(ByEntityConditionsBaseTestFixture,
       GivenConditionTriggerRuleAllAndAllTriggeringEntitiesAreSatisfied_WheEvaluate_ThenConditionIsSatisfied)
{
    MockByEntityConditions mock_condition{
        GenerateFakeTrigger(NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ALL)};

    EXPECT_CALL(mock_condition, CheckPreconditions()).WillOnce(::testing::Return(true));
    EXPECT_CALL(mock_condition, CheckIsSatisfied(::testing::NotNull())).WillRepeatedly(::testing::Return(true));
    EXPECT_TRUE(mock_condition.IsSatisfied());
}

TEST_F(ByEntityConditionsBaseTestFixture,
       GivenConditionTriggerRuleAllAndNotAllTriggeringEntitiesAreSatisfied_WheEvaluate_ThenConditionIsNotSatisfied)
{
    MockByEntityConditions mock_condition{
        GenerateFakeTrigger(NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ALL)};

    EXPECT_CALL(mock_condition, CheckPreconditions()).WillOnce(::testing::Return(true));
    EXPECT_CALL(mock_condition, CheckIsSatisfied(::testing::NotNull()))
        .WillOnce(::testing::Return(false))
        .WillRepeatedly(::testing::Return(true));
    EXPECT_FALSE(mock_condition.IsSatisfied());
}

TEST_F(ByEntityConditionsBaseTestFixture,
       GivenConditionTriggerRuleAnyAndOneOfTriggeringEntitiesIsSatisfied_WheEvaluate_ThenConditionIsSatisfied)
{
    MockByEntityConditions mock_condition{
        GenerateFakeTrigger(NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ANY)};

    EXPECT_CALL(mock_condition, CheckPreconditions()).WillOnce(::testing::Return(true));
    EXPECT_CALL(mock_condition, CheckIsSatisfied(::testing::NotNull()))
        .WillOnce(::testing::Return(true))
        .WillRepeatedly(::testing::Return(false));
    EXPECT_TRUE(mock_condition.IsSatisfied());
}

TEST_F(ByEntityConditionsBaseTestFixture,
       GivenConditionTriggerRuleAnyAndNoneOfTriggeringEntitiesIsSatisfied_WheEvaluate_ThenConditionIsNotSatisfied)
{
    MockByEntityConditions mock_condition{
        GenerateFakeTrigger(NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ANY)};

    EXPECT_CALL(mock_condition, CheckPreconditions()).WillOnce(::testing::Return(true));
    EXPECT_CALL(mock_condition, CheckIsSatisfied(::testing::NotNull())).WillRepeatedly(::testing::Return(false));
    EXPECT_FALSE(mock_condition.IsSatisfied());
}

}  // namespace OPENSCENARIO
