/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "StoryboardElement.h"

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

namespace OPENSCENARIO
{

class Act : public StoryboardElement
{
  public:
    explicit Act(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAct> act_data);

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAct> data_;
};

}  // namespace OPENSCENARIO
