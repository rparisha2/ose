/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "Storyboard/Actions/PrivateAction.h"

namespace OPENSCENARIO
{

class VisibilityAction : public PrivateAction
{
  public:
    VisibilityAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IVisibilityAction> visibility_action_data,
                   std::shared_ptr<mantle_api::IEnvironment> environment,
                   const std::vector<std::string>& actors);

    void ExecuteStartTransition() final;
    void Step() final;

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IVisibilityAction> visibility_action_data_;
};

}  // namespace OPENSCENARIO
