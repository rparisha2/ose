/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SpeedAction.h"

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Traffic/entity_helper.h>

#include <iostream>
#include <map>

using namespace units::literals;

namespace OPENSCENARIO
{

std::map<std::string, mantle_api::Dimension> enum_mapper_dynamics_dimension{
    {"UNKNOWN", mantle_api::Dimension::kUndefined},
    {"distance", mantle_api::Dimension::kDistance},
    {"rate", mantle_api::Dimension::kRate},
    {"time", mantle_api::Dimension::kTime},
};

std::map<std::string, mantle_api::Shape> enum_mapper_dynamics_shape{
    {"UNKNOWN", mantle_api::Shape::kUndefined},
    {"cubic", mantle_api::Shape::kCubic},
    {"linear", mantle_api::Shape::kLinear},
    {"sinusoidal", mantle_api::Shape::kSinusoidal},
    {"step", mantle_api::Shape::kStep},
};

SpeedAction::SpeedAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ISpeedAction> speed_action_data,
                         std::shared_ptr<mantle_api::IEnvironment> environment,
                         const std::vector<std::string>& actors)
    : MotionControlAction(environment, actors), speed_action_data_(speed_action_data)
{
}

void SpeedAction::StartAction()
{
    auto speed_action_target = speed_action_data_->GetSpeedActionTarget();
    if (!speed_action_target)
    {
        throw std::runtime_error("SpeedAction does not contain SpeedActionTarget");
    }
    const auto target_speed = GetTargetSpeed(speed_action_target.get());

    auto speed_action_dynamics = speed_action_data_->GetSpeedActionDynamics();
    if (!speed_action_dynamics)
    {
        throw std::runtime_error("SpeedAction does not contain SpeedActionDynamics");
    }

    auto dynamics_shape = speed_action_dynamics->GetDynamicsShape();
    if (dynamics_shape == NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::STEP)
    {
        for (auto actor : actors_)
        {
            auto entity = environment_->GetEntityRepository().Get(actor);
            if (!entity.has_value())
            {
                throw std::runtime_error("Entity with name \"" + actor +
                                         "\" for SpeedAction does not exist. Please adjust the scenario.");
            }

            mantle_api::SetSpeed(&entity.value().get(), target_speed);
        }
    }
    else  // all other shapes
    {
        if (mantle_api::LessOrEqual(units::velocity::meters_per_second_t(speed_action_dynamics->GetValue()), 0.0_mps))
        {
            throw std::runtime_error(
                "SpeedAction cannot have dynamics value for rate, time or distance <= 0.0. Please adjust the "
                "scenario.");
        }

        if (dynamics_shape == NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::LINEAR)
        {
            for (auto actor : actors_)
            {
                SetLinearVelocitySplineControlStrategy(actor, target_speed, speed_action_dynamics.get());
            }
        }
        else if (dynamics_shape == NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::CUBIC)
        {
            // TODO: implement
            std::cout << "OpenScenarioEngine: Dynamics shape CUBIC not yet implemented for SpeedAction. Replacing with "
                         "linear shape."
                      << std::endl;
            for (auto actor : actors_)
            {
                SetLinearVelocitySplineControlStrategy(actor, target_speed, speed_action_dynamics.get());
            }
        }
        else
        {
            std::cout << "Dynamics shape " << dynamics_shape.GetLiteral() << " not yet implemented for SpeedAction"
                      << std::endl;
        }
    }
}

bool SpeedAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
    auto entity = environment_->GetEntityRepository().Get(actor);
    if (!entity.has_value())
    {
        return false;
    }

    return speed_action_data_->GetSpeedActionDynamics()->GetDynamicsShape() ==
               NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::STEP ||
           (speed_action_data_->GetSpeedActionDynamics()->GetDynamicsShape() ==
                NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::LINEAR &&
            environment_->HasControlStrategyGoalBeenReached(entity.value().get().GetUniqueId(),
                                                            mantle_api::ControlStrategyType::kFollowVelocitySpline));
}

units::velocity::meters_per_second_t SpeedAction::GetTargetSpeed(
    NET_ASAM_OPENSCENARIO::v1_1::ISpeedActionTarget* speed_action_target)
{
    units::velocity::meters_per_second_t target_speed{0};
    auto absolute_target_speed = speed_action_target->GetAbsoluteTargetSpeed();
    auto relative_target_speed = speed_action_target->GetRelativeTargetSpeed();
    if (absolute_target_speed)
    {
        target_speed = units::velocity::meters_per_second_t(absolute_target_speed->GetValue());
    }
    else if (relative_target_speed)
    {
        /// TODO: continuous need to be taken into consideration at some point as well
        const auto& relative_entity_name = relative_target_speed->GetEntityRef()->GetNameRef();

        if (!environment_->GetEntityRepository().Get(relative_entity_name).has_value())
        {
            throw std::runtime_error("Relative Entity with name \"" + relative_entity_name +
                                     "\" of SpeedAction  does not exist. Please adjust the "
                                     "scenario.");
        }

        auto target_base = environment_->GetEntityRepository().Get(relative_entity_name).value().get().GetVelocity();
        units::velocity::meters_per_second_t base_magnitude{target_base.Length()};

        if (relative_target_speed->GetSpeedTargetValueType() ==
            NET_ASAM_OPENSCENARIO::v1_1::SpeedTargetValueType::DELTA)
        {
            target_speed = base_magnitude + units::velocity::meters_per_second_t(relative_target_speed->GetValue());
        }
        else if (relative_target_speed->GetSpeedTargetValueType() ==
                 NET_ASAM_OPENSCENARIO::v1_1::SpeedTargetValueType::FACTOR)
        {
            target_speed = base_magnitude * relative_target_speed->GetValue();
        }
    }
    else
    {
        throw std::runtime_error("SpeedAction needs to have either Relative or Absolute target speed");
    }

    return target_speed;
}

void SpeedAction::SetLinearVelocitySplineControlStrategy(
    const std::string& actor,
    units::velocity::meters_per_second_t target_speed,
    NET_ASAM_OPENSCENARIO::v1_1::ITransitionDynamics* transition_dynamics)
{
    if (!environment_->GetEntityRepository().Get(actor).has_value())
    {
        std::cout << "Entity with name \"" << actor << "\" for SpeedAction does not exist. Please adjust the scenario.";
        return;
    }

    const auto& entity = environment_->GetEntityRepository().Get(actor).value().get();

    mantle_api::SplineSection<units::velocity::meters_per_second> spline_section;
    spline_section.start_time = mantle_api::Time(0);
    if (transition_dynamics->GetDynamicsDimension() == NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::TIME)
    {
        spline_section.end_time = units::time::second_t(transition_dynamics->GetValue());
        std::get<2>(spline_section.polynomial) =
            (target_speed - entity.GetVelocity().Length()) / units::time::second_t(transition_dynamics->GetValue());
    }
    else if (transition_dynamics->GetDynamicsDimension() == NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::RATE)
    {
        spline_section.end_time = units::math::abs(target_speed - entity.GetVelocity().Length()) /
                                  units::acceleration::meters_per_second_squared_t(transition_dynamics->GetValue());
        std::get<2>(spline_section.polynomial) =
            units::math::copysign(units::acceleration::meters_per_second_squared_t(transition_dynamics->GetValue()),
                                  target_speed - entity.GetVelocity().Length());
    }
    else if (transition_dynamics->GetDynamicsDimension() == NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::DISTANCE)
    {
        auto start_speed = entity.GetVelocity().Length().value();
        // Distance has to be converted to time, since spline is expected over time, not distance
        //  distance = time(v_init + v_delta/2) = time(v_init + v_end)/2 => time = 2*distance/(v_init + v_end)
        spline_section.end_time =
            units::time::second_t(2 * transition_dynamics->GetValue() / (target_speed.value() + start_speed));
        // v_end = v_init + acceleration*time, where time = 2*distance/(v_init + v_end) then acceleration = (v_end -
        // v_init) * (v_end + v_init) / 2* distance
        auto rate = units::acceleration::meters_per_second_squared_t((target_speed.value() - start_speed) *
                                                                     (target_speed.value() + start_speed) /
                                                                     (2 * transition_dynamics->GetValue()));
        std::get<2>(spline_section.polynomial) = units::math::copysign(
            (units::acceleration::meters_per_second_squared_t(rate)), target_speed - entity.GetVelocity().Length());
    }
    std::get<3>(spline_section.polynomial) = entity.GetVelocity().Length();

    auto control_strategy = std::make_shared<mantle_api::FollowVelocitySplineControlStrategy>();
    control_strategy->default_value = target_speed;
    control_strategy->velocity_splines.push_back(spline_section);
    environment_->UpdateControlStrategies(entity.GetUniqueId(), {control_strategy});
}

}  // namespace OPENSCENARIO
