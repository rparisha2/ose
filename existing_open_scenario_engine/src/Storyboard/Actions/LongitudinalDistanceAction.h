/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "MotionControlAction.h"

namespace OPENSCENARIO
{

class LongitudinalDistanceAction : public MotionControlAction
{
  public:
    LongitudinalDistanceAction(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILongitudinalDistanceAction> longitudinal_distance_action_data,
        std::shared_ptr<mantle_api::IEnvironment> environment,
        const std::vector<std::string>& actors);

  private:
    void StartAction() override;
    bool HasControlStrategyGoalBeenReached(const std::string& actor) override;
    bool IsLongitudinal() override { return true; }
    bool IsLateral() override { return false; }
    std::optional<mantle_api::Direction> ResolveDirection(
        const NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement& displacement);

    bool InitActionProperties();

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILongitudinalDistanceAction> longitudinal_distance_action_data_;
    mantle_api::Direction direction_{};
    double distance_{};
};

}  // namespace OPENSCENARIO
