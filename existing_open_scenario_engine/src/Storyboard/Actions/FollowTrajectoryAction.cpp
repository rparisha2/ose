/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "FollowTrajectoryAction.h"

#include "Storyboard/Utils/ConvertScenarioPosition.h"
#include "Storyboard/Utils/EntityUtils.h"

#include <iostream>
#include <optional>

using namespace units::literals;

namespace OPENSCENARIO
{

FollowTrajectoryAction::FollowTrajectoryAction(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IFollowTrajectoryAction> follow_trajectory_action_data,
    std::shared_ptr<mantle_api::IEnvironment> environment,
    const std::vector<std::string>& actors)
    : MotionControlAction(environment, actors), follow_trajectory_action_data(follow_trajectory_action_data)
{
}

std::optional<mantle_api::ReferenceContext> FollowTrajectoryAction::ConvertDomainReference(
    const NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext& reference_context)
{
    switch (reference_context.GetFromLiteral(reference_context.GetLiteral()))
    {
        case NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::ABSOLUTE:
            return mantle_api::ReferenceContext::kAbsolute;
        case NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::RELATIVE:
            return mantle_api::ReferenceContext::kRelative;
        case NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::UNKNOWN:
            return {};
    }
    throw std::runtime_error("FollowTrajectoryAction: the given ReferenceContext is not supported.");
}

void FollowTrajectoryAction::StartAction()
{

    for (auto actor : actors_)
    {

        const auto& ref_entity = EntityUtils::GetEntityByName(environment_, actor);

        std::optional<mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference> timeReference{std::nullopt};

        auto follow_trajectory_action_timing = follow_trajectory_action_data->GetTimeReference()->GetTiming();
        if (follow_trajectory_action_timing)
        {
            mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference timing;
            auto domain_reference =
                ConvertDomainReference(follow_trajectory_action_timing->GetDomainAbsoluteRelative());
            timing.domainAbsoluteRelative = domain_reference.value();
            timing.offset = units::time::second_t(follow_trajectory_action_timing->GetOffset());
            timing.scale = follow_trajectory_action_timing->GetScale();
            timeReference = timing;
        }

        auto follow_trajectory_action_trajectory_ref = follow_trajectory_action_data->GetTrajectoryRef();

        if (!follow_trajectory_action_trajectory_ref)
        {
            throw std::runtime_error(
                "FollowTrajectoryAction does not contain TrajectoryRef.Deprecated element Trajectory is not "
                "supported.");
        }

        mantle_api::Trajectory trajectory;

        if (follow_trajectory_action_trajectory_ref->GetTrajectory())
        {
            auto follow_trajectory_action_trajectory = follow_trajectory_action_trajectory_ref->GetTrajectory();
            trajectory.name = follow_trajectory_action_trajectory->GetName();

            auto follow_trajectory_action_trajectory_shape = follow_trajectory_action_trajectory->GetShape();

            if (follow_trajectory_action_trajectory_shape->GetPolyline())
            {
                mantle_api::PolyLine poly_line;

                int i = 0;
                for (auto poly_line_vertex : follow_trajectory_action_trajectory_shape->GetPolyline()->GetVertices())
                {
                    mantle_api::PolyLinePoint poly_line_point;
                    i++;
                    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPosition> poly_line_vertex_position =
                        poly_line_vertex->GetPosition();
                    auto pose = ConvertScenarioPosition(environment_, poly_line_vertex_position);
                    if (pose.has_value())
                    {
                        pose.value().position = environment_->GetGeometryHelper()->TranslateGlobalPositionLocally(
                            pose.value().position,
                            pose.value().orientation,
                            ref_entity.GetProperties()->bounding_box.geometric_center);

                        // TODO: if no orientation is given in the polyline point, then orientation shall be aligned
                        // with the trajectory orientation (and not 0,0,0 in global coordinate system).
                        poly_line_point.pose = pose.value();
                        poly_line_point.time = units::time::second_t(poly_line_vertex->GetTime());

                        poly_line.push_back(poly_line_point);
                    }
                    else
                    {
                        std::cout << "FollowTrajectoryAction: Polyline position with index: " << i - 1
                                  << " could not be converted to global coordinate system. Not executing action.";
                        return;
                    }
                }

                trajectory.type = poly_line;
            }
            else
            {
                std::cout << "FollowTrajectoryAction currently only supports PolyLine shapes.";
            }
        }
        else
        {
            // TODO CatalogReference
        }

        auto control_strategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
        control_strategy->trajectory = trajectory;
        control_strategy->timeReference = timeReference;
        environment_->UpdateControlStrategies(
            environment_->GetEntityRepository().Get(actor).value().get().GetUniqueId(), {control_strategy});
    }
}

bool FollowTrajectoryAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
    const auto& ref_entity = EntityUtils::GetEntityByName(environment_, actor);

    return environment_->HasControlStrategyGoalBeenReached(ref_entity.GetUniqueId(),
                                                           mantle_api::ControlStrategyType::kFollowTrajectory);
}

}  // namespace OPENSCENARIO
