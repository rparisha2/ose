/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LaneChangeAction.h"

#include "Storyboard/Utils/ConvertToMantleApi.h"

namespace OPENSCENARIO
{

LaneChangeAction::LaneChangeAction(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILaneChangeAction> lane_change_action_data,
    std::shared_ptr<mantle_api::IEnvironment> environment,
    const std::vector<std::string>& actors)
    : MotionControlAction(environment, actors),
      lane_change_action_data_(lane_change_action_data),
      control_strategy_(std::make_shared<mantle_api::PerformLaneChangeControlStrategy>())
{
}

void LaneChangeAction::StartAction()
{
    SetupControlStrategy();
    for (const auto& actor : actors_)
    {
        if (auto entity = environment_->GetEntityRepository().Get(actor))
        {
            environment_->UpdateControlStrategies(entity.value().get().GetUniqueId(), {control_strategy_});
        }
        else
        {
            throw std::runtime_error("LaneChangeAction: Entity with name \"" + actor +
                                     "\" does not exist. Please adjust the scenario.");
        }
    }
}

bool LaneChangeAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
    if (auto entity = environment_->GetEntityRepository().Get(actor))
    {
        return environment_->HasControlStrategyGoalBeenReached(entity.value().get().GetUniqueId(),
                                                               mantle_api::ControlStrategyType::kPerformLaneChange);
    }

    return false;
}

void LaneChangeAction::SetupControlStrategy()
{
    control_strategy_->target_lane_offset = units::length::meter_t{lane_change_action_data_->GetTargetLaneOffset()};
    if (const auto transition_dynamics = lane_change_action_data_->GetLaneChangeActionDynamics())
    {
        control_strategy_->transition_dynamics = ConvertToMantleApi(*transition_dynamics);
    }
    else
    {
        throw std::runtime_error("Warning: LaneChangeAction: LaneChangeActionDynamics is not set.");
    }

    const auto lane_change_target = lane_change_action_data_->GetLaneChangeTarget();
    if (!lane_change_target)
    {
        throw std::runtime_error("LaneChangeAction: Target lane is not set. Please adjust the scenario.");
    }

    if (const auto absolute_target_lane = lane_change_target->GetAbsoluteTargetLane())
    {
        control_strategy_->target_lane_id = std::stoul(absolute_target_lane->GetValue());
    }
    else if (const auto relative_target_lane = lane_change_target->GetRelativeTargetLane())
    {
        const auto relative_entity_name = relative_target_lane->GetEntityRef()->GetNameRef();
        const auto relative_entity = environment_->GetEntityRepository().Get(relative_entity_name);
        if (!relative_entity.has_value())
        {
            throw std::runtime_error("LaneChangeAction: Relative entity with name \"" + relative_entity_name +
                                     "\" does not exist. Please adjust the scenario.");
        }

        const auto relative_pose = mantle_api::Pose{relative_entity.value().get().GetPosition(),
                                                    relative_entity.value().get().GetOrientation()};
        if (const auto target_lane_id =
                environment_->GetQueryService().GetRelativeLaneId(relative_pose, relative_target_lane->GetValue()))
        {
            control_strategy_->target_lane_id = target_lane_id.value();
        }
        else
        {
            throw std::runtime_error("LaneChangeAction: Cannot find the target lane to the reference entity \"" +
                                     relative_entity_name + "\". Please adjust the scenario.");
        }
    }
    else
    {
        throw std::runtime_error("LaneChangeAction: Target lane is not set. Please adjust the scenario.");
    }
}

}  // namespace OPENSCENARIO
