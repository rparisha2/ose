/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "PrivateAction.h"

namespace OPENSCENARIO
{

class AssignRouteAction : public PrivateAction
{
  public:
    AssignRouteAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAssignRouteAction> assign_route_action_data,
                      std::shared_ptr<mantle_api::IEnvironment> environment,
                      const std::vector<std::string>& actors);
    void Step() final;

  private:
    void ExecuteStartTransition() final;
    mantle_api::RouteDefinition CreateRouteDefinition();
    mantle_api::RouteDefinition CreateRoute(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRoute> route_object);
    mantle_api::RouteStrategy ConvertRouteStrategy(
        const NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum& route_strategy_enum);

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAssignRouteAction> assign_route_action_data_;
};

}  // namespace OPENSCENARIO
