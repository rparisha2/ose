/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "MotionControlAction.h"

#include <exception>
namespace OPENSCENARIO
{

MotionControlAction::MotionControlAction(std::shared_ptr<mantle_api::IEnvironment> environment,
                                         const std::vector<std::string>& actors)
    : PrivateAction(environment, actors)
{
}

void MotionControlAction::Step()
{
    if (isRunning())
    {
        std::vector<std::string> newly_finished_actors;

        for (auto actor : actors_with_running_control_strategy_)
        {

            try
            {
                // It throws exception when there is not entity associated with actor
                // Temporary try-catch block to avoid the execution of the test failed
                if (HasControlStrategyGoalBeenReached(actor))
                {
                    if (IsLongitudinal())
                    {
                        ResetLongitudinalControlStrategyToDefault(actor);
                    }
                    if (IsLateral())
                    {
                        ResetLateralControlStrategyToDefault(actor);
                    }
                    newly_finished_actors.push_back(actor);
                }
            }
            catch (std::exception& e)
            {
                std::cout << e.what() << std::endl;
                newly_finished_actors.push_back(actor);
            }
        }

        for (auto actor : newly_finished_actors)
        {
            actors_with_running_control_strategy_.erase(std::find(
                actors_with_running_control_strategy_.begin(), actors_with_running_control_strategy_.end(), actor));
        }

        if (actors_with_running_control_strategy_.empty())
        {
            End();
        }
    }
}

void MotionControlAction::ExecuteStartTransition()
{
    actors_with_running_control_strategy_ = actors_;

    StartAction();
}

void MotionControlAction::ResetLongitudinalControlStrategyToDefault(const std::string& actor)
{
    auto entity = environment_->GetEntityRepository().Get(actor);

    if (entity.has_value())
    {
        environment_->UpdateControlStrategies(entity.value().get().GetUniqueId(),
                                              {std::make_shared<mantle_api::KeepVelocityControlStrategy>()});
    }
}

void MotionControlAction::ResetLateralControlStrategyToDefault(const std::string& actor)
{
    auto entity = environment_->GetEntityRepository().Get(actor);

    if (entity.has_value())
    {
        environment_->UpdateControlStrategies(entity.value().get().GetUniqueId(),
                                              {std::make_shared<mantle_api::KeepLaneOffsetControlStrategy>()});
    }
}

}  // namespace OPENSCENARIO
