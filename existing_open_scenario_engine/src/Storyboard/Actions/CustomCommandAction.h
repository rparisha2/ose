/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "Storyboard/Actions/UserDefinedAction.h"

#include <vector>

namespace OPENSCENARIO
{

class CustomCommandAction : public UserDefinedAction
{
  public:
    CustomCommandAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICustomCommandAction> custom_command_action_data,
      std::shared_ptr<mantle_api::IEnvironment> environment,
      const std::vector<std::string>& actors);

    void Step() final;

  private:
    void ExecuteStartTransition() final;

    const std::string type_;
    const std::string command_;
};

}  // namespace OPENSCENARIO
