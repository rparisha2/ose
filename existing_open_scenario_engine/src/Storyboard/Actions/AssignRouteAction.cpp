/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "AssignRouteAction.h"

#include "Storyboard/Utils/ConvertScenarioPosition.h"

#include <openScenarioLib/generated/v1_1/catalog/CatalogHelperV1_1.h>

namespace OPENSCENARIO
{
AssignRouteAction::AssignRouteAction(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAssignRouteAction> assign_route_action_data,
    std::shared_ptr<mantle_api::IEnvironment> environment,
    const std::vector<std::string>& actors)
    : PrivateAction(environment, actors), assign_route_action_data_(assign_route_action_data)
{
}

void AssignRouteAction::ExecuteStartTransition()
{
    auto route_definition = CreateRouteDefinition();
    for (const auto& actor : actors_)
    {
        if (auto entity = environment_->GetEntityRepository().Get(actor))
        {
            environment_->AssignRoute(entity.value().get().GetUniqueId(), route_definition);
        }
        else
        {
            throw std::runtime_error("AssignRouteAction: Entity with name \"" + actor +
                                     "\" does not exist. Please adjust the scenario.");
        }
    }
}

void AssignRouteAction::Step()
{
    if (isRunning())
    {
        End();
    }
}

mantle_api::RouteDefinition AssignRouteAction::CreateRouteDefinition()
{
    if (assign_route_action_data_->IsSetRoute())
    {
        const auto route = assign_route_action_data_->GetRoute();

        return CreateRoute(route);
    }
    else if (assign_route_action_data_->IsSetCatalogReference())
    {
        const auto catalog_reference = assign_route_action_data_->GetCatalogReference();
        auto catalog_element = catalog_reference->GetRef();
        if (catalog_element == nullptr)
        {
            throw std::runtime_error(std::string("AssignRouteAction: CatalogReference " +
                                                 catalog_reference->GetEntryName() + " without ref."));
        }
        if (NET_ASAM_OPENSCENARIO::v1_1::CatalogHelper::IsRoute(catalog_element))
        {
            return CreateRoute(NET_ASAM_OPENSCENARIO::v1_1::CatalogHelper::AsRoute(catalog_element));
        }
    }
    else
    {
        throw std::runtime_error("AssignRouteAction: No routes defined or referenced. Please adjust the scenario.");
    }
}

mantle_api::RouteDefinition AssignRouteAction::CreateRoute(
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRoute> route_object)
{
    mantle_api::RouteDefinition route_definition{};

    if (route_object->GetClosed())
    {
        std::cout
            << "OpenScenarioEngine: AssignRouteAction: Closed routes currently not supported. Assuming closed=false"
            << std::endl;
    }

    const auto route_waypoints = route_object->GetWaypoints();

    int i = 0;
    for (const auto& waypoint : route_waypoints)
    {
        i++;
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPosition> pos = waypoint->GetPosition();
        auto pose = ConvertScenarioPosition(environment_, pos);
        if (!pose.has_value())
        {
            std::cout << "AssignRouteAction: Could not convert scenario position to global position for route "
                      << route_object->GetName() << ", waypoint index " << i - 1 << ". Omitting waypoint" << std::endl;
            continue;
        }
        route_definition.waypoints.emplace_back(
            mantle_api::RouteWaypoint{pose.value().position,
                                      ConvertRouteStrategy(NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::GetFromLiteral(
                                          waypoint->GetRouteStrategy().GetLiteral()))});
    }
    return route_definition;
}

mantle_api::RouteStrategy AssignRouteAction::ConvertRouteStrategy(
    const NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum& route_strategy_enum)
{
    switch (route_strategy_enum)
    {
        case NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum::FASTEST:
            return mantle_api::RouteStrategy::kFastest;
        case NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum::LEAST_INTERSECTIONS:
            return mantle_api::RouteStrategy::kLeastIntersections;
        case NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum::SHORTEST:
            return mantle_api::RouteStrategy::kShortest;
        case NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum::RANDOM:   // intentional fall-through
        case NET_ASAM_OPENSCENARIO::v1_1::RouteStrategy::RouteStrategyEnum::UNKNOWN:  // intentional fall-through
        default:
            return mantle_api::RouteStrategy::kUndefined;
    }
}

}  // namespace OPENSCENARIO
