/*******************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TeleportAction.h"

#include "Storyboard/Utils/ConvertScenarioPosition.h"

#include <iostream>

namespace OPENSCENARIO
{

TeleportAction::TeleportAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITeleportAction> teleport_action_data,
                               std::shared_ptr<mantle_api::IEnvironment> environment,
                               const std::vector<std::string>& actors)
    : PrivateAction(environment, actors), teleport_action_data_(teleport_action_data)
{
}

void TeleportAction::ExecuteStartTransition()
{
    if (!teleport_action_data_->GetPosition())
    {
        throw std::runtime_error("TeleportAction does not contain Position");
    }

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPosition> pos = teleport_action_data_->GetPosition();
    const auto pose = ConvertScenarioPosition(environment_, pos);

    if (!pose.has_value())
    {
        std::cout << "Warning: TeleportAction Ignored: Failed to determine the pose for teleportation.\n";
        return;
    }

    for (auto actor : actors_)
    {
        auto entity_tmp = environment_->GetEntityRepository().Get(actor);

        if (!entity_tmp.has_value())
        {
            throw std::runtime_error("Actor '" + actor + "' in TeleportAction does not exist. Please adjust scenario.");
        }

        auto& entity = entity_tmp.value().get();
        auto position = environment_->GetGeometryHelper()->TranslateGlobalPositionLocally(
            pose.value().position, pose.value().orientation, entity.GetProperties()->bounding_box.geometric_center);

        if (const auto properties = dynamic_cast<mantle_api::StaticObjectProperties*>(entity.GetProperties()))
        {
            position =
            environment_->GetQueryService().GetUpwardsShiftedLanePosition(position, properties->vertical_offset(), true);
        }
  
        entity.SetPosition(position);
        std::cout << "TeleportAction: Setting Position of entity " << actor << " to (" << position.x() << ", "
                  << position.y() << ", " << position.z() << ")" << std::endl;

        entity.SetOrientation(pose.value().orientation);

        auto lane_ids = environment_->GetQueryService().GetLaneIdsAtPosition(pose.value().position);
        entity.SetAssignedLaneIds(lane_ids);
    }
}

void TeleportAction::Step()
{
    End();
}

}  // namespace OPENSCENARIO
