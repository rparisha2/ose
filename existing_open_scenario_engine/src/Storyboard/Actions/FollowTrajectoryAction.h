/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "MotionControlAction.h"

#include <vector>

namespace OPENSCENARIO
{

class FollowTrajectoryAction : public MotionControlAction
{
  public:
    FollowTrajectoryAction(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IFollowTrajectoryAction> follow_trajectory_action_data,
        std::shared_ptr<mantle_api::IEnvironment> environment,
        const std::vector<std::string>& actors);

  private:
    void StartAction() override;
    bool HasControlStrategyGoalBeenReached(const std::string& actor) override;
    bool IsLongitudinal() override { return true; }
    bool IsLateral() override { return true; }
    std::optional<mantle_api::ReferenceContext> ConvertDomainReference(
        const NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext& reference_context);

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IFollowTrajectoryAction> follow_trajectory_action_data;
};

}  // namespace OPENSCENARIO
