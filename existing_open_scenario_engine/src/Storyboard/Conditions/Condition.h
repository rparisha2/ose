/*******************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG),
 *               2021 in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "ByEntityConditions/ByEntityConditions.h"
#include "ByValueConditions/ByValueConditions.h"
#include "Common/EdgeEvaluators.h"

#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>
#include <variant>

namespace OPENSCENARIO
{

using ByAnyCondition = std::variant<std::monostate, ByEntityCondition, ByValueCondition>;

class Condition
{
  public:
    Condition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICondition> condition,
              std::shared_ptr<mantle_api::IEnvironment> environment);

    bool IsSatisfied();

  private:
    struct EvaluateByAnyCondition
    {
        bool operator()(const ByValueCondition& by_value_condition)
        {
            return std::visit([](auto&& c) { return c.IsSatisfied(); }, by_value_condition);
        }

        bool operator()(const ByEntityCondition& by_entity_condition)
        {
            return std::visit([](auto&& c) { return c.IsSatisfied(); }, by_entity_condition);
        }

        template <typename T>
        [[noreturn]] bool operator()(T)
        {
            throw std::runtime_error("Uninitialized condition.");
        }
    };

    std::shared_ptr<mantle_api::IEnvironment> env_{nullptr};
    ByAnyCondition condition_;
    std::unique_ptr<EdgeEvaluatorBase> evaluator_;  // TODO: replace with variant
    mantle_api::Time delay_{0.0};
    mantle_api::Time start_time_{0.0};
    std::map<mantle_api::Time, bool> results_;
};
}  // namespace OPENSCENARIO
