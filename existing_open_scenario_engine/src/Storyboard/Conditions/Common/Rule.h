/*******************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *                          in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Common/floating_point_helper.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <type_traits>

namespace OPENSCENARIO
{

/// @brief Epsilon for equality of floating point types
static constexpr double RULE_FLOATING_POINT_EPSILON{1e-6};

/**
 * @brief Evaluator for Rule
 * @see NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum
 *
 * @tparam T floating point or integer type
 * @note for equality of floating point types @ref RULE_FLOATING_POINT_EPSILON is used
 */
template <typename T>
class Rule
{
  public:
    Rule(NET_ASAM_OPENSCENARIO::v1_1::Rule rule, T value) : value_{value}
    {
        if (rule == NET_ASAM_OPENSCENARIO::v1_1::Rule::GREATER_THAN)
        {
            compare_ = [](T lhs, T rhs) { return lhs > rhs; };
        }
        else if (rule == NET_ASAM_OPENSCENARIO::v1_1::Rule::LESS_THAN)
        {
            compare_ = [](T lhs, T rhs) { return lhs < rhs; };
        }
        else if (rule == NET_ASAM_OPENSCENARIO::v1_1::Rule::EQUAL_TO)
        {
            compare_ = [](T lhs, T rhs) {
                if constexpr (std::is_floating_point_v<T>)
                {
                    return mantle_api::IsEqual(lhs, rhs, RULE_FLOATING_POINT_EPSILON);
                }
                else
                {
                    return lhs == rhs;
                }
            };
        }
        else if (rule == NET_ASAM_OPENSCENARIO::v1_1::Rule::GREATER_OR_EQUAL)
        {
            compare_ = [](T lhs, T rhs) {
                if constexpr (std::is_floating_point_v<T>)
                {
                    return lhs > rhs || mantle_api::IsEqual(lhs, rhs, RULE_FLOATING_POINT_EPSILON);
                }
                else
                {
                    return lhs >= rhs;
                }
            };
        }
        else if (rule == NET_ASAM_OPENSCENARIO::v1_1::Rule::LESS_OR_EQUAL)
        {
            compare_ = [](T lhs, T rhs) {
                if constexpr (std::is_floating_point_v<T>)
                {
                    return lhs < rhs || mantle_api::IsEqual(lhs, rhs, RULE_FLOATING_POINT_EPSILON);
                }
                else
                {
                    return lhs <= rhs;
                }
            };
        }
        else if (rule == NET_ASAM_OPENSCENARIO::v1_1::Rule::NOT_EQUAL_TO)
        {
            compare_ = [](T lhs, T rhs) {
                if constexpr (std::is_floating_point_v<T>)
                {
                    return !mantle_api::IsEqual(lhs, rhs, RULE_FLOATING_POINT_EPSILON);
                }
                else
                {
                    return lhs != rhs;
                }
            };
        }
        else
        {
            throw std::runtime_error("Unknown rule (lessThan, equalTo...) used in scenario. Please adjust scenario.");
        }
    }

    /**
     * @brief Evaluates the stored rule, e.g. is current_value GREATHER_THAN(3)?
     *
     * @param current_value current value
     * @return true if current value meets rule, else false
     */
    bool IsSatisfied(T current_value) const { return compare_(current_value, value_); }

  private:
    bool (*compare_)(T, T);  ///!< comparison method
    T value_;                ///!< comparison value
};

}  // namespace OPENSCENARIO
