/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TimeHeadwayCondition.h"

#include "Constants.h"
#include "Storyboard/Utils/EntityUtils.h"

#include <iostream>

namespace OPENSCENARIO
{

TimeHeadwayCondition::TimeHeadwayCondition(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITimeHeadwayCondition> condition,
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggeringEntities> entities,
    std::shared_ptr<mantle_api::IEnvironment> environment)
    : ByEntityConditionsBase(entities),
      condition_{condition},
      environment_{environment},
      rule_{condition->GetRule(), condition->GetValue()}
{
}

bool TimeHeadwayCondition::CheckPreconditions() const
{
    if (!(condition_->GetRelativeDistanceType() ==
          NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::RelativeDistanceTypeEnum::LONGITUDINAL))
    {
        std::cout << "Warning: TimeHeadwayCondition: Currently only supports "
                     "\"RelativeDistanceType\" = \"LONGITUDINAL\"\n";
        return false;
    }

    if (!(condition_->GetCoordinateSystem() ==
              NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::CoordinateSystemEnum::ENTITY ||
          condition_->GetCoordinateSystem() ==
              NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::CoordinateSystemEnum::LANE))
    {
        std::cout << "Warning: TimeHeadwayCondition: Currently only supports "
                     "\"CoordinateSystem\" = \"ENTITY\", \"LANE\"\n";
        return false;
    }

    return true;
}

bool TimeHeadwayCondition::CheckIsSatisfied(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEntityRef> entity) const
{

    const auto& ref_entity = EntityUtils::GetEntityByName(environment_, condition_->GetEntityRef()->GetNameRef());
    const auto& trigger_entity = EntityUtils::GetEntityByName(environment_, entity->GetEntityRef()->GetNameRef());

    units::length::meter_t distance{};
    if (condition_->GetCoordinateSystem() ==
        NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::CoordinateSystemEnum::ENTITY)
    {
        distance = condition_->GetFreespace()
                       ? EntityUtils::CalculateLongitudinalFreeSpaceDistance(environment_, trigger_entity, ref_entity)
                       : EntityUtils::CalculateRelativeLongitudinalDistance(environment_, trigger_entity, ref_entity);
    }
    else if (condition_->GetCoordinateSystem() ==
             NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::CoordinateSystemEnum::LANE)
    {
        const auto longitudinal_lane_distance =
            environment_->GetQueryService().GetLongitudinalLaneDistanceBetweenPositions(trigger_entity.GetPosition(),
                                                                                        ref_entity.GetPosition());
        if (!longitudinal_lane_distance.has_value())
        {
            throw std::runtime_error(
                "TimeHeadwayCondition: CoordinateSystem is set to \"LANE\", but can not get the longitudinal distance "
                "of the reference and the triggering entities along the lane center line. Please adjust scenario.");
        }
        distance = longitudinal_lane_distance.value();

        if (condition_->GetFreespace())
        {
            const auto trigger_entity_lane_orientation =
                environment_->GetQueryService().GetLaneOrientation(trigger_entity.GetPosition());
            const auto trigger_entity_corners_along_lane =
                EntityUtils::GetCornerPositionsInLocalSortedByLongitudinalDistance(
                    environment_, trigger_entity, trigger_entity.GetPosition(), trigger_entity_lane_orientation);

            const auto ref_entity_lane_pose = environment_->GetQueryService().FindLanePoseAtDistanceFrom(
                {trigger_entity.GetPosition(), trigger_entity.GetOrientation()},
                distance,
                mantle_api::Direction::kForward);
            const auto ref_entity_corners_along_lane =
                EntityUtils::GetCornerPositionsInLocalSortedByLongitudinalDistance(
                    environment_, ref_entity, ref_entity.GetPosition(), ref_entity_lane_pose.value().orientation);

            distance = distance - (units::math::abs(trigger_entity_corners_along_lane.front().x) +
                                   units::math::abs(ref_entity_corners_along_lane.back().x));
        }
    }
    else
    {
        // should never happen
        throw std::runtime_error("TimeHeadwayCondition: the given CoordinateSystem is not supported.");
    }

    const auto trigger_entity_velocity = trigger_entity.GetVelocity().Length();
    const auto time_headway = trigger_entity_velocity > limits::kAcceptableVelocityMin
                                  ? distance / trigger_entity_velocity
                                  : limits::kTimeHeadwayMax;

    return rule_.IsSatisfied(time_headway.value());
}

}  // namespace OPENSCENARIO
