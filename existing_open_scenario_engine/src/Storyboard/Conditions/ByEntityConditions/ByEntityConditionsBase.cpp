/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ByEntityConditionsBase.h"

#include "Storyboard/Utils/EntityUtils.h"

#include <algorithm>

namespace OPENSCENARIO
{

ByEntityConditionsBase::ByEntityConditionsBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggeringEntities> entities)
    : triggering_entities_{entities}
{
}

bool ByEntityConditionsBase::IsSatisfied() const
{
    if (!CheckPreconditions())
    {
        std::cout << "Warning: ByEntityConditions automatically satisfied\n";
        return true;
    }

    const auto& triggering_entities_rule = triggering_entities_->GetTriggeringEntitiesRule();
    const auto& entities = triggering_entities_->GetEntityRefs();
    if (triggering_entities_rule == NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::ANY)
    {
        return std::any_of(entities.begin(), entities.end(), [this](auto& entity) { return CheckIsSatisfied(entity); });
    }
    else if (triggering_entities_rule == NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::ALL)
    {
        return std::all_of(entities.begin(), entities.end(), [this](auto& entity) { return CheckIsSatisfied(entity); });
    }
    else
    {
        throw std::runtime_error("ByEntityConditions: TriggeringEntitiesRule is not supported.");
    }
}

}  // namespace OPENSCENARIO
