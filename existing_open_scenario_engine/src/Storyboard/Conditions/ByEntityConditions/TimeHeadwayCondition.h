/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *                     in-tech GmbH
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "Storyboard/Conditions/ByEntityConditions/ByEntityConditionsBase.h"
#include "Storyboard/Conditions/Common/Rule.h"

#include <MantleAPI/Execution/i_environment.h>

#include <memory>

namespace OPENSCENARIO
{
class TimeHeadwayCondition : public ByEntityConditionsBase
{
  public:
    TimeHeadwayCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITimeHeadwayCondition> condition,
                         std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggeringEntities> entities,
                         std::shared_ptr<mantle_api::IEnvironment> environment);

  private:
    bool CheckPreconditions() const override;
    bool CheckIsSatisfied(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEntityRef> entity) const override;

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITimeHeadwayCondition> condition_;
    std::shared_ptr<mantle_api::IEnvironment> environment_;
    OPENSCENARIO::Rule<double> rule_;
};

}  // namespace OPENSCENARIO
