/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RelativeDistanceCondition.h"

#include "Storyboard/Utils/EntityUtils.h"

#include <MantleAPI/Common/pose.h>

#include <iostream>

namespace OPENSCENARIO
{
RelativeDistanceCondition::RelativeDistanceCondition(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRelativeDistanceCondition> condition,
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggeringEntities> entities,
    std::shared_ptr<mantle_api::IEnvironment> environment)
    : ByEntityConditionsBase(entities),
      condition_{condition},
      environment_{environment},
      rule_{condition->GetRule(), condition->GetValue()}
{
}

bool RelativeDistanceCondition::CheckPreconditions() const
{
    if (!(condition_->GetRelativeDistanceType() ==
          NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType::RelativeDistanceTypeEnum::LONGITUDINAL))
    {
        std::cout << "Warning: RelativeDistanceCondition: Currently only supports "
                     "\"RelativeDistanceType\" = \"LONGITUDINAL\"\n";
        return false;
    }

    if (!(condition_->GetCoordinateSystem() ==
          NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::CoordinateSystemEnum::ENTITY))
    {
        std::cout << "Warning: RelativeDistanceCondition: Currently only supports "
                     "\"CoordinateSystem\" = \"ENTITY\"\n";
        return false;
    }

    return true;
}

bool RelativeDistanceCondition::CheckIsSatisfied(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEntityRef> entity) const
{
    const auto& ref_entity = EntityUtils::GetEntityByName(environment_, condition_->GetEntityRef()->GetNameRef());
    const auto& trigger_entity = EntityUtils::GetEntityByName(environment_, entity->GetEntityRef()->GetNameRef());

    const auto distance =
        condition_->GetFreespace()
            ? EntityUtils::CalculateLongitudinalFreeSpaceDistance(environment_, trigger_entity, ref_entity)
            : EntityUtils::CalculateRelativeLongitudinalDistance(environment_, trigger_entity, ref_entity);

    return rule_.IsSatisfied(distance.value());
}

}  // namespace OPENSCENARIO
