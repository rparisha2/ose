/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>

namespace OPENSCENARIO
{

class ByEntityConditionsBase
{
  public:
    ByEntityConditionsBase(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggeringEntities> entities);
    virtual ~ByEntityConditionsBase() = default;

    bool IsSatisfied() const;

  private:
    /// @brief Check if Preconditions for the current Condition are met
    ///
    /// Executed once before checking each triggering entity, e.g. to catch requests for unimplemented features or
    /// errors in the attributes of the condition.
    ///
    /// @return true if preconditions are met
    virtual bool CheckPreconditions() const { return true; };

    /// @brief Check satisfaction of a given triggering entity form the triggering entity list for this condition.
    /// @param entity Triggering entity.
    /// @return True if the given triggering entity satisfies the condition and false vice-versa.
    virtual bool CheckIsSatisfied(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEntityRef> entity) const = 0;

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggeringEntities> triggering_entities_;
};

}  // namespace OPENSCENARIO
