# OpenScenarioEngine

- [User Guide](#User-Guide) (this document)
- [Architecture Documentation](doc/architecture.md)
- [Generator Documentation](doc/generator.md)

# User Guide

The **OpenScenarioEngine** is an implementation of the component _OpenSCENARIO Director_, as proposed by the the [ASAM **OpenSCENARIO 1.1**](https://www.asam.net/standards/detail/openscenario/) standard:

> An OpenSCENARIO Director (OSC Director) is a component that interprets the OSC Model Instance and governs the progress of a scenario in a simulation. The OSC Director is the component responsible for running the scenario.

## Usage

The engine has several dependencies, which are managed by submodules which are checked out into `engine/deps`.
For a detailed overview of the dependencies please refer to the [Architecture Documentation](doc/architecture.md).

### Prerequisites

The engine internally uses the `OpenScenarioLib`, which need to be built manually (please refer to the [build instructions](https://github.com/RA-Consulting-GmbH/openscenario.api.test/blob/master/doc/main.adoc)) before configuring using CMake.

For CMake to find the library, you need to tell it where the pre-built libraries are via the `CMAKE_PREFIX_PATH`, which can be done e.g. by adding the following lines to the `.bashrc` (if you use bash):

```bash
CMAKE_PREFIX_PATH=<path_to_engine>/engine/deps/openscenario_api/cpp/build/cgReleaseMakeShared/expressionsLib:<path_to_engine>/engine/deps/openscenario_api/cpp/build/cgReleaseMakeShared/openScenarioLib:<path_to_engine>/engine/deps/openscenario_api/cpp/build/cgReleaseMakeShared/antlr4_runtime/src/antlr4_runtime/runtime/Cpp/dist:$CMAKE_PREFIX_PATH
```

### Building the Engine

> Linux
>
> ```bash
> # STARTING FROM ROOT OF REPOSITORY
>
> # get dependencies
> git submodule init
> git submodule update
>
> # building the openscenario_api
> # this might differ on your machine → see official build instructions
> cd engine/deps/openscenario_api/cpp/buildArtifact
> chmod +x generateLinux.sh
> ./generateLinux.sh shared release make parallel
>
> cd ../../../.. # back to engine
> cmake -S . -B build
> cd build
> make OpenScenarioEngine
> ```

> Windows <small>(MINGW64 - experimental)</small>
> ```bash
> # STARTING FROM ROOT OF REPOSITORY
>
> # get dependencies
> git submodule init
> git submodule update
>
> # patching openscenario_api for MinGW64
> cd engine/deps/openscenario_api
> git apply ../patches/openscenario_api/mingw64/v1.3.1.patch
>
> # building the openscenario_api (path to JAVA executable might differ!)
> cd cpp
> cmake -Wno-dev --preset="MSYS-shared-release" -DJava_JAVA_EXECUTABLE=/C/Program\ Files/Zulu/zulu-11/bin/java.exe
> cmake --build --preset="Build-MSYS-shared-release"
>
> # building the engine
> cd ../../.. # back to engine
> cmake -G "MSYS Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=bin -S . -B build
> cd build
> make install
> ```
> ⚠️ Note that the demo is not part the `all` target.

### Running the Demo (Using the Engine)

  The demo uses googletest/googlemock and the fakes supplied by the `MantleAPI` to show how the `OpenScenarioEngine` can be used in an executable.

  > Linux
  > ```bash
  > # STARTING FROM ROOT OF REPOSITORY
  > cd engine
  > mkdir build && cd build
  > cmake ..
  > make OpenScenarioEngineDemo # would also build OpenScenarioEngine
  > ./OpenScenarioEngineDemo ../demo/example/scenario.xosc
  > ```

  > Windows <small>(MINGW64 - experimental)</small>
  > ```bash
  > # STARTING FROM ROOT OF REPOSITORY
  > cd engine
  > cmake -G "MSYS Makefiles" -S . -B build
  > cd build
  > make OpenScenarioEngineDemo # would also build OpenScenarioEngine
  > ./OpenScenarioEngineDemo ../demo/example/scenario.xosc
  > ```
  > ⚠️ If you start the demo, Windows might tell you that some `dll`_s_ are missing. Locate and copy all of them into `build`.

# Features

## Covered Actions and Conditions

The following Actions and Conditions of [OpenSCENARIO v1.1](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/index.html) are under development:

### Conditions

| Type              | Subtype          | Status                                                                                                       |
| ------------------| ------------- | ------------------------------------------------------------------------------------------------------------ |
| ByEntityCondition | [ReachPositionCondition](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/ReachPositionCondition.html)       | ✔️ Restricted by conversion: see [Conversions::Position](#position) below                                     |
| ByEntityCondition | [RelativeDistanceCondition](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/RelativeDistanceCondition.html) | Supports only `RelativeDistanceType::kLongitudinal && CoordinateSystem::kEntity`                             |
| ByEntityCondition | [RelativeSpeedCondition](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/RelativeSpeedCondition.html)       | ✔️ Complete                                                                                                   |
| ByEntityCondition | [TimeHeadwayCondition](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/TimeHeadwayCondition.html)           | Supports only `DistanceType::kEuclidean` and `CoordinateSystem::kEntity` or `CoordinateSystem::kLane`                             |
| ByEntityCondition | [TimeToCollisionCondition](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/TimeToCollisionCondition.html)   | In clarification: [Issue #7](https://gitlab.eclipse.org/eclipse/openopass/openscenario1_engine/-/issues/7) |
| ByValueCondition  | [SimulationTimeCondition](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/SimulationTimeCondition.html)     | ✔️ Complete |
| ByValueCondition  | [UserDefinedValueCondition](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/UserDefinedValueCondition.html)     | ✔️ Complete |

### Actions

| Interface           | Type                       | Status     |
| --------------------| -------------------------- | ---------- |
| _none_              | [AcquirePositionAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/AcquirePositionAction.html)       | ✔️ Complete   |
| _none_              | [AssignControllerAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/AssignControllerAction.html)     | 🚧 Sets without consideration of motion domain         |
| _none_              | [AssignRouteAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/AssignRouteAction.html)               | ✔️ Restricted by conversion: see [Conversions::Position](#position)       |
| _none_              | [CustomCommandAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/CustomCommandAction.html)       | ✔️ Complete   |
| _none_              | [DeleteEntityAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/DeleteEntityAction.html)             | ❌ Broken (See [Issue #23](https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine/-/issues/23))                                                               |
| _none_              | [TeleportAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/TeleportAction.html)                     | ✔️ Restricted by conversion: see [Conversions::Position](#position) below |
| _none_              | [TrafficSignalAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/TrafficSignalAction.html) | ✔️ Complete                                             |
| _none_              | [TrafficSignalStateAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/TrafficSignalStateAction.html) | ✔️ Complete                                             |
| _none_              | [TrafficSinkAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/TrafficSinkAction.html)               | ✔️ Does not interpret `rate` and `TrafficDefinition`, and restricted by conversion: see [Conversions::Position](#position) below |
| _none_              | [VisibilityAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/VisibilityAction.html)       | ✔️ Complete   |
| MotionControlAction | [FollowTrajectoryAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/FollowTrajectoryAction.html)     | ✔️ Complete (Possibility for optimization in the base class)     |
| MotionControlAction | [LaneChangeAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/LaneChangeAction.html)                 | ✔️ Complete    |
| MotionControlAction | [LaneOffsetAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/LaneOffsetAction.html)                 | 🚧  Supports only AbsoluteTargetLaneOffset. In addition, LaneOffsetActionDynamics and continuous are ignored at the moment    |
| MotionControlAction | [LateralDistanceAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/LateralDistanceAction.html)       | ❌  Not yet implemented     |
| MotionControlAction | [LongitudinalDistanceAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/LongitudinalDistanceAction.html) | ❌  Not yet implemented     |
| MotionControlAction | [SpeedAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/SpeedAction.html)                           | 🚧 Supports shapes linear and cubic, latter using linear shape coefficients (see [MR74](https://gitlab.eclipse.org/eclipse/openopass/openscenario1_engine/-/merge_requests/74)) |

## TrafficSignalController

If defined in the road network, the engine manages [TrafficSignalController](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/TrafficSignalController.html)s on its own.
The corresponding traffic light states of the traffic light phases are automatically emitted for the respective time steps, using the interface `mantle_api::IEnvironment::SetTrafficSignalState`.

Restrictions:
- Phases must define at least a single TrafficSignalState
- Delays and references between controllers not implemented yet
- TrafficSignalGroupStates (openSCENARIO 1.2) currently not supported

## Conversions

### Absolute Target Lane
Converts number (ID) of the target lane entity from string to [mantle_api::LaneId](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Map/lane_definitio.h)

### Coordinate System
Converts `NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem` enum to [OpenScenario::CoordinateSystem](https://code.in-tech.global/projects/PBESH/repos/openpass-openscenarioengine/browse/ose/src/nodes/utils/ConvertScenarioCoordinateSystem.h) enum.

### Lane Change Target
Converts to [mantle_api::UniqueId](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Common/i_identifiable.h)

| Subtype                | Status |
|------------------------| :----: |
| RelativeTargetLane     | ✔️      |
| AbsoluteTargetLane     | ✔️      |

### Position

Converts to [mantle_api::Pose](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Common/pose.h)

| Subtype                | Status |
|------------------------| :----: |
| WorldPosition          | ✔️     |
| RelativeWorldPosition  | ❌     |
| RelativeObjectPosition | ❌     |
| RoadPosition           | ✔️     |
| RelativeRoadPosition   | ❌     |
| LanePosition           | ✔️     |
| RelativeLanePosition   | ✔️     |
| RoutePosition          | ❌     |
| GeoPosition            | ✔️     |
| TrajectoryPosition     | ❌     |

### Relative Distance Type
Converts `NET_ASAM_OPENSCENARIO::v1_1::IRelativeDistanceType` enum type to [OpenScenario::RelativeDistanceType](https://code.in-tech.global/projects/PBESH/repos/openpass-openscenarioengine/browse/ose/src/nodes/utils/ConvertScenarioRelativeDistanceType.h) enum.

### Relative Target Lane
Converts `NET_ASAM_OPENSCENARIO::v1_1::IRelativeTargetLane` type object to [mantle_api::UniqueId](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Common/i_identifiable.h) from its relative pose and the signed number of lanes that is offset from the reference entity's current lane.

### Route
Converts to [OPENSCENARIO::Route](https://code.in-tech.global/projects/PBESH/repos/openpass-openscenarioengine/browse/ose/src/nodes/utils/ConvertScenarioRoute.h)

| Subtype                | Status |
|------------------------| :----: |
| Route                  | ✔️      |
| CatalogReference       | ✔️      |

### Rule
Compares the quantitative variables or signals with the given condition and returns True or False. **Note**: Conversion rule for DateTime object type is not yet implemented.

| Type of the variable   | Status |
|------------------------| :----: |
| Double                 | ✔️      |
| String                 | ✔️      |
| DateTime               | ❌      |

### Speed Action Target
Converts to `OPENSCENARIO::SpeedActionTarget` which is of type `units::velocity::meters_per_second_t`.

| Subtype                | Status |
|------------------------| :----: |
| AbsoluteTargetSpeed    | ✔️      |
| RelativeTargetSpeed    | ✔️      |

### Time Reference
Converts `NET_ASAM_OPENSCENARIO::v1_1::ITimeReference` object type to [mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Traffic/control_strategy.h) when the timing information is taken into account. **Note**: For now only absolute time value is considered but not relative time value.

### Time to Collision Condition Target
Converts `NET_ASAM_OPENSCENARIO::v1_1::ITimeToCollisionConditionTarget` object type either of `mantle_api::Pose` or `Entity` depending on its property. **Note**: If the object has the property `Position`, then it is restricted by conversion [Conversions::Position](#position)

### Traffic Signal Controller
Takes in the `NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalController` object type and returns the name of the target object in the form of a string.

### Trajectory Reference
Converts `NET_ASAM_OPENSCENARIO::v1_1::ITrajectoryRef` object type to [mantle_api::Trajectory](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Common/trajectory.h)

| Subtype                | Status |
|------------------------| :----: |
| TrajectoryRef          | ✔️      |
| CatalogReference       | ✔️      |

### Transition Dynamics
Converts `NET_ASAM_OPENSCENARIO::v1_1::ITransitionDynamics` object type to [mantle_api::TransitionDynamics](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Traffic/control_strategy.h)  
**Note**: Right now the property `followingMode` is not considered.

# Dependencies

| Dependency | Commit | Version | License |
| ---------- | ------ | ------- | ------- |
| [MantleAPI](https://gitlab.eclipse.org/eclipse/openpass/mantle-api) | 125625f | | EPL 2.0 |
| [OpenSCENARIO API](https://github.com/RA-Consulting-GmbH/openscenario.api.test/) | 65f8dca | 1.3.1 | Apache 2.0 |
| [YASE](https://gitlab.eclipse.org/eclipse/openpass/yase) | e687b5c4 | | EPL 2.0 |
| [Units](https://github.com/nholthaus/units) | ea6d126 | | MIT License |
| [googletest](https://github.com/google/googletest) | 58d77fa | 1.12.1 | BSD-3-Clause License |
| [CPM](https://github.com/cpm-cmake/CPM.cmake) | 03705fc | 0.36.0 | MIT License |

# Issues

Please report (and see) [here](https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine/-/issues).

# License

[Eclipse Public License 2.0](LICENSE)
